//
//  RenderEngine.cpp
//  PokeFPS
//
//  Created by Alexis on 05/08/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "RenderEngine.h"
#include "GFX/OpenGL/GLTextureBinder.h"
#include "GFX/OpenGL/GLHelper.h"

#include "GFX/3DUtils/Plane3D.h"

#include "Game/GameEngine.h"

RenderEngine* RenderEngine::_instance = NULL;

RenderEngine::RenderEngine (GameData *gameData) : _gameData(gameData), _modelProvider ()	{
	_instance = this;
	
	setupGL ();
	
	_square = ModelProvider::modelNamed ("square");
}

RenderEngine::~RenderEngine ()	{
	delete _fbo;
	
	SDL_DestroyRenderer (_theRenderer);
	SDL_DestroyWindow (_theWindow);
}

void RenderEngine::render()	{
	/* Reset viewport */
	glViewport (0, 0, _phWidth, _phHeight);
	GLTextureBinder::forceBindTexture (0);
	GLTextureBinder::forceBindTexture (0, 1);
	
	_fbo->bind ();
	
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	/* Setting up for 2D HUD render */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity ();
	gluOrtho2D(0.0, (float)WIN_W/(float)TILE_SIZE, (float)WIN_H/(float)TILE_SIZE, 0.0);
	glDisable (GL_DEPTH_TEST);
	glMatrixMode (GL_MODELVIEW);
	glDisable (GL_LIGHTING);
	glDisable (GL_CULL_FACE);
	
	renderHUD ();
	
	/* Rendering additional viewports rendering */
	glDisable (GL_LIGHTING);
	_gameData->guiEngine->renderViewports();
	
	glFlush();
	SDL_GL_SwapWindow (_theWindow);
}

void RenderEngine::renderHUD()	{
	
	glEnable (GL_TEXTURE_2D);
	glDisable (GL_LIGHTING);
	
	VBO::disableVBOs ();
	
	GLTextureBinder::forceBindTexture (0, 1);
	glDisable (GL_TEXTURE_2D);
	GLTextureBinder::forceBindTexture (0, 0);
	glDisable (GL_TEXTURE_2D);
	
	glColor4f (1.0, 1.0, 1.0, 1.0);
	// glScaled (2 + sin ((double)SDL_GetTicks () / 1000.0), 2 + sin ((double)SDL_GetTicks () / 1000.0), 1);
	
	// _gameData->ship->render ();
	const std::set<Entity*> &entities = GameEngine::instance ()->entities ();
	// for (auto it = entities.begin (); it != entities.end (); it++)
	//	(*it)->render ();
	for (auto e : entities)
		e->render ();
	
	glColor4f (1.0, 1.0, 1.0, 1.0);
	VBO::disableVBOs ();
	
	_gameData->guiEngine->renderAll ();
	
	_fbo->unbind ();
	//_fbo->bindRender (0);
	
	glColor4f (1.0, 1.0, 1.0, 1.0);
	VBO::disableVBOs ();
	
	glPushMatrix();
	glScaled (_gameData->winGLWidth, _gameData->winGLHeight, 1.0);
	_fbo->bindRender (0);
	_shaderNTSC->useShader (true);
	_shaderNTSC->setUniform ("time", (GLfloat)SDL_GetTicks () / (GLfloat)1000.0);
	_square->render (true);
	_shaderNTSC->useShader (false);
	glPopMatrix ();
}

bool RenderEngine::setupGL()	{
	SDL_Init ( SDL_INIT_VIDEO );
	_theWindow = SDL_CreateWindow ("Galactic War", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIN_W, WIN_H, SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI);
	// _theRenderer = SDL_CreateRenderer (_theWindow, -1, 0);
	SDL_GL_CreateContext (_theWindow);
	SDL_GL_GetDrawableSize (_theWindow, &_phWidth, &_phHeight);
	
#if defined(__WIN32__) || defined(__linux__)
	glewInit ();
#endif
	
	glEnable(GL_DEPTH_TEST);
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	// glBlendFunc (GL_ONE_MINUS_DST_ALPHA, GL_DST_ALPHA);	glMatrixMode( GL_PROJECTION );
	glLoadIdentity ();
	glCullFace (GL_BACK);
	// gluPerspective (FOV,(double)WIN_W/WIN_H,0.5,1000);	// Edit here to change GL View caracteristics
	
#define USE_ALPHA_TESTING

#ifdef USE_ALPHA_TESTING
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc (GL_GREATER, 0.0);
#else
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);          // Enable Alpha Blending (disable alpha testing)
    glEnable(GL_BLEND);
#endif
	
	//glClearColor(0.0, 0.5, 0.9, 1.0);
	glClearColor (0, 0, 0.1, 1);

	glDisable (GL_COLOR_MATERIAL);
	
	_shaderNTSC = new Shader ("fxaa");
	_shaderNTSC->pushUniform ("texture0", 0);
	_shaderNTSC->pushUniform ("screensize", (GLfloat)1.0 / (GLfloat)_phWidth, 1.0 / (GLfloat)_phHeight);
	_shaderNTSC->pushUniform ("time", (GLfloat)0);
	_shaderNTSC->useShader (false);
	
	Log::log << "Uniform \"time\" = " << (int)_shaderNTSC->getUniform ("time") << std::endl;
	
	_fbo = new FBO (_phWidth, _phHeight);
	
	/* Setting up a quad VBO */
	/* Building the VBOs */
	GLfloat vert[] = {
		0.0, 0.0,
		0.0, 1.0,
		1.0, 1.0,
		1.0, 0.0
	}, tex[] = {
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0
	};
	
	/* Well ... we'll set a texture at GL ID 1 because else the VBO class won't build the texture buffer */
	VBO *square = new VBO (vert, tex, NULL, NULL, 4, 1, GL_QUADS, 2);
	square->setTexture (0);
	_modelProvider.addModel ("square", square);
	
	GLTextureBinder::init ();
	
	//SDL_EnableUNICODE(1);
	
	return (_theWindow != NULL);
}

float RenderEngine::pixelsToGLUnit(int pixels)	{
	return (float)pixels / (float)TILE_SIZE;
}
