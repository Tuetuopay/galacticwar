#ifndef SDLGLUTILS_H
#define SDLGLUTILS_H

#include "OSCompatibility.h"

#ifndef GL_CLAMP_TO_EDGE
# define GL_CLAMP_TO_EDGE 0x812F
#endif

#ifndef BUFFER_OFFSET
# define BUFFER_OFFSET(a) ((char*)NULL + (a))
#endif

GLuint loadTexture(const char * filename,bool useMipMap = true, bool cleanAlpha = false);
int takeScreenshot(const char * filename);
void drawAxis(double scale = 1);
void drawBoundingBox ();
int initFullScreen(unsigned int * width = NULL,unsigned int * height = NULL);
int XPMFromImage(const char * imagefile, const char * XPMfile);
SDL_Cursor * cursorFromXPM(const char * xpm[]);

double deg2rad (double deg);

#ifdef __APPLE__
void drawStr(const char *str);
#endif

#endif //SDLGLUTILS_H
