//
//  TextEngine.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _TEXT_ENGINE
#define _TEXT_ENGINE

#include "OSCompatibility.h"

#include "GFX/sdlglutils.h"

typedef struct FontDescriptor
{
	float width, height;
} FontDescriptor;

class TextEngine
{
public:
	TextEngine (std::string font = "");
	~TextEngine ();
	
	/* Will render a SINGLE COLORED NUMBER [0;9] */
	void drawColoredNumber (int number);
	
	/* Renders a full number */
	void drawNumber (int number, float red = 1.0, float green = 1.0, float blue = 1.0);
	
	/* Renders the full background of the specified number
	 * Args: Background rgba
	 *       Outline rgba (default same as background)
	 */
	void drawBackground (int number, float rBG, float gBG, float bBG, float aBG);
	void drawBackground (int number,
							   float rBG, float gBG, float bBG, float aBG,
							   float rOL, float gOL, float bOL, float aOL);
	void drawBackground (char str[], float rBG, float gBG, float bBG, float aBG);
	void drawBackground (char str[],
							   float rBG, float gBG, float bBG, float aBG,
							   float rOL, float gOL, float bOL, float aOL);
	void drawBackground (std::string& str, float rBG, float gBG, float bBG, float aBG);
	void drawBackground (std::string& str,
						 float rBG, float gBG, float bBG, float aBG,
						 float rOL, float gOL, float bOL, float aOL);
	void drawBackground (float width, float rBG, float gBG, float bBG, float aBG);
	void drawBackground (float width,
						 float rBG, float gBG, float bBG, float aBG,
						 float rOL, float gOL, float bOL, float aOL);
	
	/* Renders a string */
	void drawString (const std::string& text, float r = 1.0, float g = 1.0, float b = 1.0)	{ drawString((unsigned char*)text.c_str(), r, g, b); }
	void drawString (const unsigned char text[], float r = 1.0, float g = 1.0, float b = 1.0);
	void drawString3D (const std::string& text, float r = 1.0, float g = 1.0, float b = 1.0)	{ drawString3D((unsigned char*)text.c_str(), r, g, b); }
	void drawString3D (const unsigned char text[], float r = 1.0, float g = 1.0, float b = 1.0);
	
	/* Returns a string width in GL units once rendered */
	float getStringWidth (const char string[]);
	float getStringWidth (const std::string& string);
	float getNumberWidth (int nbr);
	
	/* Will load up all the textures */
	void loadTextures ();
	
	/* Commentless */
	inline static TextEngine* instance ()	{ return _instance; }

private:
	/* Ze instance */
	static TextEngine *_instance;
	
	/* Creates and builds all the display lists for each number */
	void buildDisplayLists ();
	
	/* Font name */
	std::string _fontName;
		
	/* Tells if the textures has ever been loaded, so that we can create as many TextEngines as we wish */
	static bool _resourcesLoaded;
	/* Number textures */
	static GLuint _texDefault;
	/* Display lists for the render */
	static GLuint _dlDefault[256];
	static GLuint _dlFont;
	static FontDescriptor	_fontDescriptor[256];
};

#endif /* defined(_TEXT_ENGINE) */
