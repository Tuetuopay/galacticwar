//
//  TextEngine.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "TextEngine.h"

#include "GFX/OpenGL/GLTextureBinder.h"

/* Declaring the ststic members */
bool	TextEngine::_resourcesLoaded = false;
GLuint	TextEngine::_texDefault = 0;
GLuint	TextEngine::_dlDefault[256] = {0};
GLuint	TextEngine::_dlFont = 0;
FontDescriptor TextEngine::_fontDescriptor[256] = {{0}};

TextEngine* TextEngine::_instance = NULL;

TextEngine::TextEngine (std::string font) : _fontName (font)
{
	if (!_instance)	_instance = this;
	// TextEngine::_resourcesLoaded = false;
	_fontName = font;
	loadTextures();
}

TextEngine::~TextEngine()
{
	
}

void TextEngine::drawString (const unsigned char text[], float r, float g, float b)
{
	glPushMatrix();
	glColor3f (r, g, b);
	
	/*/
	glListBase (_dlFont);
	glCallLists ((int)strlen ((const char*)text), GL_BYTE, text);
	//*/
	
	//*/
	glPushMatrix();
	for (int i = 0; i < strlen((const char*)text); i++)	{
		if (text[i] == '\n')	{
			glPopMatrix();
			glTranslated (0.0, 1.0 + 1.0 / 4.0, 0.0);
			glPushMatrix();
			continue;
		} else if (text[i] == '\t')	{
			glTranslatef (_fontDescriptor['a'].width * 4.0, 0.0, 0.0);	/* Assuming 1 tab is 4 chars */
			continue;
		}
		glCallList (_dlDefault[text[i]] );
		glTranslatef (_fontDescriptor[text[i]].width, 0.0, 0.0);
	}
	glPopMatrix();
	//*/
	glPopMatrix();
}
void TextEngine::drawString3D(const unsigned char text[], float r, float g, float b)	{
	glPushMatrix();
	glScaled (0.1, -0.1, 0.1);	/* Else text would be upside-down */
	drawString (text, r, g, b);
	glPopMatrix();
}

void TextEngine::drawColoredNumber (int number)
{
	if (number >= 10)	return;
	char nbr[2] = "";
	sprintf (nbr, "%d", number);
	switch (number)	{
		case 1:
			drawString (nbr, 4.0/255.0, 52.0/255.0, 1.0);
			break;
		case 2:
			drawString (nbr, 0.0, 125.0/255.0, 0.0);
			break;
		case 3:
			drawString (nbr, 1.0, 38.0/255.0, 0.0);
			break;
		case 4:
			drawString (nbr, 1.0/255.0, 20.0/255.0, 0.5);
			break;
		case 5:
			drawString (nbr, 130.0/255.0, 13.0/255.0, 0.0);
			break;
		case 6:
			drawString (nbr, 0.0, 0.5, 0.5);
			break;
		case 7:
			drawString (nbr, 2.0/255.0, 2.0/255.0, 2.0/255.0);
			break;
		case 8:
			drawString (nbr, 0.5, 0.5, 1.0);
			break;
	}
}

void TextEngine::drawNumber (int number, float r, float g, float b)
{
	char nbr[20] = "";	/* A 32-bit integer is roughly < 16 chars */
	sprintf (nbr, "%d", number);
	
	drawString (nbr, r, g, b);
}

void TextEngine::drawBackground(float width, float rBG, float gBG, float bBG, float aBG, float rOL, float gOL, float bOL, float aOL)
{
	glPushMatrix();
	// glTranslated (0.0, 0.1, 0.0);
	glScaled (1.0, 1.2, 1.0);
	glLineWidth (1.0);
	
	// width *= 0.6;
	
	glDisable (GL_TEXTURE_2D);
	
	glColor4f (rBG, gBG, bBG, aBG);
	glBegin (GL_QUADS);
		glVertex2d (0.0, 0.0);
		glVertex2d (width, 0.0);
		glVertex2d (width, 1.0);
		glVertex2d (0.0, 1.0);
	glEnd ();
	
	glColor4f (rOL, gOL, bOL, aOL);
	glBegin (GL_LINE_STRIP);
		glVertex2d (0.0, 0.0);
		glVertex2d (width, 0.0);
		glVertex2d (width, 1.0);
		glVertex2d (0.0, 1.0);
		glVertex2d (0.0, 0.0);
	glEnd ();
	glPopMatrix();
}
void TextEngine::drawBackground (float width, float rBG, float gBG, float bBG, float aBG)
{
	drawBackground(width, rBG, gBG, bBG, aBG, rBG, gBG, bBG, aBG);
}
void TextEngine::drawBackground (int number, float rBG, float gBG, float bBG, float aBG)
{
	drawBackground(number, rBG, gBG, bBG, aBG, rBG, gBG, bBG, aBG);
}
void TextEngine::drawBackground (int number, float rBG, float gBG, float bBG, float aBG, float rOL, float gOL, float bOL, float aOL)
{
	char nbr[20] = "";
	sprintf (nbr, "%d", number);
	drawBackground(nbr, rBG, gBG, bBG, aBG, rOL, gOL, bOL, aOL);
}
void TextEngine::drawBackground (char str[], float rBG, float gBG, float bBG, float aBG)
{
	drawBackground(str, rBG, gBG, bBG, aBG, rBG, gBG, bBG, aBG);
}
void TextEngine::drawBackground (char str[], float rBG, float gBG, float bBG, float aBG, float rOL, float gOL, float bOL, float aOL)
{
	/* First, compute the width, one number is .6 wide, including margins between number
	 * then adding the left margin
	 */
	drawBackground((float)(getStringWidth (str) * 0.6 + 0.2), rBG, gBG, bBG, aBG, rOL, gOL, bOL, aOL);
}
void TextEngine::drawBackground (std::string& str, float rBG, float gBG, float bBG, float aBG)
{
	drawBackground((char*)str.c_str(), rBG, gBG, bBG, aBG, rBG, gBG, bBG, aBG);
}
void TextEngine::drawBackground (std::string& str,
					 float rBG, float gBG, float bBG, float aBG,
					 float rOL, float gOL, float bOL, float aOL)
{
	drawBackground((char*)str.c_str(), rBG, gBG, bBG, aBG, rBG, gBG, bBG, aBG);
}

float TextEngine::getStringWidth (const std::string& string)
{
	return getStringWidth (string.c_str());
}
float TextEngine::getStringWidth (const char string[])
{
	float width = 0.0;
	for (int i = 0; i < strlen (string); i++)
		width += _fontDescriptor[string[i] + ((string[i] > 0) ? 0 : 256)].width;
	
	return width + 0.6;
}
float TextEngine::getNumberWidth (int nbr)
{
	char str[100] = "";
	sprintf (str, "%d", nbr);
	return getStringWidth (str);
}

void TextEngine::loadTextures()
{
	if (_resourcesLoaded)	return;
	_resourcesLoaded = true;

	if (_fontName == "")	_fontName = "Menlo-24";
	
	// std::cout << "Loading " << _fontName << " font." << std::endl;
	
	_texDefault = loadTexture (std::string ("font/" + _fontName + ".png").c_str ());
	
	FILE *fDat = fopen (std::string ("font/" + _fontName + ".dat").c_str (), "rb");
	if (fDat) {
		fread (_fontDescriptor, sizeof(_fontDescriptor[0]), sizeof(_fontDescriptor) / sizeof(_fontDescriptor[0]), fDat);
		fclose (fDat);
	} else {
		std::cout << "Warning: could not load file \"" << std::string ("font/" + _fontName + ".dat") << "\". Will be using a monospace font !" << std::endl;
		for (int i = 0; i < 256; i++)
			_fontDescriptor[i].width = _fontDescriptor[i].height = 1.0;
	}
	
	// std::cout << "\\t size: " << _fontDescriptor['\t'].width << ", a -> " << _fontDescriptor['a'].width << "\n";
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);

	buildDisplayLists();
}

void TextEngine::buildDisplayLists()
{
	/* Building font display lists */
	for (int v = 0; v < 16; v++)	{
		for (int u = 0;u < 16; u++)	{
			_dlDefault[(15 - v) * 16 + u] = glGenLists(1);
			glNewList (_dlDefault[(15 - v) * 16 + u], GL_COMPILE);
				glPushMatrix();
				glEnable (GL_TEXTURE_2D);
				glBindTexture (GL_TEXTURE_2D, _texDefault);
				// glScaled (0.8, 0.8, 1.0);
				//glTranslated (0.0, 0.1, 0.0);
				glBegin (GL_QUADS);
					glTexCoord2d(((double)u + 1.0) / 16.0, (double)v / 16.0);			glVertex2d(1.0, 1.0);
					glTexCoord2d(((double)u + 1.0) / 16.0, ((double)v + 1.0) / 16.0);	glVertex2d(1.0, 0.0);
					glTexCoord2d((double)u / 16.0, ((double)v + 1.0) / 16.0);			glVertex2d(0.0, 0.0);
					glTexCoord2d((double)u / 16.0, (double)v / 16.0);					glVertex2d(0.0, 1.0);
				glEnd ();
				glPopMatrix();
			glEndList();
		}
	}
}
