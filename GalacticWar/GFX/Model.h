//
//  Model.h
//  GalacticWar
//
//  Created by Alexis on 15/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _MODEL_H
#define _MODEL_H

#include "OSCompatibility.h"

#include "GFX/OpenGL/VBO.h"

class Model	{
public:
	Model (float period, VBO *firstFrame = NULL);
	~Model ();
	
	bool addFrame (VBO *frame);
	bool addFrame (const float *vertices, const float *texture, const float *colors, const float *normals,
				   int nVertices, GLuint gltexture = 0, GLenum drawMode = GL_TRIANGLES,
				   int vertexSize = 3, int colorSize = 4, int textureSize = 2);
	
	virtual void update (double frameLength);
	virtual void render (bool forceTexturing = false);
	
	inline double period () const { return _period; }
	inline void setPeriod (double period) { _period = period; }
	
	inline void reset ()	{ _time = 0.0; }
	
private:
	/* Basically all the frames */
	std::vector<VBO*>	_frames;
	
	/* Animations preperties */
	float _period, _time;
};

#endif /* defined(_MODEL_H) */