//
//  RenderEngine.h
//  PokeFPS
//
//  Created by Alexis on 05/08/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

/* / / / / / / / / / /  Render Engine / / / / / / / / / / / /
 * Handling all game's render work. Charged of handling both
 * 3D render and 2D (HUD) render
 */

#ifndef _RENDER_ENGINE_H
#define _RENDER_ENGINE_H

#define WIN_W	1280
#define WIN_H	720
#define FOV		90.0
#define FPS		30

#define TILE_SIZE	16	/* Well ... 16px a GL Unit, seems legit ! */
#define TILES_W		WIN_W/TILE_SIZE
#define TILES_H		WIN_H/TILE_SIZE


#include "OSCompatibility.h"

#include "GFX/sdlglutils.h"	/* Some useful things in here */

#include "Game/GameData.h"
#include "GFX/TextEngine.h"	/* We *should* need this xD */
#include "GFX/OpenGL/Shader.h"
#include "GFX/OpenGL/VBO.h"
#include "GFX/OpenGL/FBO.h"

#include "GFX/ModelProvider.h"

class RenderEngine
{
public:
	RenderEngine (GameData *gameData);
	~RenderEngine ();
	
	/* Main render function, that will call the other stuff */
	void render ();
	
	/* Utilitary */
	static float pixelsToGLUnit (int pixels);
	//void setAlpha (float alpha)	{ _shaderLight->setUniform ("alpha", alpha); }
	
	static RenderEngine* instance ()	{ return _instance; }
	
	//static void disableShaders ()	{ _instance->_shaderLight->useShader (false); }
	//static void enableShaders ()	{ _instance->_shaderLight->useShader (true); }
	
	void warpMouse (int x, int y)	{ SDL_WarpMouseInWindow (_theWindow, x, y); }
	
private:
	/* Rendering methods. Should NOT be called manually (aka. render setup by render ()) */
	void renderHUD ();
	
	void computeHoveredObject ();
	
	/* Setting up methods. Returns if setup was successful */
	bool setupGL ();
	
	/* A copy of the game's data */
	GameData	*_gameData;

	// Shader	*_shaderSSAO, *_shaderColorCorrection, *_shaderLight;
	// VBO		*_squareBO;
	Shader	*_shaderNTSC;
	FBO		*_fbo;
	Model	*_square;
	
	ModelProvider	_modelProvider;
	
	SDL_Window		*_theWindow;
	SDL_Renderer	*_theRenderer;
	
	// Let's manage Retina (high-dpi) screens
	int _phWidth, _phHeight;
	
	static RenderEngine	*_instance;
};

#endif /* defined(_RENDER_ENGINE_H) */
