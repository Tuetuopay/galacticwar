//
//  Vector3D.cpp
//  PokeFPS
//
//  Created by Alexis on 07/08/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GFX/3DUtils/Vector3D.h"

Vector3D::Vector3D ()
{
	x = y = z = 0;
}

Vector3D::Vector3D (double _x, double _y, double _z)
{
	x = _x;
	y = _y;
	z = _z;
}

double Vector3D::length() const	{
	return sqrt (x*x + y*y + z*z);
}

void Vector3D::normalize()	{
    (*this) = normalized ();
}

Vector3D Vector3D::normalized () const	{
    if (length() == 0)	return (*this);
    return (*this) / length();
}

void Vector3D::round ()	{
    (*this) = rounded ();
}

Vector3D Vector3D::rounded() const	{
    Vector3D vec (*this);
	/*/
    if (vec.x < 0)	vec.x--;
    if (vec.y < 0)	vec.y--;
    if (vec.z < 0)	vec.z--;

    vec.x = (int)vec.x;
    vec.y = (int)vec.y;
    vec.z = (int)vec.z;
	//*/
	
	vec.x = floor (vec.x);
	vec.y = floor (vec.y);
	vec.z = floor (vec.z);

    return vec;
}

Vector3D Vector3D::normalVector (double x, double y, double z) const	{
	return normalVector (Vector3D (x, y, z));
}
Vector3D Vector3D::normalVector (Vector3D vec) const	{
	return this->cross (vec);
}

Vector3D Vector3D::cross (const Vector3D &vec) const	{	
	return Vector3D ((y*vec.z) - (z*vec.y),
					 (z*vec.x) - (x*vec.z),
					 (x*vec.y) - (y*vec.x));
}

Vector3D Vector3D::operator=(const Vector3D &vec)
{
	x = vec.x;
	y = vec.y;
	z = vec.z;
	
	return *this;
}
Vector3D Vector3D::operator+ (const Vector3D &vec) const
{
	Vector3D ret (x, y, z);
	ret.x += vec.x;
	ret.y += vec.y;
	ret.z += vec.z;
	
	return ret;
}
Vector3D Vector3D::operator- (const Vector3D &vec) const
{
	Vector3D ret (x, y, z);
	ret.x -= vec.x;
	ret.y -= vec.y;
	ret.z -= vec.z;
	
	return ret;
}
Vector3D Vector3D::operator* (double mult) const
{
	Vector3D ret (x, y, z);
	ret.x *= mult;
	ret.y *= mult;
	ret.z *= mult;
	
	return ret;
}
Vector3D Vector3D::operator/ (double mult) const
{
	Vector3D ret (x, y, z);
	ret.x /= mult;
	ret.y /= mult;
	ret.z /= mult;
	
	return ret;
}

Vector3D Vector3D::operator+= (const Vector3D &vec)
{
	(*this) = (*this) + vec;
	return (*this);
}
Vector3D Vector3D::operator-= (const Vector3D &vec)
{
	(*this) = (*this) - vec;
	return (*this);
}
Vector3D Vector3D::operator*= (double mult)
{
	(*this) = (*this) * mult;
	return (*this);
}
Vector3D Vector3D::operator/= (double mult)
{
	(*this) = (*this) / mult;
	return (*this);
}

std::string Vector3D::describe() const	{
    return "(" + std::to_string (x) + ", " + std::to_string (y) + ", " + std::to_string (z) + ")";
}

Vector3D::~Vector3D ()
{
	
}
