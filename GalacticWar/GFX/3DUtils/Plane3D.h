//
//  Plane3D.h
//  PokeFPS
//
//  Created by Alexis on 15/10/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _PLANE_3D_H
#define _PLANE_3D_H

#include "Vector3D.h"

class Plane3D	{
public:
	Plane3D ();
	Plane3D (Vector3D fixedPoint, Vector3D normal);
	Plane3D (Vector3D point1, Vector3D point2, Vector3D point3);
	Plane3D (Vector3D point1, Vector3D point2, Vector3D point3, Vector3D directionPoint);
	~Plane3D ();
	
	bool isInFront (Vector3D pt) const;
	double apply (Vector3D pt) const;
	Vector3D intersection (Vector3D pt1, Vector3D pt2) const;
	Vector3D orthogonalPoint (Vector3D pt) const;
	
	void updateFixedPoint (Vector3D pt);
	void updateNormal (Vector3D norm);
	
	inline void flipNormal ()	{ _normal *= -1;	_offset *= -1; }
	
	inline double distance (const Plane3D& pl) const { return (_fixedPoint - pl.orthogonalPoint (_fixedPoint)).length (); }
	inline double distance (const Vector3D& vec) const	{ return (vec - orthogonalPoint (vec)).length (); }
	
	inline const Vector3D& normal () const	{ return _normal; }
	
private:
	Vector3D	_fixedPoint, _normal;
	double _offset;
};

#endif /* defined(_PLANE_3D_H) */
