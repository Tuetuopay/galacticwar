//
//  HitboxRenderer.h
//  PokeFPS
//
//  Created by Alexis on 23/09/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _HITBOX_RENDERER_H
#define _HITBOX_RENDERER_H

#include "OSCompatibility.h"
#include "GFX/OpenGL/VBO.h"
#include "GFX/3DUtils/AxisAlignedBB.h"

#define HBRENDERER_CYLINDER_SLICES	12
#define HBRENDERER_SPHERE_SLICES	12
#define HBRENDERER_SPHERE_LAYERS	6

class HitboxRenderer
{
public:
	HitboxRenderer (bool buildStuff = true);	/* As of now, buildRenderData is called upon program start */
	~HitboxRenderer ();
	
	/* Quick-drawing of a hitbox */
	/* The hitbox is rendered from it's origin: AABB by the lower corner, Cylinder by Base center, Sphere by center point */
	void render (const AxisAlignedBB &hitbox);
	
	/* This is used to report all generation & computation stuff to another thread when loading the game */
	static void buildRenderData ();
	
	static HitboxRenderer* instance () { return _instance; }
	
private:
	static GLuint _dlCube;
	static GLUquadric *_quadric;
	static VBO	*_cubeBO;
	
	static HitboxRenderer	*_instance;
};

#endif /* defined(_HITBOX_RENDERER_H) */
