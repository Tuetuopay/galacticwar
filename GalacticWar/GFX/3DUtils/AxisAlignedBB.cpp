//
//  AxisAlignedBB.cpp
//  PokeFPS
//
//  Created by Alexis on 02/12/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "AxisAlignedBB.h"

#include "HitboxRenderer.h"

AxisAlignedBB::AxisAlignedBB (Vector3D min, Vector3D max) : _min(min), _max(max)	{
}
AxisAlignedBB::AxisAlignedBB (double minX, double minY, double minZ, double maxX, double maxY, double maxZ)
	: _min (minX, minY, minZ), _max (maxX, maxY, maxZ)	{
}
AxisAlignedBB::~AxisAlignedBB ()	{
	
}

AxisAlignedBB AxisAlignedBB::getBoundingBox (const Vector3D& min, const Vector3D& max) 	{
	return AxisAlignedBB (min, max);
}
AxisAlignedBB AxisAlignedBB::getBoundingBox (double minX, double minY, double minZ, double maxX, double maxY, double maxZ) 	{
	return AxisAlignedBB (minX, minY, minZ, maxX, maxY, maxZ);
}

AxisAlignedBB& AxisAlignedBB::setBounds (const Vector3D& min, const Vector3D& max)	{
	_min = min;	_max = max;
	return *this;
}
AxisAlignedBB& AxisAlignedBB::setBounds (double minX, double minY, double minZ, double maxX, double maxY, double maxZ)	{
	_min.x = minX;	_min.y = minY;	_min.z = minZ;
	_max.x = maxX;	_max.y = maxY;	_max.z = maxZ;
	return *this;
}

/* Adds the coordinates to the bounding box extending it if the point lies outside the current ranges. */
AxisAlignedBB& AxisAlignedBB::addCoord (const Vector3D& point)	{
	if (point.x < 0.0)
		_min.x += point.x;
	
	if (point.x > 0.0)
		_max.x += point.x;
	
	if (point.y < 0.0)
		_min.y += point.y;
	
	if (point.y > 0.0)
		_max.y += point.y;
	
	if (point.z < 0.0)
		_min.z += point.z;
	
	if (point.z > 0.0)
		_max.z += point.z;
	
	return *this;
}
AxisAlignedBB& AxisAlignedBB::addCoord (double x, double y, double z)	{
	return addCoord (Vector3D (x, y, z));
}

/* Returns a bounding box expanded by the specified vector (if negative numbers are given it will shrink). */
AxisAlignedBB& AxisAlignedBB::expand (const Vector3D& vec)	{
	_min -= vec;	_max += vec;
	return *this;
}
AxisAlignedBB& AxisAlignedBB::expand (double x, double y, double z)	{
	return expand (Vector3D (x, y, z));
}

/* Returns a bounding box offseted by the specified vector (if negative numbers are given it will shrink). */
AxisAlignedBB AxisAlignedBB::getOffsetBoundingBox (const Vector3D& vec) const	{
	return AxisAlignedBB (*this).offset (vec);
}
AxisAlignedBB AxisAlignedBB::getOffsetBoundingBox (double x, double y, double z) const	{
	return AxisAlignedBB (*this).offset (x, y, z);
}

/* How the given hitbow will be ejected from instance */
Vector3D AxisAlignedBB::eject (const AxisAlignedBB& box, const Vector3D& motion) const 	{
	Vector3D workMotion (motion), ret;
	AxisAlignedBB workBox (*this);
	
	ret.y = workMotion.y = workBox.calculateYOffset (box, workMotion.y);
	//workBox.offset (0.0, workMotion.y, 0.0);
	//if (workMotion.y != motion.y)
	//	workMotion = Vector3D ();
	
	ret.x = workMotion.x = workBox.calculateXOffset (box, workMotion.x);
	//workBox.offset (workMotion.x, 0.0, 0.0);
	//if (workMotion.x != motion.x)
	//	workMotion = Vector3D ();
	
	ret.z = workMotion.z = workBox.calculateZOffset (box, workMotion.z);
	//workBox.offset (0.0, 0.0, workMotion.z);
	//if (workMotion.x != motion.x)
	//	workMotion = Vector3D ();
	
	if (intersectsWith (box))	{
		if (ret.x > ret.y && ret.x > ret.z)
			ret.y = ret.z = 0;
		else if (ret.y > ret.x && ret.y > ret.z)
			ret.x = ret.z = 0;
		else if (ret.z > ret.x && ret.z > ret.y)
			ret.y = ret.z = 0;
	}
	
	return ret;
}

/**
 * If instance and the argument bounding boxes overlap in the unspecified dimensions, calculate the offset between them
 * in the said dimension.  return <def> if the bounding boxes do not overlap or if <def> is closer to 0 than the
 * calculated offset.  Otherwise return the calculated offset.
 */
double AxisAlignedBB::calculateXOffset(const AxisAlignedBB& box, double def) const	{
	if (box._max.y > _min.y && box._min.y < _max.y)
	{
		if (box._max.z > _min.z && box._min.z < _max.z)
		{
			double delta;
			
			if (def > 0.0 && box._max.x <= _min.x)	{
				delta = _min.x - box._max.x;
				if (delta < def)
					def = delta;
			}
			
			if (def < 0.0 && box._min.x >= _max.x)	{
				delta = _max.x - box._min.x;
				if (delta > def)
					def = delta;
			}
		}
	}
	
	return def;
}
double AxisAlignedBB::calculateYOffset(const AxisAlignedBB& box, double def) const	{
	if (box._max.x > _min.x && box._min.x < _max.x)
	{
		if (box._max.z > _min.z && box._min.z < _max.z)
		{
			double delta;
			
			if (def > 0.0 && box._max.y <= _min.y)	{
				delta = _min.y - box._max.y;
				if (delta < def)
					def = delta;
			}
			
			if (def < 0.0 && box._min.y >= _max.y)	{
				delta = _max.y - box._min.y;
				if (delta > def)
					def = delta;
			}
		}
	}
	
	return def;
}
double AxisAlignedBB::calculateZOffset(const AxisAlignedBB& box, double def) const	{
	if (box._max.x > _min.x && box._min.x < _max.x)
	{
		if (box._max.y > _min.y && box._min.y < _max.y)
		{
			double delta;
			
			if (def > 0.0 && box._max.z <= _min.z)	{
				delta = _min.z - box._max.z;
				if (delta < def)
					def = delta;
			}
			
			if (def < 0.0 && box._min.z >= _max.z)	{
				delta = _max.z - box._min.z;
				if (delta > def)
					def = delta;
			}
		}
	}
	
	return def;
}

/* Returns whether the given bounding box intersects with instance. */
bool AxisAlignedBB::intersectsWith(const AxisAlignedBB& box) const	{
	return box._max.x > _min.x && box._min.x < _max.x ? (box._max.y > _min.y && box._min.y < _max.y ? box._max.z > _min.z && box._min.z < _max.z : false) : false;
}

/* Offsets the current bounding box by the specified coordinates. */
AxisAlignedBB& AxisAlignedBB::offset (Vector3D vec)	{
	_min += vec;
	_max += vec;
	return *this;
}
AxisAlignedBB& AxisAlignedBB::offset (double x, double y, double z)	{
	return offset (Vector3D (x, y, z));
}

/* Returns if the supplied Vec3D is completely inside the bounding box */
bool AxisAlignedBB::isVecInside (const Vector3D& vec) const	{
	return (vec.x > _min.x && vec.x < _max.x && vec.y > _min.y && vec.y < _max.y && vec.z > _min.z && vec.z < _max.z);
}
bool AxisAlignedBB::isVecInside (double x, double y, double z) const	{
	return isVecInside (Vector3D (x, y, z));
}

/* Returns the average length of the edges of the bounding box. */
double AxisAlignedBB::getAverageEdgeLength () const	{
	static Vector3D avg;
	avg = _max - _min;
	return (avg.x + avg.y + avg.z) / 3.0;
}

/* Returns a bounding box that is inset by the specified amounts */
AxisAlignedBB& AxisAlignedBB::contract (Vector3D inset)	{
	_min += inset;	_max -= inset;
	return *this;
}
AxisAlignedBB& AxisAlignedBB::contract (double x, double y, double z)	{
	return contract (Vector3D (x, y, z));
}

/* Sets the bounding box to the same bounds as the bounding box passed in. */
void AxisAlignedBB::setBB (AxisAlignedBB box)	{
	_min = box._min;	_max = box._max;
}

/**
 * Checks if the specified vector is within the YZ dimensions of the bounding box. Args: Vec3D
 */
bool AxisAlignedBB::isVecInYZ (Vector3D vec)	{
	return vec.y >= _min.y && vec.y <= _max.y && vec.z >= _min.z && vec.z <= _max.z;
}
bool AxisAlignedBB::isVecInYZ (double x, double y, double z)	{
	return isVecInYZ (Vector3D (x, y, z));
}

/**
 * Checks if the specified vector is within the XZ dimensions of the bounding box. Args: Vec3D
 */
bool AxisAlignedBB::isVecInXZ (Vector3D vec)	{
	return vec.x >= _min.x && vec.x <= _max.x && vec.z >= _min.z && vec.z <= _max.z;
}
bool AxisAlignedBB::isVecInXZ (double x, double y, double z)	{
	return isVecInXZ (Vector3D (x, y, z));
}

/**
 * Checks if the specified vector is within the XY dimensions of the bounding box. Args: Vec3D
 */
bool AxisAlignedBB::isVecInXY(Vector3D vec)	{
	return vec.x >= _min.x && vec.x <= _max.x && vec.y >= _min.y && vec.y <= _max.y;
}
bool AxisAlignedBB::isVecInXY(double x, double y, double z)	{
	return isVecInXY (Vector3D (x, y, z));
}

void AxisAlignedBB::render () const	{
	HitboxRenderer::instance()->render (*this);
}














