//
//  AxisAlignedBB.h
//  PokeFPS
//
//  Created by Alexis on 02/12/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _AXIS_ALIGNED_BB_H
#define _AXIS_ALIGNED_BB_H

#include <iostream>

#include "GFX/3DUtils/Vector3D.h"

/* Well, because I'm lazy, this class is a HUGE copy/paste from Minecraft's AxisAlignedBB ...
 * Why reinventing what's working ? We must admit that Minecraft's collisions management is the least buggy
 * thing (except on multiplayer, but that's coming from lag)
 */

class AxisAlignedBB	{
public:
	AxisAlignedBB ()	{ setBounds (0, 0, 0, 1, 1, 1); }
	AxisAlignedBB (Vector3D min, Vector3D max);
	AxisAlignedBB (double minX, double minY, double minZ, double maxX, double maxY, double maxZ);
	~AxisAlignedBB ();
	
	static AxisAlignedBB getBoundingBox (const Vector3D& min, const Vector3D& max);
	static AxisAlignedBB getBoundingBox (double minX, double minY, double minZ, double maxX, double maxY, double maxZ);

	AxisAlignedBB& setBounds (const Vector3D& min, const Vector3D& max);
	AxisAlignedBB& setBounds (double minX, double minY, double minZ, double maxX, double maxY, double maxZ);
	
	/* Adds the coordinates to the bounding box extending it if the point lies outside the current ranges. */
	AxisAlignedBB& addCoord (const Vector3D& point);
	AxisAlignedBB& addCoord (double x, double y, double z);
	
	/* Returns a bounding box expanded by the specified vector (if negative numbers are given it will shrink). */
	AxisAlignedBB& expand (const Vector3D& vec);
	AxisAlignedBB& expand (double x, double y, double z);
	
	/* Returns a bounding box offseted by the specified vector (if negative numbers are given it will shrink). */
	AxisAlignedBB getOffsetBoundingBox (const Vector3D& vec) const;
	AxisAlignedBB getOffsetBoundingBox (double x, double y, double z) const;
	
	/**
	 * If instance and the argument bounding boxes overlap in the unspecified dimensions, calculate the offset between them
	 * in the said dimension.  return <def> if the bounding boxes do not overlap or if <def> is closer to 0 than the
	 * calculated offset.  Otherwise return the calculated offset.
	 */
	double calculateXOffset(const AxisAlignedBB& box, double def) const;
	double calculateYOffset(const AxisAlignedBB& box, double def) const;
	double calculateZOffset(const AxisAlignedBB& box, double def) const;
	
	/* How the given hitbow will be ejected from instance */
	Vector3D eject (const AxisAlignedBB& box, const Vector3D& motion) const;
	
	/* Returns whether the given bounding box intersects with instance. */
	bool intersectsWith(const AxisAlignedBB& box) const;
	
	/* Offsets the current bounding box by the specified coordinates. */
	AxisAlignedBB& offset (const Vector3D vec);
	AxisAlignedBB& offset (double x, double y, double z);
	
	/* Returns if the supplied Vec3D is completely inside the bounding box */
	bool isVecInside (const Vector3D& vec) const;
	bool isVecInside (double x, double y, double z) const;
	
	/* Returns the average length of the edges of the bounding box. */
	double getAverageEdgeLength () const;
	
	/* Returns a bounding box that is inset by the specified amounts */
	AxisAlignedBB& contract (Vector3D inset);
	AxisAlignedBB& contract (double x, double y, double z);
	
	/* Sets the bounding box to the same bounds as the bounding box passed in. */
	void setBB (AxisAlignedBB box);
	
	inline std::string toString ()	{
		return "box[" + _min.describe () + " -> " + _max.describe () + "]";
	}
	
	inline AxisAlignedBB copy () const { return *this; }
	
	void render () const;
	
	inline const Vector3D& min () const	{ return _min; }
	inline const Vector3D& max () const	{ return _max; }
	
	inline AxisAlignedBB operator+ (const Vector3D& vec) const	{ return copy () += vec; }
	inline AxisAlignedBB operator- (const Vector3D& vec) const	{ return copy () -= vec; }
	inline AxisAlignedBB operator+= (const Vector3D& vec)	{ offset (vec); return *this; }
	inline AxisAlignedBB operator-= (const Vector3D& vec)	{ offset (vec * -1);	return *this; }
	
private:
	/**
	 * Checks if the specified vector is within the YZ dimensions of the bounding box. Args: Vec3D
	 */
	bool isVecInYZ (Vector3D vec);
	bool isVecInYZ (double x, double y, double z);
	
	/**
	 * Checks if the specified vector is within the XZ dimensions of the bounding box. Args: Vec3D
	 */
	bool isVecInXZ (Vector3D vec);
	bool isVecInXZ (double x, double y, double z);
	
	/**
	 * Checks if the specified vector is within the XY dimensions of the bounding box. Args: Vec3D
	 */
	bool isVecInXY(Vector3D vec);
	bool isVecInXY(double x, double y, double z);
	
protected:
	Vector3D _min, _max;
};

#endif /* defined(_AxisAlignedBB_H) */