//
//  Vector3D.h
//  PokeFPS
//
//  Created by Alexis on 07/08/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _VECTOR_3D_H
#define _VECTOR_3D_H

/* #include <iostream>	*/
/* #include <string>	*/
/* #include <cmath>		*/
#include "OSCompatibility.h"

class Vector3D
{
public:
	Vector3D ();
	Vector3D (double x, double y, double z);
	~Vector3D ();
	
	Vector3D operator= (const Vector3D &vec);
	Vector3D operator+ (const Vector3D &vec) const;
	Vector3D operator- (const Vector3D &vec) const;
	// Vector3D operator* (const Vector3D &vec) const;	/* VECTORIAL product, not scalar */
	Vector3D operator* (double mult) const;
	Vector3D operator/ (double mult) const;
	Vector3D operator+= (const Vector3D &vec);
	Vector3D operator-= (const Vector3D &vec);
	// Vector3D operator*=(const Vector3D &vec);
	Vector3D operator*= (double mult);
	Vector3D operator/= (double mult);
	
	std::string describe () const;
	
	void normalize ();
	Vector3D normalized () const;	/* so that you can chain operations: vec.normalized () + vec2 ... */
	double length () const;
	
	/* Will convert to integer values */
	void round ();
	Vector3D rounded () const;
	
	inline double dot (const Vector3D& vec) const	{ return (x*vec.x) + (y*vec.y) + (z*vec.z); }
	Vector3D cross (const Vector3D& vec) const;
	
	/* Will return the normal vector (aka. rotated 90°) with the given one */
	Vector3D normalVector (double x, double y, double z) const;
	Vector3D normalVector (Vector3D vec) const;
	
	double x, y, z;
};

#endif /* defined(_VECTOR_3D_H) */
