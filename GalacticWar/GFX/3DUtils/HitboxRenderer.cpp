//
//  HitboxRenderer.cpp
//  PokeFPS
//
//  Created by Alexis on 23/09/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "HitboxRenderer.h"

GLuint HitboxRenderer::_dlCube = 0;
GLUquadric *HitboxRenderer::_quadric = NULL;
VBO *HitboxRenderer::_cubeBO = NULL;

HitboxRenderer* HitboxRenderer::_instance = NULL;

HitboxRenderer::HitboxRenderer (bool buildStuff)
{
	if (buildStuff)	buildRenderData();
	if (!_instance)	_instance = this;
}

HitboxRenderer::~HitboxRenderer ()
{
	
}

// void HitboxRenderer::render (const Hitbox &hitbox)	{
//	glPushMatrix();
	// glColor4ub (red, green, blue, alpha);
	/*/
	switch (hitbox.hitboxType ())	{
		case HITBOX_AXIS_ALIGNED:	{
			Vector3D vec1 = hitbox.axisAlignedBBMin(), vec2 = hitbox.axisAlignedBBMax();
			glTranslated (vec1.x , vec1.y, vec1.z);
			glScaled ((vec2 - vec1).x, (vec2 - vec1).y, (vec2 - vec1).z);
			glCallList (_dlCube);
			break;
		}
			
		case HITBOX_CYLINDER:
			glTranslated (hitbox.cylinderBBBaseCenter().x, hitbox.cylinderBBBaseCenter().y, hitbox.cylinderBBBaseCenter().z);
			glRotated (90, 1, 0, 0);
			gluCylinder (_quadric, hitbox.cylinderBBRadius(), hitbox.cylinderBBRadius(), hitbox.cylinderBBHeight(), HBRENDERER_CYLINDER_SLICES, 1);
			break;
			
		case HITBOX_SPHERICAL:
			glTranslated (hitbox.sphereBBCenter().x, hitbox.sphereBBCenter().y, hitbox.sphereBBCenter().z);
			gluSphere (_quadric, hitbox.sphereBBRadius(), HBRENDERER_SPHERE_SLICES, HBRENDERER_SPHERE_LAYERS);
			break;
	}
	//*/
//	glPopMatrix();
//}

void HitboxRenderer::render (const AxisAlignedBB &hitbox)	{
	glDisable (GL_TEXTURE_2D);
	
	glLineWidth (2);
	glPushMatrix ();
	glTranslated (hitbox.min ().x, hitbox.min ().y, hitbox.min ().z);
	glScaled ((hitbox.max () - hitbox.min ()).x, (hitbox.max () - hitbox.min ()).y, (hitbox.max () - hitbox.min ()).z);
	if (_cubeBO)	_cubeBO->render ();
	glPopMatrix ();
	glLineWidth (1);
	
	glEnable (GL_TEXTURE_2D);
}

void HitboxRenderer::buildRenderData ()	{
	if (_dlCube)	return;
	
	_dlCube = glGenLists (1);
	_quadric = gluNewQuadric();
	
	float vert[] = {
		0, 0, 0,
		0, 0, 1,
		0, 1, 1,
		0, 1, 0,
		0, 0, 0,
		1, 0, 0,
		1, 0, 1,
		1, 1, 1,
		1, 1, 0,
		1, 0, 0,
		1, 1, 0,
		0, 1, 0,
		0, 1, 1,
		1, 1, 1,
		1, 0, 1,
		0, 0, 1
	}, col[] = {
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, 1, 1,
	};
	
	if (!_cubeBO)	_cubeBO = new VBO (vert, NULL, col, NULL, 16, 0, GL_LINE_STRIP);
	
	/* Configuring cylinder & sphere */
	gluQuadricDrawStyle (_quadric, GLU_LINE);
	gluQuadricTexture (_quadric, GL_FALSE);
	
}
