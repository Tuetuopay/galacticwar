//
//  Plane3D.cpp
//  PokeFPS
//
//  Created by Alexis on 15/10/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "Plane3D.h"

Plane3D::Plane3D ()	{
	_fixedPoint = Vector3D ();
	_normal = Vector3D (0.0, 1.0, 0.0);;
	_offset = 0.0;
}
Plane3D::Plane3D (Vector3D fixedPoint, Vector3D normal)	{
	_fixedPoint = fixedPoint;
	_normal = normal.normalized ();
}
Plane3D::Plane3D (Vector3D point1, Vector3D point2, Vector3D point3)	{
	_fixedPoint = point1;
	_normal = (point1 - point2).cross (point1 - point3).normalized ();
	
	_offset = -apply (point1);
}
Plane3D::Plane3D (Vector3D point1, Vector3D point2, Vector3D point3, Vector3D directionPoint)	{
	_fixedPoint = point1;
	_normal = (point1 - point2).cross (point1 - point3).normalized ();
	
	_offset = -apply (point1);
	
	if (!isInFront (directionPoint))
		flipNormal ();
}

Plane3D::~Plane3D ()	{
	
}

bool Plane3D::isInFront (Vector3D pt) const	{
	return (apply (pt) >= 0);
}

double Plane3D::apply (Vector3D pt) const	{
	return _normal.dot (pt) + _offset;
}

/* Plane: - normal (a, b, c)
 *		  - eqn: ax + by + cz + _offset = 0
 * Line: A (Ax, Ay, Az), B (Bx, By, Bz)
 *		=>  { x = (Bx - Ax)t + a'
 *			{ y = (By - Ay)t + b'	=> let's say that (a', b', c') = A (Ax, Ay, Az)
 *			{ z = (Bz - Az)t + c'
 *
 * Inject line into plane: 
 * [(Bx - Ax)t + Ax]a + [(By - Ay)t + Ay]b + [(Bz - Az)t + Az]c + _offset = 0
 * (Bx - Ax)ta + (By - Ay)tb + (Bz - Az)tc = -_offset - a Ax - b Ay - c Az
 *			-_offset - a Ax - b Ay - c Az
 * t = ——————————————————————————————————————
 *		(Bx - Ax)a + (By - Ay)b + (Bz - Az)c
 *
 * D the intersection point
 *		(					-_offset - a Ax - b Ay - c Az
 *		( x = (Bx - Ax) —————————————————————————————————————— + Ax
 *		(				 (Bx - Ax)a + (By - Ay)b + (Bz - Az)c
 *		(					-_offset - a Ax - b Ay - c Az
 *	D =	( y = (By - Ay) —————————————————————————————————————— + Ay
 *		(				 (Bx - Ax)a + (By - Ay)b + (Bz - Az)c
 *		(					-_offset - a Ax - b Ay - c Az
 *		( x = (Bz - Az) —————————————————————————————————————— + Az
 *		(				 (Bx - Ax)a + (By - Ay)b + (Bz - Az)c
 *
 * double t = (-_offset - (_normal.x*pt1.x) - (_normal.y*pt1.y) - (_normal.z*pt1.z)) /
 *		(((pt2.x - pt1.x)*_normal.x) + ((pt2.y - pt1.y)*_normal.y) + ((pt2.z - pt1.z)*_normal.z));
 */
Vector3D Plane3D::intersection (Vector3D pt1, Vector3D pt2) const	{
	double t = (-_offset - (_normal.x*pt1.x) - (_normal.y*pt1.y) - (_normal.z*pt1.z)) / (((pt2.x - pt1.x)*_normal.x) + ((pt2.y - pt1.y)*_normal.y) + ((pt2.z - pt1.z)*_normal.z));
	return ((pt2 - pt1) * t) + pt1;
}
Vector3D Plane3D::orthogonalPoint (Vector3D pt) const	{
	return intersection (pt, pt + _normal);
}



















