//
//  ModelProvider.cpp
//  GalacticWar
//
//  Created by Alexis on 15/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "ModelProvider.h"

ModelProvider* ModelProvider::_instance = NULL;

ModelProvider::ModelProvider ()	{
	if (!_instance)
		_instance = this;
}

ModelProvider::~ModelProvider ()	{
	for (auto it = _models.begin (); it != _models.end (); it++)
		delete it->second;
}

bool ModelProvider::addModel (std::string name, const float *vertices, const float *texture, const float *colors, const float *normals,
							  int nVertices, GLuint gltexture, GLenum drawMode,
							  int vertexSize, int colorSize, int textureSize)	{
	if (modelNamed (name))
		return false;
	
	VBO *model = new VBO (vertices, texture, colors, normals, nVertices, gltexture, drawMode, vertexSize, colorSize, textureSize);
	
	return addModel (name, model);
}

bool ModelProvider::addModel (std::string name, VBO *model)	{
	if (modelNamed (name) || !model)
		return false;
	return addModel (name, new Model (1.0, model));
}

bool ModelProvider::addModel (std::string name, Model *model)	{
	if (modelNamed (name) || !model)
		return false;
	
	_models.insert (std::pair<std::string, Model*>(name, model));
	return true;
}

Model* ModelProvider::modelNamed (std::string name)	{
	if (!_instance)	return NULL;
	
	if (_instance->_models.count (name) > 0)
		return _instance->_models[name];
	else
		return NULL;
}

void ModelProvider::update (double frameLength)	{
	for (auto it = _models.begin (); it != _models.end (); it++)
		it->second->update (frameLength);
}









