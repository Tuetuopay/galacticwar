//
//  Model.cpp
//  GalacticWar
//
//  Created by Alexis on 15/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "Model.h"

Model::Model (float period, VBO *firstFrame)	{
	_period = period;
	_time = 0.0;
	
	addFrame (firstFrame);
}

bool Model::addFrame (VBO *frame)	{
	if (!frame)	return false;
	
	_frames.push_back (frame);
	return true;
}
bool Model::addFrame (const float *vertices, const float *texture, const float *colors, const float *normals,
					  int nVertices, GLuint gltexture, GLenum drawMode,
					  int vertexSize, int colorSize, int textureSize)	{
	VBO *frame = new VBO (vertices, texture, colors, normals, nVertices, gltexture, drawMode, vertexSize, colorSize, textureSize);
	
	return addFrame (frame);
}

void Model::update (double frameLength)	{
	_time += frameLength;
	while (_time >= (_frames.size () * _period))
		_time -= _frames.size () * _period;
}
void Model::render (bool forceTexturing)	{
	if (_frames[_time / _period])
		_frames[_time / _period]->render (forceTexturing);
}

Model::~Model ()	{
	for (auto it = _frames.begin (); it != _frames.end (); it++)
		delete *it;
	_frames.clear ();
}