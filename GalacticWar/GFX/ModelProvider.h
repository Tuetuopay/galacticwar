//
//  ModelProvider.h
//  GalacticWar
//
//  Created by Alexis on 15/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _MODEL_PROVIDER_H
#define _MODEL_PROVIDER_H

#include "OSCompatibility.h"

#include "GFX/OpenGL/VBO.h"
#include "GFX/Model.h"

class ModelProvider	{
public:
	ModelProvider ();
	~ModelProvider ();
	
	bool addModel (std::string name, const float *vertices, const float *texture, const float *colors, const float *normals,
						  int nVertices, GLuint gltexture = 0, GLenum drawMode = GL_TRIANGLES,
						  int vertexSize = 3, int colorSize = 4, int textureSize = 2);
	bool addModel (std::string name, VBO *model);
	bool addModel (std::string nale, Model *model);
	
	void update (double frameLength);
	
	static inline ModelProvider* instance ()	{ return _instance; }
	
	static Model* modelNamed (std::string name);
	
private:
	std::map<std::string,Model*>	_models;
	
	static ModelProvider	*_instance;
	
};

#endif /* defined(_MODEL_PROVIDER_H) */