//
//  FBO.h
//  PokeFPS
//
//  Created by Alexis on 19/09/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _FBO_H
#define _FBO_H

#include "OSCompatibility.h"

class FBO
{
public:
	FBO (double screenWidth, double screenHeight);
	~FBO ();
	
	void bind ();
	void unbind ();
	
	void enable ();
	void disable ();
	
	void bindRender (GLint textureUnit);
	void bindDepth (GLint textureUnit);
	
	GLuint depthTexture ();
	GLuint renderTexture ();
	GLuint frameBuffer ();
	
private:
	GLuint _bufID, _depthTexture, _renderTexture;
};

#endif /* defined(_FBO_H) */
