//
//  VBO.cpp
//  PokeFPS
//
//  Created by Alexis on 17/08/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GFX/OpenGL/VBO.h"
#include "GLTextureBinder.h"

bool VBO::_arrayVertexEnabled = false;
bool VBO::_arrayTextureEnabled = false;
bool VBO::_arrayColorEnabled = false;
bool VBO::_arrayNormalEnabled = false;

VBO::VBO (const float *vertices, const float *texture, const float *colors, const float *normals,
		  int nVertices, GLuint gltexture, GLenum drawMode,
		  int vertexSize, int colorSize, int textureSize)
	: _nVertex(nVertices), _nVertData(vertexSize), _nTexData(textureSize), _nColorData (colorSize), _texture(gltexture), _mode(drawMode)
{
	_checkData();
	_isTexEnabled = _isColEnabled = _isNormEnabled = false;
	
	if (vertices)	{
		glGenBuffers (1, &_bufVertex);
		glBindBuffer (GL_ARRAY_BUFFER, _bufVertex);
		glBufferData (GL_ARRAY_BUFFER, sizeof (float) * _nVertex * _nVertData, vertices, GL_DYNAMIC_DRAW);
	}
	if (texture && gltexture)	{
		glGenBuffers (1, &_bufTexture);
		glBindBuffer (GL_ARRAY_BUFFER, _bufTexture);
		glBufferData (GL_ARRAY_BUFFER, sizeof(float) * _nVertex * _nTexData, texture, GL_DYNAMIC_DRAW);
		_isTexEnabled = true;
	}
	if (colors)	{
		glGenBuffers (1, &_bufColors);
		glBindBuffer (GL_ARRAY_BUFFER, _bufColors);
		glBufferData (GL_ARRAY_BUFFER, sizeof(float) * _nVertex * _nColorData, colors, GL_DYNAMIC_DRAW);
		_isColEnabled = true;
	}
	if (normals)	{
		glGenBuffers (1, &_bufNormals);
		glBindBuffer (GL_ARRAY_BUFFER, _bufNormals);
		glBufferData (GL_ARRAY_BUFFER, sizeof(float) * _nVertex * 3, normals, GL_DYNAMIC_DRAW);
		_isNormEnabled = true;
	}
	glBindBuffer (GL_ARRAY_BUFFER, 0);
}

VBO::~VBO ()
{
	glDeleteBuffers (1, &_bufVertex);
	disableTexture();
	disableColor();
	disableNormals();
}

void VBO::render(bool forceTexturing)	{
	if (!_arrayVertexEnabled)	glEnableClientState (GL_VERTEX_ARRAY);
	
	glBindBuffer (GL_ARRAY_BUFFER, _bufVertex);
	glVertexPointer (_nVertData, GL_FLOAT, 0, BUFFER_OFFSET(0));
	
	if (_isTexEnabled && (_texture || forceTexturing))	{
		glEnable (GL_TEXTURE_2D);
		if (!_arrayTextureEnabled) glEnableClientState (GL_TEXTURE_COORD_ARRAY);
		if (!forceTexturing)	GLTextureBinder::bindTexture (_texture);	// glBindTexture (GL_TEXTURE_2D, _texture);
		glBindBuffer (GL_ARRAY_BUFFER, _bufTexture);
		glTexCoordPointer (_nTexData, GL_FLOAT, 0, BUFFER_OFFSET(0));
	} else {
		glDisable (GL_TEXTURE_2D);
		if (_arrayTextureEnabled)	glDisableClientState (GL_TEXTURE_COORD_ARRAY);
	}
	
	if (_isColEnabled)	{
		if (!_arrayColorEnabled) glEnableClientState (GL_COLOR_ARRAY);
		glBindBuffer (GL_ARRAY_BUFFER, _bufColors);
		glColorPointer (_nColorData, GL_FLOAT, 0, BUFFER_OFFSET(0));
	} else
		if (_arrayColorEnabled)	glDisableClientState (GL_COLOR_ARRAY);
	
	if (_isNormEnabled)	{
		if (!_arrayNormalEnabled) glEnableClientState (GL_NORMAL_ARRAY);
		glBindBuffer (GL_ARRAY_BUFFER, _bufNormals);
		glNormalPointer (GL_FLOAT, 0, BUFFER_OFFSET(0));
	} else
		if (_arrayNormalEnabled)	glDisableClientState (GL_NORMAL_ARRAY);
	
	glDrawArrays (_mode, 0, _nVertex);
	
	glBindBuffer (GL_ARRAY_BUFFER, 0);
	GLTextureBinder::bindTexture (0);
}

void VBO::_checkData()	{
	/* Vertex pointer: 2, 3, 4 */
	if (_nVertData < 2)	_nVertData = 2;
	if (_nVertData > 4)	_nVertData = 4;
	/* Texture pointer: 1, 2, 3, 4 */
	if (_nTexData < 1)	_nTexData = 1;
	if (_nTexData > 4)	_nTexData = 4;
	/* Color pointer: 3, 4 */
	if (_nColorData < 3)	_nColorData = 3;
	if (_nColorData > 4)	_nColorData = 4;
}

void VBO::enableTexture (const float *data, GLuint texture, int nData)	{
	if (!(data && texture))
		return;
	
	_nTexData = nData;
	_checkData();
	
	if (!_bufTexture)
		glGenBuffers (1, &_bufTexture);
	glBindBuffer (GL_ARRAY_BUFFER, _bufTexture);
	glBufferData (GL_ARRAY_BUFFER, sizeof(float) * _nVertex * _nTexData, data, GL_DYNAMIC_DRAW);
	_isTexEnabled = true;
	
	glBindBuffer (GL_ARRAY_BUFFER, 0);
}

void VBO::disableTexture()	{
	if (!_isTexEnabled)
		return;
	glDeleteBuffers (1, &_bufTexture);
	_bufTexture = 0;
	_isTexEnabled = false;
}

void VBO::enableColor (const float *data, int nData)	{
	if (!data)
		return;
	_nColorData = nData;
	_checkData();
	
	if (!_bufColors)
		glGenBuffers (1, &_bufColors);
	glBindBuffer (GL_ARRAY_BUFFER, _bufColors);
	glBufferData (GL_ARRAY_BUFFER, sizeof(float) * _nVertex * _nColorData, data, GL_DYNAMIC_DRAW);
	_isColEnabled = true;
	
	glBindBuffer (GL_ARRAY_BUFFER, 0);
}

void VBO::disableColor()	{
	if (!_isColEnabled)
		return;
	glDeleteBuffers (1, &_bufColors);
	_bufColors = 0;
	_isColEnabled = false;
}

void VBO::enableNormals (const float *data)	{
	if (!data)
		return;
	_checkData();
	
	if (!_bufNormals)
		glGenBuffers (1, &_bufNormals);
	glBindBuffer (GL_ARRAY_BUFFER, _bufNormals);
	glBufferData (GL_ARRAY_BUFFER, sizeof(float) * _nVertex * 3, data, GL_DYNAMIC_DRAW);
	_isNormEnabled = true;
	
	glBindBuffer (GL_ARRAY_BUFFER, 0);
}

void VBO::disableNormals()	{
	if (!_isNormEnabled)
		return;
	glDeleteBuffers (1, &_bufNormals);
	_bufNormals = 0;
	_isNormEnabled = false;
}

void VBO::updateVertexCoord (const float *data, int nData)	{
	if (!data)
		return;
	_nVertData = nData;
	_checkData();
	
	glBindBuffer (GL_ARRAY_BUFFER, _bufVertex);
	glBufferData (GL_ARRAY_BUFFER, sizeof(float) * _nVertex * _nVertData, data, GL_DYNAMIC_DRAW);	
	glBindBuffer (GL_ARRAY_BUFFER, 0);
}

void VBO::enableVBOs ()	{
	glEnableClientState (GL_VERTEX_ARRAY);
	glEnableClientState (GL_TEXTURE_COORD_ARRAY);
	glEnableClientState (GL_COLOR_ARRAY);
	glEnableClientState (GL_NORMAL_ARRAY);
	_arrayVertexEnabled = _arrayTextureEnabled = _arrayColorEnabled = _arrayNormalEnabled = true;
}
void VBO::disableVBOs ()	{
	glDisableClientState (GL_VERTEX_ARRAY);
	glDisableClientState (GL_TEXTURE_COORD_ARRAY);
	glDisableClientState (GL_COLOR_ARRAY);
	glDisableClientState (GL_NORMAL_ARRAY);
	_arrayVertexEnabled = _arrayTextureEnabled = _arrayColorEnabled = _arrayNormalEnabled = false;
}




















