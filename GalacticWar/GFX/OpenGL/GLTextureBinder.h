//
//  GLTextureBinder.h
//  PokeFPS
//
//  Created by Alexis on 05/10/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GL_TEXTURE_BINDER_H
#define _GL_TEXTURE_BINDER_H

#include "OSCompatibility.h"

class GLTextureBinder
{
public:
	GLTextureBinder ();
	~GLTextureBinder ();
	
	static void bindTexture (GLuint tex, int unit = 0);
	static void forceBindTexture (GLuint tex, int unit = 0);
	
	static void init ();
	
private:
	static std::vector<GLuint>	_activeTextures;
};

#endif /* defined(_GL_TEXTURE_BINDER_H) */
