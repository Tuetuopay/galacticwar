//
//  GLTextureBinder.cpp
//  PokeFPS
//
//  Created by Alexis on 05/10/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GLTextureBinder.h"

/* I thing a looooong time will pass before OpenGL implementations hit 256 texture unit ... GL_TEXTURE255 ! */
std::vector<GLuint> GLTextureBinder::_activeTextures (256);

GLTextureBinder::GLTextureBinder ()
{
	
}

GLTextureBinder::~GLTextureBinder ()
{
	
}

void GLTextureBinder::init ()	{
	for (int i = 0; i < 256; i++)
		_activeTextures.at (i) = 0;
}

void GLTextureBinder::bindTexture (GLuint tex, int unit)	{
	unit = (int)clamp (unit, 0, 255);
	if (tex != _activeTextures.at (unit))
		forceBindTexture (tex, unit);
}
void GLTextureBinder::forceBindTexture (GLuint tex, int unit)	{
	unit = (int)clamp (unit, 0, 255);
	glActiveTexture (GL_TEXTURE0 + unit);
	glBindTexture (GL_TEXTURE_2D, tex);
	_activeTextures.at (unit) = tex;
}
