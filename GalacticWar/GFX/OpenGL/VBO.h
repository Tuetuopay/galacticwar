//
//  GFX/VBO.h
//  PokeFPS
//
//  Created by Alexis on 17/08/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _VBO_H
#define _VBO_H

#include "OSCompatibility.h"

#ifndef BUFFER_OFFSET
# define BUFFER_OFFSET(a) ((char*)NULL + (a))
#endif

class VBO
{
public:
	VBO (const float *vertices, const float *texture, const float *colors, const float *normals,
		 int nVertices, GLuint gltexture = 0, GLenum drawMode = GL_TRIANGLES,
		 int vertexSize = 3, int colorSize = 4, int textureSize = 2);
	~VBO ();
	
	/* Rendering. If forceTexturing is enabled, the VBO won't override current bound texture */
	void render (bool forceTexturing = false);
	void setTexture (GLuint texture)	{ _texture = texture; }
	
	void updateVertexCoord (const float *data, int nData);
	/* Enablers */
	void enableTexture (const float *data, GLuint texture, int nData);
	void disableTexture ();
	void enableColor (const float *data, int nData);
	void disableColor ();
	void enableNormals (const float *data);
	void disableNormals ();
	
	static void enableVBOs ();
	static void disableVBOs ();
	
private:
	GLuint	_bufVertex, _bufTexture, _bufColors, _bufNormals,		/* VBOs */
			_texture;												/* Texture */
	int _nVertex,											/* Amount of vertices to render */
		_nVertData, _nTexData, _nColorData;		/* Amount of info per vertex (ex: amount of coordinates for texture) */
	bool _isTexEnabled, _isColEnabled, _isNormEnabled;
	GLenum _mode;
	
	/* Will check _nVertData, _nTexData and _nColorData */
	void _checkData ();
	
	static bool _arrayVertexEnabled, _arrayTextureEnabled, _arrayColorEnabled, _arrayNormalEnabled;
};

#endif /* defined(__PokeFPS__VBO__) */
