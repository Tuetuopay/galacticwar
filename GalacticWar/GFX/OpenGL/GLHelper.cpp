//
//  GLHelper.cpp
//  PokeFPS
//
//  Created by Alexis on 14/10/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GLHelper.h"

/* I thing a looooong time will pass before OpenGL implementations hit 256 texture unit ... GL_TEXTURE255 ! */
std::vector<GLuint> GLHelper::_activeTextures (256);

/* After a quick look in my GL.h, I havent't spotted any OpenGL constants passed
 * to glEnable[ClientState] > 0xFFFF ... We'll take a margin (exactly double the size, ~130KB) !  */
std::vector<bool>	GLHelper::_glEnabled (0x1FFFF);
std::vector<bool>	GLHelper::_glEnabledClientStates (0x1FFFF);

GLHelper::GLHelper ()	{
	
}

void GLHelper::init ()	{
	for (int i = 0; i < 256; i++)	_activeTextures.at (i) = 0;
	for (int i = 0; i < 0x1FFFF; i++)	{
		_glEnabled.at (i) = false;
		_glEnabledClientStates.at (i) = false;
	}
}

void GLHelper::bindTexture2D (GLuint tex, int unit)	{
	unit = (int)clamp (unit, 0, 255);
	if (tex != _activeTextures.at (unit))
		forceBindTexture2D (tex, unit);
}
void GLHelper::forceBindTexture2D (GLuint tex, int unit)	{
	unit = (int)clamp (unit, 0, 255);
	glActiveTexture (GL_TEXTURE0 + unit);
	glBindTexture (GL_TEXTURE_2D, tex);
	_activeTextures.at (unit) = tex;
}

void GLHelper::enable (GLenum prop)	{
	if (prop < 0x1FFFF)
		if (!_glEnabled.at (prop))
			glEnable (prop);
}
void GLHelper::disable (GLenum prop)	{
	if (prop < 0x1FFFF)
		if (_glEnabled.at (prop))
			glDisable (prop);
}

void GLHelper::enableClientState (GLenum prop)	{
	if (prop < 0x1FFFF)
		if (!_glEnabledClientStates.at (prop))
			glEnableClientState (prop);
}
void GLHelper::disableClientState (GLenum prop)	{
	if (prop < 0x1FFFF)
		if (_glEnabledClientStates.at (prop))
			glDisableClientState (prop);
}

void GLHelper::printGLError (int callLine)	{
	GLenum err = glGetError ();
	if (err)
		std::cout << "L" << callLine << " # # # # # GL ERROR " << err << " : " << gluErrorString (err) << " # # # # # \n";
}






