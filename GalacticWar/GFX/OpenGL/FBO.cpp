//
//  FBO.cpp
//  PokeFPS
//
//  Created by Alexis on 19/09/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "FBO.h"

#include "GLTextureBinder.h"

FBO::FBO (double screenWidth, double screenHeight)
{
	glGenFramebuffers (1, &_bufID);
	glGenTextures (1, &_depthTexture);
	glGenTextures (1, &_renderTexture);
	
	glBindFramebuffer (GL_FRAMEBUFFER, _bufID);
	
	GLTextureBinder::bindTexture (_depthTexture);
	// glBindTexture (GL_TEXTURE_2D, _depthTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);		/* No texel interpolation */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);	/* Clamping tex coord to [0.0, 1.0] */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, screenWidth, screenHeight, 0, GL_DEPTH_COMPONENT,	GL_FLOAT, 0);
	
	GLTextureBinder::bindTexture (_renderTexture);
	// glBindTexture (GL_TEXTURE_2D, _renderTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);		/* No texel interpolation */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA8, screenWidth, screenHeight, 0, GL_BGRA, GL_UNSIGNED_BYTE, 0);
	
	// glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, TextureID, 0/*mipmap level*/);
	glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _renderTexture, 0 /* < mipmap level */);
	glFramebufferTexture2D (GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,  GL_TEXTURE_2D, _depthTexture,  0 /* < mipmap level */);
	
	// glDrawBuffer (GL_BACK);
	// glReadBuffer (GL_BACK);
	
	GLTextureBinder::bindTexture (0);
	// glBindTexture (GL_TEXTURE_2D, 0);
	glBindFramebuffer (GL_FRAMEBUFFER, 0);
}

void FBO::enable()	{
	glDisable(GL_TEXTURE_2D);
	GLTextureBinder::bindTexture (0);
	// glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, _bufID);
	 // glClear(GL_DEPTH_BUFFER_BIT);	/* Already done by the engine */
	//glColorMask(false, false, false, false);
	// glCullFace(GL_BACK);				/* Already done in the engine */
}
void FBO::disable()	{
	
}

void FBO::bindRender (GLint textureUnit)	{
	GLTextureBinder::bindTexture (_renderTexture, textureUnit);
	// glActiveTexture (GL_TEXTURE0 + textureUnit);
	// glBindTexture(GL_TEXTURE_2D, _renderTexture);
}
void FBO::bindDepth (GLint textureUnit)	{
	GLTextureBinder::bindTexture (_depthTexture, textureUnit);
	// glActiveTexture (GL_TEXTURE0 + textureUnit);
	// glBindTexture(GL_TEXTURE_2D, _depthTexture);
}

GLuint FBO::depthTexture()	{
	return _depthTexture;
}
GLuint FBO::renderTexture()	{
	return _renderTexture;
}
GLuint FBO::frameBuffer()	{
	return _bufID;
}

void FBO::bind()	{
	glBindFramebuffer (GL_FRAMEBUFFER, _bufID);
}
void FBO::unbind()	{
	glBindFramebuffer (GL_FRAMEBUFFER, 0);
}

FBO::~FBO ()
{
	glDeleteFramebuffers (1, &_bufID);
	glDeleteTextures (1, &_depthTexture);
	glDeleteTextures (1, &_renderTexture);
}
