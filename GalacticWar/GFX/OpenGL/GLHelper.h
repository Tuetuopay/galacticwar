//
//  GLHelper.h
//  PokeFPS
//
//  Created by Alexis on 14/10/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GLHELPER_H
#define _GLHELPER_H

#include "OSCompatibility.h"

class GLHelper	{
public:
	GLHelper ();
	
	static void init ();
	
	/* Texture Binding */
	static void bindTexture2D (GLuint tex, int unit);
	static void forceBindTexture2D (GLuint tex, int unit);
	
	/* Things to enable/disable */
	static void enable (GLenum prop);
	static void disable (GLenum prop);
	static void enableClientState (GLenum prop);
	static void disableClientState (GLenum prop);

	static void printGLError (int callLine);

private:
	bool _wasInit;
	
	/* Just to make this class as abstract */
	virtual void _abstracter () = 0;
	
	/* Texture Binding */
	static std::vector<GLuint>	_activeTextures;
	
	/* Enabling/disabling */
	static std::vector<bool>	_glEnabled;
	static std::vector<bool>	_glEnabledClientStates;
};

#endif /* defined(_GLHELPER_H) */