//
//  Log.cpp
//  GalacticWar
//
//  Created by Alexis on 21/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "Log.h"

#include <ctime>

std::mutex Log::Logger::mutex;

/* Those two come from http://www.cplusplus.com/forum/general/54588/#msg294798 */
/* Handling std::endl, std::ends and std::flush */
Log::Logger &Log::operator<< (Log::Logger &l, std::ostream & (*manip)(std::ostream &)) {
	l._time = true;
	manip (std::cout);
	Log::Logger::mutex.unlock ();
	return l;
}
/* Handling std::boolalpha, std::hex and std::scientific */
Log::Logger &Log::operator<< (Log::Logger &l, std::ostream & (*manip)(std::ios_base &)) {
	Log::Logger::mutex.lock ();
	manip (std::cout);
	Log::Logger::mutex.unlock ();
	return l;
}

template<class T>
std::string to_string_2 (const T& v)	{
	std::string ret = std::to_string (v);
	if (ret.length () == 1)
		ret = "0" + ret;
	return ret;
}

std::string Log::getTime ()	{
	time_t t = time (nullptr);
    struct tm * now = localtime (&t);
	std::string res;
	res += to_string_2 (now->tm_mday);
	res += "/" + to_string_2 (now->tm_mon + 1);
	res += " " + to_string_2 (now->tm_hour);
	res += ":" + to_string_2 (now->tm_min);
	res += ":" + to_string_2 (now->tm_sec);
	// delete now;
	return res;
}
