//
//  Thread.h
//  PokeFPS
//
//  Created by Alexis on 12/10/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _THREAD_H
#define _THREAD_H

#include "OSCompatibility.h"

class Thread	{
public:
	typedef void *(*ThreadFunction)(void *params);
	
	Thread ();
	Thread (Thread::ThreadFunction func, void *data);
	~Thread ();
	
	virtual void run ();
	
	void start ();
	void stop ();
	void kill ();
	
	void join ();
	
private:
	pthread_t   _thread;
	ThreadFunction	_func;
	void *_funcData;
	
	static void* exec (void *thread);
};

#endif /* defined(_THREAD_H) */
