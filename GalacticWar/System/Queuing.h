//
//  Queuing.h
//  GalacticWar
//
//  Created by Alexis on 28/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _QUEUING_H
#define _QUEUING_H

#include <queue>

template<class T>
class Queuing	{
public:
	inline Queuing () {}
	
	inline bool queueEmpty () const { return _q.empty (); }
	inline size_t queueSize () const { return _q.size (); }
	inline T& queueFront () { return _q.front (); }
	inline const T& queueFront () const { return _q.front (); }
	inline T& queueBack () { return _q.back (); }
	inline const T& queueBack () const { return _q.back (); }
	
	inline void queuePush (const T& v) { _q.push (v); }
	inline void queuePush (T&& v) { _q.push (v); }
	
	inline void queuePop () { _q.pop (); }
	
protected:
	std::queue<T> _q;
};

#endif /* defined (_QUEUING_H) */