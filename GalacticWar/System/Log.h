//
//  Log.h
//  GalacticWar
//
//  Created by Alexis on 21/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _LOG_H
#define _LOG_H

#include <mutex>
#include <iostream>

namespace Log {
	
	std::string getTime ();
	
	class Logger	{
	public:
		Logger (bool nl) : nl(nl), _time(true) {}
		~Logger () {}
		
		template<class T>
		inline Logger& operator<< (const T& t) {
			if (_time)	{
				mutex.lock ();
				std::cout << "[" << getTime () << "] ";
				_time = false;
			}
			std::cout << t;
			if (nl)	std::cout << std::endl;
			return *this;
		}
		
		/* Those two come from http://www.cplusplus.com/forum/general/54588/#msg294798 */
		/* Handling std::endl, std::ends and std::flush */
		friend Logger &operator<< (Logger &l, std::ostream & (*manip)(std::ostream &));
		/* Handling std::boolalpha, std::hex and std::scientific */
		friend Logger &operator<< (Logger &l, std::ostream & (*manip)(std::ios_base &));
		
	private:
		bool nl, _time;
		static std::mutex mutex;
	};
	
	static Logger log (false), lognl (true);
}

#endif /* defined (_LOG_H) */
