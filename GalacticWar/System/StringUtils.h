//
//  OSCompatibility.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _STRING_UTILS_H
#define _STRING_UTILS_H

#include "OSCompatibility.h"

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
std::vector<std::string> split(const std::string &s, char delim);

#endif /* defined(_STRING_UTILS_H) */