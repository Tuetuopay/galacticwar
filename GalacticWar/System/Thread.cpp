//
//  Thread.cpp
//  PokeFPS
//
//  Created by Alexis on 12/10/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "Thread.h"

Thread::Thread ()   {
#ifndef __WIN32__
	_thread = 0;	/* Damn you windows >_< */
#endif
	_func = NULL;
	_funcData = NULL;
}
Thread::Thread (Thread::ThreadFunction func, void *data)	{
#ifndef __WIN32__
	_thread = 0;	/* Damn you windows >_< */
#endif
	_func = func;
	_funcData = data;
}

void Thread::run ()	{
	if (_func)
		(*_func) (_funcData);
}
void Thread::start ()	{
	pthread_create (&_thread, NULL, Thread::exec, (void*)this);
}
void Thread::stop ()	{
	pthread_kill (_thread, SIGSTOP);
}
void Thread::kill ()	{
	pthread_kill (_thread, SIGKILL);
}
void Thread::join ()	{
	pthread_join (_thread, nullptr);
}

void* Thread::exec (void *thread)	{
	if (thread)
		((Thread*)thread)->run ();
	
	return NULL;
}

Thread::~Thread()	{
}
