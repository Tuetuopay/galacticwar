//
//  NetworkGUI.h
//  GalacticWar
//
//  Created by Alexis on 19/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _NETWORK_GUI_H
#define _NETWORK_GUI_H

#include "OSCompatibility.h"

#include "GUI/GUILib/GUILib.h"

class NetworkGUI : public GUIScreen	{
public:
	NetworkGUI ();
	~NetworkGUI ();

	GUI_SIGNAL (onClient, onClientS)
	GUI_SIGNAL (onServer, onServerS)
	
	int port;
	std::string host () { return _teHost->text (); }
	
private:
	GUILabel *_lblTitle;
	
	GUIButton *_btnServer, *_btnClient;
	GUITextEdit *_teHost;
	
	GUISpinBox *_sbPort;
	
	GUI_SLOT (on_btnClient_clicked, NetworkGUI)
	GUI_SLOT (on_btnServer_clicked, NetworkGUI)
	GUI_SLOT (on_sbPort_changed, NetworkGUI)
};

#endif /* defined (_NETWORK_GUI_H) */