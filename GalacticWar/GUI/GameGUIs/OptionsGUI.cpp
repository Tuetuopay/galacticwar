//
//  OptionsGUI.cpp
//  GalacticWar
//
//  Created by Alexis on 13/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "OptionsGUI.h"

#include "Game/GameEngine.h"
#include "Game/Entities/Ship.h"

OptionsGUI::OptionsGUI () : GUIScreen (20, 11)	{
	_lblTitle = new GUILabel (1, 1, "Options", this);
	_lblTitle->centerHorizontally ();
	addElement (_lblTitle);
	
	_lblShipAcc = new GUILabel (1, 3, "Ship acceleration", this);
	addElement (_lblShipAcc);
	_sbShipAcc = new GUISpinBox (10, 3, this, 1, 4);
	_sbShipAcc->stickToRight ();
	addElement (_sbShipAcc);
	
	_lblRocketAcc = new GUILabel (1, 5, "Rocket acceleration", this);
	addElement (_lblRocketAcc);
	_sbRocketAcc = new GUISpinBox (10, 5, this, 1, 4);
	_sbRocketAcc->stickToRight ();
	addElement (_sbRocketAcc);
	
	_lblEdgeMode = new GUILabel (1, 7, "Edge mode", this);
	addElement (_lblEdgeMode);
	_cbEdgeMode = new GUICheckBox (14.5, 7, "Wrap", this);
	addElement (_cbEdgeMode);
	
	_btnCancel = new GUIButton (1, 9, "Cancel", this);
	_btnCancel->stickToRight ();
	addElement (_btnCancel);
	_btnOk = new GUIButton (1, 9, "Ok", this);
	_btnOk->stickToRight (_btnCancel);
	addElement (_btnOk);
	
	_btnCancel->onClick (on_btnCancel_clicked, this);
	_btnOk->onClick (on_btnOk_clicked, this);
	
	_sbShipAcc->valueChanged (on_sbShipAcc_valueChanged, this);
	_sbRocketAcc->valueChanged (on_sbRocketAcc_valueChanged, this);
	
	_cbEdgeMode->onCheck (on_cbEdgeMode_onCheck, this);
}

OptionsGUI::~OptionsGUI ()	{
	
}

void OptionsGUI::on_btnCancel_clicked ()	{
	hide (true);
}
void OptionsGUI::on_btnOk_clicked ()	{
	hide (true);
}

void OptionsGUI::on_sbShipAcc_valueChanged ()	{
	Ship::shipAcc = _sbShipAcc->value ();
}
void OptionsGUI::on_sbRocketAcc_valueChanged ()	{
	Ship::rocketAcc = _sbRocketAcc->value ();
}

void OptionsGUI::on_cbEdgeMode_onCheck ()	{
	GameEngine::instance ()->wrapAround (_cbEdgeMode->isToggled ());
}











