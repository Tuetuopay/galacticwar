//
//  DebugGUI.cpp
//  GalacticWar
//
//  Created by Alexis on 13/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "DebugGUI.h"
#include "System/SystemProfiler.h"

#include "Game/GameEngine.h"

DebugGUI::DebugGUI () : GUIScreen (0, 0)	{
	label = new GUILabel (1.0, 1.0, "Is GalaxyWar working ?\nThis is a \"Hello world I guess ...\nFair enough !", this);
	addElement (label);
	
	show ();
}

DebugGUI::~DebugGUI ()	{
	
}

void DebugGUI::update (GameData *gameData)	{
	static std::string RAM = "";
	
	std::string text;
	
	RAM = SystemProfiler::ramToString (SystemProfiler::currentRAMUsage());
	
	text += "GalaxyWar Indev - " + RAM;
	
	text += "\n\n";
	
	text += std::string ("Pos ") + gameData->ship->pos ().describe() + "\n";
	text += std::string ("Spe ") + gameData->ship->spe ().describe() + "\n";
	text += std::string ("Acc ") + gameData->ship->acc ().describe() + "\n";
	
	text += "Linear speed: " + std::to_string (gameData->ship->spe ().length()) + "\n";
	text += "Angle: " + std::to_string (gameData->ship->rot ()) + "\n";
	text += "Shot rockets: " + std::to_string (GameEngine::instance ()->entities ().size () - 1) + "\n";
	
	label->setText (text);
}

