//
//  DebugGUI.h
//  GalacticWar
//
//  Created by Alexis on 13/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _DEBUG_GUI_H
#define _DEBUG_GUI_H

#include <iostream>

#include "GUI/GUILib/GUILib.h"
#include "Game/GameData.h"


class DebugGUI : public GUIScreen	{
public:
	DebugGUI ();
	~DebugGUI ();
	
	void update (GameData *gameData);
	
private:
	GUILabel *label;
};

#endif /* defined(_DEBUG_GUI_H) */