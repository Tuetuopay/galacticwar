//
//  ScoresGUI.h
//  GalacticWar
//
//  Created by Alexis on 16/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _ScoresGUI_H
#define _ScoresGUI_H

#include "Game/GameData.h"
#include "GUI/GUILib/GUILib.h"

class ScoresGUI : public GUIScreen	{
public:
	ScoresGUI ();
	~ScoresGUI ();
	
	void update (GameData *_gameData);
	
private:
	GUILabel	*_lblTitle, *_lblScore1, *_lblScore2;
};

#endif /* defined(_ScoresGUI_H) */