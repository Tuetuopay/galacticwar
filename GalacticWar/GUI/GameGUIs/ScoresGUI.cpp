//
//  ScoresGUI.cpp
//  GalacticWar
//
//  Created by Alexis on 16/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "ScoresGUI.h"

ScoresGUI::ScoresGUI () : GUIScreen (10, 7)	{
	_lblTitle = new GUILabel (1, 1, "-= Scores =-", this);
	_lblTitle->centerHorizontally ();
	addElement (_lblTitle);
	
	_lblScore1 = new GUILabel (1, 3, "Player 1 : 0", this);
	addElement (_lblScore1);
	
	_lblScore2 = new GUILabel (1, 5, "Player 2 : 0", this);
	addElement (_lblScore2);
	
	show ();
}

void ScoresGUI::update (GameData *gameData)	{
	_lblScore1->setText ("Player 1 : " + std::to_string (gameData->scores[0]));
	_lblScore2->setText ("Player 2 : " + std::to_string (gameData->scores[1]));
}

ScoresGUI::~ScoresGUI ()	{
	
}
