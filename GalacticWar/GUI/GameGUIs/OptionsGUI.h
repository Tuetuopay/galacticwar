//
//  OptionsGUI.h
//  GalacticWar
//
//  Created by Alexis on 13/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _OPTIONS_GUI_H
#define _OPTIONS_GUI_H

#include "OSCompatibility.h"
#include "GUI/GUILib/GUILib.h"

class OptionsGUI : public GUIScreen	{
public:
	OptionsGUI ();
	~OptionsGUI ();
	
	GUI_SLOT(on_btnCancel_clicked, OptionsGUI)
	GUI_SLOT(on_btnOk_clicked, OptionsGUI)
	GUI_SLOT(on_sbShipAcc_valueChanged, OptionsGUI)
	GUI_SLOT(on_sbRocketAcc_valueChanged, OptionsGUI)
	GUI_SLOT(on_cbEdgeMode_onCheck, OptionsGUI)
	
private:
	GUILabel	*_lblTitle, *_lblShipAcc, *_lblRocketAcc, *_lblEdgeMode;
	GUISpinBox	*_sbShipAcc, *_sbRocketAcc;
	GUICheckBox	*_cbEdgeMode;
	GUIButton	*_btnOk, *_btnCancel;
};

#endif /* defined(_OPTIONS_GUI_H) */