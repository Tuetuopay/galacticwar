//
//  NetworkGUI.cpp
//  GalacticWar
//
//  Created by Alexis on 19/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "NetworkGUI.h"

NetworkGUI::NetworkGUI () : GUIScreen (17, 7)	{
	_lblTitle = new GUILabel (1, 1, "Multiplayer", this);
	_lblTitle->centerHorizontally ();
	addElement (_lblTitle);
	
	_teHost = new GUITextEdit (10, 1, 1, 3, "localhost", this);
	addElement (_teHost);
	
	_sbPort = new GUISpinBox (10, 3, this, 1025, 65564);
	_sbPort->setValue (5400);
	addElement (_sbPort);
	port = 5400;
	
	_btnClient = new GUIButton (1, 5, "Client", this);
	_btnClient->stickToRight ();
	addElement (_btnClient);
	
	_btnServer = new GUIButton (1, 5, "Server", this);
	_btnServer->stickToRight (_btnClient);
	addElement (_btnServer);
	
	_btnClient->onClick (on_btnClient_clicked, this);
	_btnServer->onClick (on_btnServer_clicked, this);
	_sbPort->valueChanged (on_sbPort_changed, this);
	
	show ();
}
						 
void NetworkGUI::on_btnClient_clicked ()	{
	GUI_SendSignal (onClientS);
}
void NetworkGUI::on_btnServer_clicked ()	{
	GUI_SendSignal (onServerS);
}
void NetworkGUI::on_sbPort_changed ()	{
	port = _sbPort->value ();
}

NetworkGUI::~NetworkGUI ()	{
	
}