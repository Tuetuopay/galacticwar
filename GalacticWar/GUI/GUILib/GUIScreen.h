//
//  GUIScreen.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_SCREEN_H
#define _GUI_SCREEN_H

#include "OSCompatibility.h"

#include "GUI.h"
#include "GUIElement.h"

#define TRANSITION_DURATION	0.5

class GUIScreen : public GUI
{
public:
	/* Same as GUI's constructor, look in GUI.h */
	GUIScreen (double width, double height, double x = 0.0, double y = 0.0, GUI* parent = NULL);
	~GUIScreen ();
	
	/* Renders the current GUI screen */
	void render ();
	
	/* Event managing */
	bool processMouseButton (double x, double y, SDL_MouseButtonEvent event);
	void mouseMotion (double x, double y, bool isClicking);
	void processKeypress (SDL_KeyboardEvent keyboardEvent);
	void processTextInput (SDL_TextInputEvent textEvent);
	bool processMouseWheel (SDL_MouseWheelEvent wheelEvent, int x, int y);
	
	/* So that GUI Screens may add additional events processing */
	virtual bool _processMouseButton (double x, double y, SDL_MouseButtonEvent)	{ return isInsideMe (x, y); }
	virtual void _mouseMotion (double x, double y, bool isClicking)		{}
	virtual void _processKeypress (SDL_KeyboardEvent keyboardEvent)		{}
	virtual void _processTextInput (SDL_TextInputEvent textEvent)		{}
	virtual void _processMouseWheel (SDL_MouseWheelEvent wheelEvent, int x, int y)		{}
	
	/* GUI visibility */
	void show (bool animate = false);
	void hide (bool animate = false);
	inline void setShown (bool shown)	{ _isShown = shown; }
	inline bool isShown ()	{ return _isShown; }
	
	/* GUI data */
	bool setBackground (const std::string& filename);
	bool setBackground (GLuint texture);
	inline void setBackgroundColor	(GLcolor color)	{ _bgColor = color; }
	inline void setOutlineColor		(GLcolor color)	{ _olColor = color; }
	inline void setBackgroundColor	(float r, float g, float b, float a) { _bgColor.r = r; _bgColor.g = g; _bgColor.b = b; _bgColor.a = a; }
	inline void setOutlineColor		(float r, float g, float b, float a) { _olColor.r = r; _olColor.g = g; _olColor.b = b; _olColor.a = a; }
	
	/* Returns tthe description */
	std::string describe ()	{ return _description; }
	void setDescription (const std::string& description)	{ _description = description; }
	
public:
	/* Will push this element onto te GUI screen */
	void addElement (GUIElement *guiElement);
	
private:
	/* Stores everything inside this GUI screen */
	std::vector<GUIElement*>	_guiElements;
	
	/* If this is the current GUI shown */
	bool _isShown;
	
	/* Time when the GUI was shown */
	float _showBeginning;
	float _hideBeginning;
	
protected:
	/* Background of the GUI */
	GLcolor _bgColor, _olColor;
	GLuint  _bgTexture;
};

#endif /* defined(_GUI_SCREEN_H) */
