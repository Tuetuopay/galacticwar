//
//  GUI.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUI.h"

VBO * GUI::_vboSquareFilled = NULL;
VBO * GUI::_vboSquareEmpty = NULL;

double GUI::screenGLHeight = 0.0;
double GUI::screenGLWidth = 0.0;
int GUI::screenPXWidth = 0;
int GUI::screenPXHeight = 0;


GUI::GUI (GUI* parent) : _width (1.0), _height (1.0), _x (0.0), _y (0.0), _textEngine(), _parent(parent)
{
	_buildVBOs();
}
GUI::GUI (GUI* parent, double width, double height, double x, double y)
		: _width(width), _height(height), _x(x), _y(y), _textEngine (), _parent(parent)
{
	_buildVBOs();
}

GUI::~GUI ()
{}

void GUI::resize (double width, double height)
{
	_width = width;		_height = height;
}

void GUI::move (double x, double y)
{
	_x = x;				_y = y;
}

void GUI::moveCenter (double x, double y)
{
	move (x - _width / 2.0, y - _height / 2.0);
}

void GUI::moveToScreenCenter()	{
	if (screenGLWidth == 0 || screenGLHeight == 0)	return;
	
	moveCenter (screenGLWidth / 2.0, screenGLHeight / 2.0);
}

void GUI::centerHorizontally()	{
 	_x = ((_parent) ? _parent->width() : screenGLWidth) / 2.0 - _width / 2.0;
}
void GUI::centerVertically()	{
	_y = ((_parent) ? _parent->height() : screenGLHeight) / 2.0 - _height / 2.0;
}

void GUI::stickToUp (GUI *target)	{
	if (target == NULL || target == _parent)
		_y = 1.0;
	else
		_y = target->y () + target->height () + 1.0;
}
void GUI::stickToDown (GUI *target)	{
	if (target == NULL || target == _parent)
		_y = ((_parent) ? _parent->height () : screenGLHeight) - _height - 1.0;
	else
		_y = target->y () - _height - 1.0;
}
void GUI::stickToLeft (GUI *target)	{
	if (target == NULL || target == _parent)
		_x = 1.0;
	else
		_x = target->x () + target->width () + 1.0;
}
void GUI::stickToRight (GUI *target)	{
	if (target == NULL || target == _parent)
		_x = ((_parent) ? _parent->width () : screenGLWidth) - _width - 1.0;
	else
		_x = target->x () - _width - 1.0;
}

double GUI::absX()	{
	if (_parent) return _parent->absX() + _x;
	else return _x;
}
double GUI::absY()	{
	if (_parent)	return _parent->absY() + _y;
	else return _y;
}

void GUI::render()
{
	
}

void GUI::mouseMotion (double x, double y, bool isClicking)
{
	
}

bool GUI::processMouseButton (double x, double y, SDL_MouseButtonEvent event)	{
	/* If the click was made inside the screen */
	return isInsideMe (x, y);
}

void GUI::processKeypress (SDL_KeyboardEvent keyboardEvent)
{
	
}

void GUI::processTextInput (SDL_TextInputEvent textEvent)	{
	
}

bool GUI::processMouseWheel (SDL_MouseWheelEvent wheelEvent, int x, int y)	{
	return isInsideMe (x, y);
}

bool GUI::isInsideMe (float x, float y)	{
	return (x >= _x && x <= _x + _width && y >= _y && y <= _y + _height);
}

std::string GUI::hierarchy()	{
	if (_parent)
		return _parent->hierarchy() + " > " + _description;
	else
		return "* " + _description;
}

void GUI::_buildVBOs()	{
	/* Building the VBOs */
	GLfloat vert[] = {
		0.0, 0.0,
		0.0, 1.0,
		1.0, 1.0,
		1.0, 0.0
	}, tex[] = {
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0
	};
	if (!_vboSquareEmpty)
	/* Well ... we'll set a texture at GL ID 1 because else the VBO class won't build the texture buffer */
		_vboSquareEmpty = new VBO (vert, tex, NULL, NULL, 4, 1, GL_LINE_LOOP, 2);
	if (!_vboSquareFilled)
		_vboSquareFilled = new VBO (vert, tex, NULL, NULL, 4, 1, GL_QUADS, 2);
}
void GUI::drawSquare (double w, double h, bool filled, GLuint texture)	{
	glPushMatrix();
	glScaled (w, h, 1.0);
	if (filled)	{
		_vboSquareFilled->setTexture (texture);
		_vboSquareFilled->render();
		_vboSquareFilled->setTexture (0);
	}	else	{
		_vboSquareEmpty->setTexture (texture);
		_vboSquareEmpty->render();
		_vboSquareEmpty->setTexture (0);
	}
	glPopMatrix();
}

/* (ax - b) ^3 + (ax - b) ^2 + 1
 * a = cube root of pi
 * b = a
 * approx. 1.4645
 */

float GUI::popTransitionScale (float time, float duration)
{
	const float a = cbrtf (M_PI);
	
	time *= 1.0 / duration;
	
	if (time >= 1.0)
		return 1.0;
	else if (time <= 0.0)
		return 0.0;
	else
		return (a * time - a) * (a * time - a) * (a * time - a) + (a * time - a) * (a * time - a) + 1.0;
}

/* (x - 1) ^3
 * —————————— + 1.25
 *     4
 */

double GUI::hoverScale (double time, double duration)	{
	if (duration != 0.0)
		time *= 1.0 / duration;
	
	if (time >= 1.0)
		return 1.25;
	else if (time <= 0.0)
		return 1.0;
	else
		return ((time -1.0)*(time -1.0)*(time -1.0) / 6.0) + 1.25;
}











