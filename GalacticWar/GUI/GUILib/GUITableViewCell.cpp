//
//  GUITableViewCell.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUITableViewCell.h"

GUITableViewCell::GUITableViewCell (std::string label, GUI* parent, double width)
		: GUIButton (0.0, 0.0, label, parent)
{
	if (width > _width)
		resize (width, _height);
	_height = GUI_CELL_DEFAULT_HEIGHT;
	
	setToggleable (true);
	setZoomOnHover (false);
	
	_description = "GUITableViewCell";
}

GUITableViewCell::~GUITableViewCell ()
{
	
}

/*/
void GUITableViewCell::render ()	{
	glPushMatrix();
	glTranslatef (_x, _y, 0.0);
	
	* Background of the cell *//*
	if (!_isToggled)
		_textEngine.drawBackground ((float)_width, 0.5, 0.5, 0.5, 0.4, 0.3, 0.3, 0.3, 0.8);
	else
		_textEngine.drawBackground ((float)_width, 0.9, 0.9, 0.9, 0.8, 0.7, 0.7, 0.7, 0.8);
	
	glTranslatef (0.4, 0.1, 0.0);
	_textEngine.drawString (_text, 0.0, 0.0, 0.0);
	glPopMatrix();
}
//*/