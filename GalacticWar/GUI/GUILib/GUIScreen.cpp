//
//  GUIScreen.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUIScreen.h"

#include "GUIEngine.h"

GUIScreen::GUIScreen (double width, double height, double x, double y, GUI* parent) : GUI(parent, width, height, x, y)
{
	GLcolor bgColor = {0, 0, 0, 0.5};
	GLcolor olColor = {0};	/* No outline */
	
	_bgColor = bgColor;
	_olColor = olColor;
	
	_isShown = false;
	
	_bgTexture = 0;
	
	_showBeginning = TRANSITION_DURATION * -2;
	_hideBeginning = TRANSITION_DURATION * -2;
	
	GUIEngine::addGUIScreen (this);
	
	_description = "GUIScreen";
}

GUIScreen::~GUIScreen ()
{
	for (int i = 0; i < _guiElements.size(); i++)
		delete _guiElements.at (i);
	
	if (_bgTexture)
		glDeleteTextures (1, &_bgTexture);
	
	/* This is telling te GUI Engine that we were deleted. Useful for:
	 * - manually deleting screens, this way the GUI Engine won't try to access it
	 */
	GUIEngine::screenDeleted (this);
}

void GUIScreen::addElement (GUIElement *guiElement)
{
	_guiElements.push_back (guiElement);
}

void GUIScreen::render()
{
	glPushMatrix();
	
	glTranslatef (_x + _width / 2.0, _y + _height / 2.0, 0.0);
	
	if (!_isShown)	{
		if ((float)SDL_GetTicks() / 1000.0 - _hideBeginning < TRANSITION_DURATION)	{
			float scale = popTransitionScale (TRANSITION_DURATION - (float)SDL_GetTicks() / 1000.0 + _hideBeginning, TRANSITION_DURATION);
			glScalef (scale, scale, 1.0);
		}
		else	{
			glPopMatrix(); /* We pushed a matrix a th function's beginning */
			return;
		}
	}
	
	/* The scaling to play an animation when opening the GUI screen */
	if ((float)SDL_GetTicks() / 1000.0 - _showBeginning < TRANSITION_DURATION)	{
		float scale = popTransitionScale ((float)SDL_GetTicks() / 1000.0 - _showBeginning, TRANSITION_DURATION);
		glScalef (scale, scale, 1.0);
	}
	
	/* Rendering the backgroud of the GUI */
	glTranslatef (-_width / 2.0, -_height / 2.0, 0.0);
	
	/* Background with its colors */
	glColor4f (_bgColor.r, _bgColor.g, _bgColor.b, _bgColor.a);
	drawSquare (_width, _height, true, _bgTexture);
	
	/* Outline */
	glColor4f (_olColor.r, _olColor.g, _olColor.b, _olColor.a);
	drawSquare (_width, _height, false);
	
	/* Rendering each element of the GUI */
	for (int i = 0; i < _guiElements.size(); i++)	{
		glPushMatrix();
		_guiElements.at (i)->render();
		glPopMatrix();
	}
	glPopMatrix();
}

bool GUIScreen::processMouseButton (double x, double y, SDL_MouseButtonEvent event)	{
	if (!_isShown)	return false;
	bool elementCaughtClick = false;
	for (int i = 0; i < _guiElements.size(); i++)
		if (_guiElements[i]->processMouseButton (x - _x, y - _y, event))
			elementCaughtClick = true;
	
	/* If the click was made inside the screen */
	return (isInsideMe (x, y) || elementCaughtClick);
}

void GUIScreen::mouseMotion (double x, double y, bool isClicking)
{
	if (!_isShown)	return;
	for (int i = 0; i < _guiElements.size(); i++)
		_guiElements.at (i)->mouseMotion (x - _x, y - _y, isClicking);
}
bool GUIScreen::processMouseWheel (SDL_MouseWheelEvent wheelEvent, int x, int y)	{
	if (!_isShown)	return false;
	bool elementCaughtClick = false;
	for (int i = 0; i < _guiElements.size(); i++)
		if (_guiElements[i]->processMouseWheel (wheelEvent, x - _x, y - _y))
			elementCaughtClick = true;
	
	/* If the "wheel click" was made inside the screen */
	return (isInsideMe (x, y) || elementCaughtClick);
}

void GUIScreen::processKeypress (SDL_KeyboardEvent keyboardEvent)
{
	if (!_isShown)	return;
	for (int i = 0; i < _guiElements.size(); i++)
		_guiElements.at (i)->processKeypress (keyboardEvent);
}
void GUIScreen::processTextInput (SDL_TextInputEvent textEvent)	{
	if (!_isShown)	return;
	for (int i = 0; i < _guiElements.size(); i++)
		_guiElements[i]->processTextInput (textEvent);
}

bool GUIScreen::setBackground (const std::string& filename)
{
	if (!(_bgTexture = loadTexture (filename.c_str())))	{
		std::cout << "Warning: unable to load background texture \"" << filename << "\". Using default OS X GUI color." << std::endl;
		GLcolor bgColor = {0.93, 0.93, 0.93, 1.0},
				olColor = {1.0, 1.0, 1.0, 1.0};
		_bgColor = bgColor;
		_olColor = olColor;
		
		return false;
	}
	
	return true;
}

bool GUIScreen::setBackground (GLuint texture)
{
	_bgTexture = texture;
	
	return texture != 0;
}

void GUIScreen::show(bool animate)
{
	if (!_isShown)		/* Preventing animation replay when GUI is already visible */
		_showBeginning = (float)SDL_GetTicks() / 1000.0; /* Time is given in ms */
	_isShown = true;
	
	if (!animate)	_showBeginning = TRANSITION_DURATION * -2;
}

void GUIScreen::hide(bool animate)
{
	if (_isShown)
		_hideBeginning = (float)SDL_GetTicks() / 1000.0; /* Time is given in ms */
	_isShown = false;
	
	if (!animate)	_hideBeginning = TRANSITION_DURATION * -2;
}

