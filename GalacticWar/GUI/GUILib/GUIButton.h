//
//  GUIButton.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_BUTTON_H
#define _GUI_BUTTON_H

#include "OSCompatibility.h"

#include "GUIElement.h"
#include "GFX/OpenGL/VBO.h"

class GUIButton : public GUIElement
{
public:
	GUIButton (float x, float y, GUI* parent = NULL);
	GUIButton (float x, float y, std::string title = "Button", GUI* parent = NULL);
	~GUIButton ();
	
	/* The string contained in a button */
	inline const std::string& text ()				{ return _text; }
	inline void setText (const std::string &string)	{ _text = string; }
	
	inline bool isToggleable ()				{ return _isToggleable; }
	inline void setToggleable (bool state)	{ _isToggleable = state; }
	inline bool isToggled ()				{ return (_isToggleable) ? _isToggled : false; }
	inline void setToggled (bool state)		{ _isToggled = (_isToggleable) ? state : _isToggled; }
	inline void toggle ()					{ _isToggled = (_isToggleable) ? !_isToggled : _isToggled; }
	inline bool zoomOnHover ()				{ return _zoomOnHover; }
	inline void setZoomOnHover (bool zoom)	{ _zoomOnHover = zoom; }
	
	/* Event processing */
	bool processMouseButton (double x, double y, SDL_MouseButtonEvent event);
	void mouseMotion (double x, double y, bool isClicking);
	void processKeypress (SDL_KeyboardEvent keyboardEvent);
	
	/* Some GUIs may need this ... */
	inline double clickX ()	{ return _clickX; }
	inline double clickY ()	{ return _clickY; }
	
	/* Rendering */
	void render ();
	
	/* Function to be called when the button is clicked. Prototype must be
	 * void callbackFunction (void)
	 ***/
	GUI_SIGNAL(onClick, onClickSignal)
	GUI_SIGNAL(toggledUp, toggledUpSignal)
	GUI_SIGNAL(toggledDown, toggledDownSignal)
	
	/* When making a GUI, it *could* be useful to make a default button triggered by the Enter Key */
	inline bool isDefault ()					{ return _isDefaultButton; }
	inline void setDefault (bool isDefault)		{ _isDefaultButton = isDefault; }
	
protected:
	/* Click related infos */
	bool _isClicked;
	bool _isToggled;
	bool _clickBeganOnMe;
	bool _isHovered;
	bool _zoomOnHover;
	
	double _hoverBeginning, _hoverEnding;
	
	bool _isToggleable;
	
	double _clickX, _clickY;
	
	/* When making a GUI, it *could* be useful to make a default button triggered by the Enter Key */
	bool _isDefaultButton;
	
	/* The text contained by the button */
	std::string _text;
	double _textWidth;	/* So that no need to recompute it every frame */
	
	void _setupButton ();
};

#endif /* defined(_GUI_BUTTON_H) */
