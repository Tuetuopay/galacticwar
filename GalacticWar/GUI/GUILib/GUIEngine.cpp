//
//  GUIEngine.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUIEngine.h"

std::vector<GUIScreen*>	GUIEngine::_guiScreens;
std::vector<GUIViewport*> GUIEngine::_guiViewports;

GUIEngine::GUIEngine ()	{
}

GUIEngine::~GUIEngine()	{
	for (int i = 0; i < _guiScreens.size(); i++)
		if (_guiScreens[i])
			delete _guiScreens.at (i);
}

void GUIEngine::addGUIScreen (GUIScreen *screen)	{
	_guiScreens.push_back (screen);
}
void GUIEngine::addGUIViewport (GUIViewport *viewport)	{
	_guiViewports.push_back (viewport);
}

void GUIEngine::renderAll()	{
	static float modelview[16];
	glGetFloatv(GL_MODELVIEW_MATRIX , modelview);
	
	for (int i = 0; i < _guiScreens.size(); i++)	{
		if (!_guiScreens[i])	continue;	/* If this screen has been manually deleted */
		glLoadMatrixf(modelview);
		glPushMatrix();
		_guiScreens.at (i)->render();
		glPopMatrix();
	}
}

void GUIEngine::renderViewports()	{
	glColor4f (1.0, 1.0, 1.0, 1.0);
	for (int i = 0; i < _guiViewports.size(); i++)
		if (_guiViewports[i])	_guiViewports[i]->doRender();
}

void GUIEngine::reset ()	{
	_guiScreens.clear ();
	_guiViewports.clear();
}

void GUIEngine::screenDeleted (GUIScreen *scr)	{
	for (int i = 0; i < _guiScreens.size(); i++)
		if (_guiScreens[i] == scr)
			_guiScreens[i] = NULL;
}

bool GUIEngine::processMouseButtonAll (double x, double y, SDL_MouseButtonEvent event)	{
	/* We're proceeding in reverse order here so that the top
	 * GUI screen is proceeded first
	 ***/
	
	for (int i = (int)_guiScreens.size() - 1; i >= 0; i--)	{
		if (!_guiScreens[i])	continue;	/* If this screen has been manually deleted */
		_guiScreens.at (i)->_processMouseButton (x, y, event);
		if (_guiScreens.at (i)->processMouseButton (x, y, event))
			return true;
	}
	
	return false;
}

void GUIEngine::processMouseMotionAll (double x, double y, bool isClicking)
{
	for (int i = 0; i < _guiScreens.size(); i++)	{
		if (!_guiScreens[i])	continue;	/* If this screen has been manually deleted */
		_guiScreens.at (i)->_mouseMotion (x, y, isClicking);
		_guiScreens.at (i)->mouseMotion (x, y, isClicking);
	}
}

bool GUIEngine::processMouseWheelAll (SDL_MouseWheelEvent wheelEvent, int x, int y)	{
	/* We're proceeding in reverse order here so that the top
	 * GUI screen is proceeded first
	 ***/
	
	for (int i = (int)_guiScreens.size() - 1; i >= 0; i--)	{
		if (!_guiScreens[i])	continue;	/* If this screen has been manually deleted */
		_guiScreens.at (i)->_processMouseWheel (wheelEvent, x, y);
		if (_guiScreens.at (i)->processMouseWheel (wheelEvent, x, y))
			return true;
	}
	
	return false;
}

void GUIEngine::processKeypressAll (SDL_KeyboardEvent keyboardEvent)
{
	for (int i = 0; i < _guiScreens.size(); i++)	{
		if (!_guiScreens[i])	continue;	/* If this screen has been manually deleted */
		_guiScreens.at (i)->_processKeypress (keyboardEvent);
		_guiScreens.at (i)->processKeypress (keyboardEvent);
	}
}

void GUIEngine::processTextInputAll (SDL_TextInputEvent textEvent)	{
	for (int i = 0; i < _guiScreens.size(); i++)	{
		if (!_guiScreens[i])	continue;	/* If this screen has been manually deleted */
		_guiScreens.at (i)->_processTextInput (textEvent);
		_guiScreens.at (i)->processTextInput (textEvent);
	}
}
