//
//  GUITabBar.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_TAB_BAT_H
#define _GUI_TAB_BAT_H

#include <iostream>
#include <vector>

#include "GUILib.h"
#include "GUIScreen.h"

class GUITabBar : public GUIElement
{
public:
	GUITabBar (double x, double y, GUI* parent = NULL, double width = 0.0);
	~GUITabBar ();
	
	void addTab (std::string title, GUIScreen* tabView);
	void addTab (GUIButton* tabButton, GUIScreen* tabView);
	
	void removeTab (int tabIndex);
	void removeTab (std::string tabTitle);
	
	void setTabTitle (int tabIndex, std::string title);
	void setTabTitle (std::string tabTitle, std::string title);
	std::string tabTitle (int tabIndex);
	int tabIndex (std::string tabTitle);
	
	GUIButton* tabButton (int tabIndex);
	GUIButton* tabButton (std::string tabTitle);
	
	GUIScreen* tabView (int tabIndex);
	GUIScreen* tabView (std::string tabTitle);
	void setTabView (int tabIndex, GUIScreen* tabScreen);
	void setTabView (std::string tabTitle, GUIScreen* tabScreen);
	
	inline int selectedTabIndex ()	{ return _selectedTabIndex; }
	
	double width ();	/* This will recompute width, save it to _width and return it */
	
	inline long tabCount ()	{ return _tabButtons.size(); }
	
	void render ();
	bool processMouseButton (double x, double y, SDL_MouseButtonEvent event);
	void mouseMotion (double x, double y, bool isClicking);
	bool processMouseWheel (SDL_MouseWheelEvent wheelEvent, int x, int y);
	
	GUI_SLOT (on_btnClicked, GUITabBar)
	
	GUI_SIGNAL (sTabChanged, sTabChangedT)
	
protected:
	std::vector<GUIButton*>	_tabButtons;
	std::vector<GUIScreen*>	_tabScreens;
	
	int _selectedTabIndex;
	
	void _updateTabButtonsPositions ();
	bool _indexExists (int i);
};

#endif /* defined(_GUI_TAB_BAT_H) */
