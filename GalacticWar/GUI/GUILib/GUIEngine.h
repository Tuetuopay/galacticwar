//
//  GUIEngine.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_ENGINE_H
#define _GUI_ENGINE_H

#include "OSCompatibility.h"

#include "GUIScreen.h"
#include "GUIViewport.h"

class GUIEngine	{
public:
	GUIEngine ();
	~GUIEngine ();
	
	/* Because properties are static ones, we need a way to put them baack to 0 */
	void reset ();
	
	/* Renders all the GUIs. */
	void renderAll ();
	void renderViewports ();
	
	/* This will enable us to manually delete a GUI Screen */
	static void screenDeleted (GUIScreen* scr);
	
	/* Event processing. Calls all the GUIScreens corresponding classes
	 bool buttonUp stands for the event is BUTTONDOWN or BUTTONUP */
	bool processMouseButtonAll (double x, double y, SDL_MouseButtonEvent event);
	void processMouseMotionAll (double x, double y, bool isClicking);
	void processKeypressAll (SDL_KeyboardEvent keyboardEvent);
	void processTextInputAll (SDL_TextInputEvent textEvent);
	bool processMouseWheelAll (SDL_MouseWheelEvent wheelEvent, int x, int y);
	
	/* Adds a GUI to the list */
	static void addGUIScreen (GUIScreen *screen);
	
	/* Adds a viewport to the screen */
	static void addGUIViewport (GUIViewport *viewport);
	
protected:
	static std::vector<GUIScreen*>	_guiScreens;
	static std::vector<GUIViewport*>	_guiViewports;
};

#endif /* defined(_GUI_ENGINE_H) */
