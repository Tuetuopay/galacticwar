//
//  GUIImageButton.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUIImageButton.h"

GUIImageButton::GUIImageButton (float x, float y, std::string filename, GUI* parent) : GUIButton (x, y, parent)
{
	resize (1.0, 1.0);
	if (!(_image = loadTexture (filename.c_str())))
		std::cout << "Warning: unable to load texture \"" << filename << "\"." << std::endl;
	
	_description = "GUIImageButton";
}

GUIImageButton::GUIImageButton (float x, float y, GLuint glTexture, GUI* parent) : GUIButton (x, y, parent)
{
	resize (1.0, 1.0);
	_image = glTexture;
	
	_description = "GUIImageButton";
}

GUIImageButton::~GUIImageButton()
{
	
}

void GUIImageButton::render ()
{
	glPushMatrix();
	glTranslatef (_x, _y, 0.0);
	
	if (_isToggled)	glColor3f (0.5, 0.5, 0.5);
	else if (_isHovered)	glColor3f (0.8, 0.8, 1.0);
	else			glColor3f (1.0, 1.0, 1.0);
	
	drawSquare (_width, _height, true, _image);
	
	glPopMatrix();
}
