//
//  GUITextEdit.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUITextEdit.h"

GUITextEdit::GUITextEdit (double width, double height, double x, double y, std::string defaultText, GUI* parent)
	: GUIElement (width, height, x, y, parent), _text(defaultText), _clickBeganOnMe(false), _editing(false)
{
	_description = "GUITextEdit";
}

GUITextEdit::~GUITextEdit() {}

bool GUITextEdit::processMouseButton (double x, double y, SDL_MouseButtonEvent event)	{
	if (event.button != SDL_BUTTON_LEFT)	return false;
	
	if (event.type == SDL_MOUSEBUTTONUP)	{
		_editing = (x >= _x && x <= _x + _width && y >= _y && y <= _y + _height && _clickBeganOnMe);
		_clickBeganOnMe = false;
	} else {
		_clickBeganOnMe = (x >= _x && x <= _x + _width && y >= _y && y <= _y + _height);
		_editing = false;
	}
	
	return isInsideMe (x, y);
}

void GUITextEdit::processKeypress (SDL_KeyboardEvent keybordEvent)
{
	if (_editing)	{
		if (keybordEvent.keysym.scancode == SDL_SCANCODE_BACKSPACE && _text.length() > 0 && keybordEvent.type == SDL_KEYDOWN)
			_text.pop_back();	/* Removing the last character */
		
		GUI_SendSignal(textChangedSignal);
	}
}
void GUITextEdit::processTextInput (SDL_TextInputEvent textEvent)	{
	if (_editing)	{
		_text += textEvent.text;
		
		GUI_SendSignal(textChangedSignal);
	}
}

void GUITextEdit::render ()
{
	glPushMatrix();
	glTranslated (_x, _y, 0.0);
	std::string renderString = _text;
	if (_editing)	renderString += '_';
	/* Background */
	glColor4f (0.0, 0.0, 0.0, 1.0);
	drawSquare (_width, 1.2, true);
	glColor4f (1.0, 1.0, 1.0, 1.0);
	drawSquare (_width, 1.2, false);
	// _textEngine.drawBackground ((float)_width, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0);
	glTranslated (0.1, 0.1, 0.0);
	_textEngine.drawString (renderString);
	glPopMatrix();
}
