//
//  GUIImageButton.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_IMAGE_BUTTON
#define _GUI_IMAGE_BUTTON

#include "OSCompatibility.h"

#include "GUIButton.h"

class GUIImageButton : public GUIButton
{
public:
	GUIImageButton (float x, float y, std::string filename, GUI* parent = NULL);
	GUIImageButton (float x, float y, GLuint glTexture, GUI* parent = NULL);
	~GUIImageButton ();
	
	/* The only overriden method :P */
	void render ();
	
	/* If we want to change the texture afterwards */
	inline void setTexure (GLuint texture) { _image = texture; }
	
private:
	/* The image shown as a button */
	GLuint _image;
};

#endif /* defined(_GUI_IMAGE_BUTTON) */
