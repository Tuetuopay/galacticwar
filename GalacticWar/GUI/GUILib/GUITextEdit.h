//
//  GUITextEdit.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_TEXT_EDIT
#define _GUI_TEXT_EDIT

#include "OSCompatibility.h"

#include "GUIElement.h"

class GUITextEdit : public GUIElement
{
public:
	GUITextEdit (double width, double height, double x, double y, std::string defaultText = "", GUI* parent = NULL);
	~GUITextEdit ();
	
	void render ();
	bool processMouseButton (double x, double y, SDL_MouseButtonEvent event);
	void processKeypress (SDL_KeyboardEvent keyboardEvent);
	void processTextInput (SDL_TextInputEvent textEvent);
	
	inline const std::string& text ()				{ return _text; }
	inline void setText (const std::string& text)	{ _text = text; }
	
	/* Signal functions */
	GUI_SIGNAL(textChanged, textChangedSignal)
	
private:
	std::string _text, _maxText;
	
	bool _clickBeganOnMe;
	bool _editing;
};

#endif /* defined(_GUI_TEXT_EDIT) */
