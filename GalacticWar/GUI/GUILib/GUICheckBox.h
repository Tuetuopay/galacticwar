//
//  GUICheckBox.h
//  PokeFPS
//
//  Created by Alexis on 12/12/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUICheckBox_H
#define _GUICheckBox_H

#include <iostream>
#include "GUIImageButton.h"
#include "GUILabel.h"

class GUICheckBox : public GUIImageButton {
public:
	GUICheckBox (double x, double y, std::string text, GUI* parent = NULL);
	~GUICheckBox ();
	
	void render ();
	
	inline void setText (const std::string& text)	{ _label->setText (text); }
	inline std::string text () const		{ return _label->text (); }
	
	GUI_SIGNAL (onCheck, onCheckS)
	GUI_SIGNAL (onUncheck, onUncheckS)
	
private:
	static GLuint _texChecked, _texUnchecked;
	
	GUILabel *_label;
	
	GUI_SLOT (checked, GUICheckBox)
	GUI_SLOT (unchecked, GUICheckBox)
};

#endif /* defined(_GUICheckBox_H) */