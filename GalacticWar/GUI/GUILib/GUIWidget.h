//
//  GUIWidget.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_WIDGET_H
#define _GUI_WIDGET_H

#include "OSCompatibility.h"

#include "GFX/sdlglutils.h"
#include "GFX/OpenGL/VBO.h"

#include "GFX/TextEngine.h"

#include "GUISignalSlot.h"

#define TRANSITION_DURATION	0.5

class GUIWidget
{
public:
	/* Width, height, x and y are given in GL units */
	GUIWidget (GUIWidget *parent = NULL);
	GUIWidget (double x, double y, GUIWidget *parent = NULL);
	GUIWidget (double x, double y, double width, double height, GUIWidget *parent = NULL);
	~GUIWidget ();
	
	/* Will redefine the gui's geometry */
	void resize (double width, double height);
	void move   (double x, double y);
	void moveCenter	(double x, double y);
	void moveToScreenCenter ();
	void centerHorizontally ();
	void centerVertically ();
	
	/* Apply render modifiers (alpha, position, scale, ...) */
	void applyModifiers ();
	/* Renders the Widget. Needs to be overriden by child widgets */
	void render ();
	/* Render children widgets. Should not be overriden, exceptin very special cases */
	void renderChildren ();
	
	/* Event processing. Implemented by child classes
	 bool buttonUp stands for the event is BUTTONDOWN or BUTTONUP */
	bool processMouseButton (double x, double y, SDL_MouseButtonEvent event);
	void mouseMotion		(double x, double y, bool isClicking);
	void processKeypress	(SDL_KeyboardEvent keyboardEvent);
	/* Event processing for children widget. May not be overriden, except in special cases */
	bool processMouseButtonChildren (double x, double y, SDL_MouseButtonEvent event);
	void mouseMotionChildren		(double x, double y, bool isClicking);
	void processKeypressChildren	(SDL_KeyboardEvent keyboardEvent);
	
	/* Will return the scale to apply when using a popout transition
	 * time is given in seconds
	 */
	double popTransitionScale (double time, double duration = 1.0);
	
	bool isInsideMe (double x, double y);
	
	/* Getters */
	double width ()	{ return _width; }
	double height()	{ return _height; }
	double x ()		{ return _x; }
	double y ()		{ return _y; }
	/* Absolute coordinates on screen, not only relative ones to parent */
	double absX ();
	double absY ();
	double alpha ()	{ return _alpha; }
	void setAlpha (double alpha)	{ _alpha = (alpha >= 0 && alpha <= 1.0) ? alpha : _alpha; }
	
	/* Widget visibility */
	void show (bool animate = false);
	void hide (bool animate = false);
	void setShown (bool shown)	{ _isShown = shown; }
	bool isShown ()	{ return _isShown; }

	
	/* Commentless */
	GUIWidget*	parent ()	{ return _parent; }
	void		setParent (GUIWidget* parent = NULL)	{ _parent = parent; }
	
	/* Screen dimensions in GL Units and in Pixels */
	static double	screenGLWidth, screenGLHeight;
	static int		screenPXWidth, screenPXHeight;
	
protected:
	/* Child management */
	std::vector<GUIWidget*>	_children;
	void addWidget (GUIWidget *widget);
	
	/* Widget's geometry */
	double _width, _height, _x, _y;
	double _alpha;
	
	void importParentGeometry ();
	
	/* Is the widget shown ? */
	bool _isShown;
	
	/* Time when the widget was shown */
	double _showBeginning, _hideBeginning;
	
	/* So that all Widgets will have a text engine without the need to redeclare one */
	TextEngine	_textEngine;
	
	/* So that all Widgets will have a square (filled and not) in a VBO without needing to redeclare one */
	void	_buildVBOs ();
	static VBO *_vboSquareFilled, *_vboSquareEmpty;
	static void drawSquare (double w, double h, bool filled, GLuint texture = 0);
	
	/* For debugging purposes: a descriptor string of the Widget */
	std::string _description;
	
	/* The parent Widget */
	GUIWidget*	_parent;
	
private:
	void _buildWidget (double x, double y, double width, double height, GUIWidget *parent);
};

#endif /* defined(__PokeFPS__GUIWidget__) */
