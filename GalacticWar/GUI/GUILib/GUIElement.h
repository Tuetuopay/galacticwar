//
//  GUIElement.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_ELEMENT_H
#define _GUI_ELEMENT_H

#include "OSCompatibility.h"

#include "GUI.h"

class GUIElement : public GUI
{
public:
	GUIElement (GUI* parent = NULL);
	GUIElement (double width, double height, GUI* parent = NULL);
	GUIElement (double width, double height, double x = 0.0, double y = 0.0, GUI* parent = NULL);
	~GUIElement ();
	
	/* This determines if this GUI element currently has the focus in the GUI screen */
	inline bool hasFocus () { return _hasFocus; }
	
	virtual void render () {}
	virtual bool processMouseButton (double x, double y, SDL_MouseButtonEvent event)	{ return isInsideMe (x, y); }
	virtual void mouseMotion (double x, double y, bool isClicking) {}
	virtual void processKeypress (SDL_KeyboardEvent keyboardEvent) {}
	virtual void processTextInput (SDL_TextInputEvent textEvent)   {}
	virtual bool processMouseWheel (SDL_MouseWheelEvent wheelEvent, int x, int y)	{ return isInsideMe (x, y); }
	
protected:
	bool _hasFocus;	/* Currently unused >_< */
};

#endif /* defined(_GUI_ELEMENT_H) */
