//
//  GUISpinBox.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_SPIN_BOX_H
#define _GUI_SPIN_BOX_H

#include "OSCompatibility.h"

#include "GUIElement.h"

#include "GUIImageButton.h"
#include "GUILabel.h"

#define _GUI_SPIN_BOX_BTN_SCALE	0.6

class GUISpinBox : public GUIElement	{
public:
	GUISpinBox (double x, double y, GUI* parent = NULL, int min = 1, int max = 99);
	~GUISpinBox ();
	
	/* Events management */
	GUI_SLOT (on_btnUp_clicked, GUISpinBox)
	GUI_SLOT (on_btnDown_clicked, GUISpinBox)
	GUI_SIGNAL (valueChanged, valueChangedSignal)
	
	/* Event processing */
	bool processMouseButton (double x, double y, SDL_MouseButtonEvent event);
	void mouseMotion (double x, double y, bool isClicking);
	
	/* Rendering */
	void render ();
	
	/* Accessors */
	inline int value ()	{ return _value; }
	void setValue (int value);
	void setRange (int min, int max);
	
protected:
	/* Widgets */
	GUILabel	*_lblDisplay;
	GUIImageButton	*_btnUp, *_btnDown;
	
	/* Data */
	int _value, _min, _max;
	std::string _valueString;
	
	void _updateValueString ();
	void _updateSubWidgets ();
};

#endif /* defined(_GUI_SPIN_BOX_H) */
