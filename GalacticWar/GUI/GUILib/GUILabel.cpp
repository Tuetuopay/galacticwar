//
//  GUILabel.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUILabel.h"

GUILabel::GUILabel (float x, float y, const std::string& text, GUI* parent, float scale) : GUIElement(10.0, 1.0, x, y, parent) , _text (text), _scale(scale)
{
	resize (_textEngine.getStringWidth(_text), 1.0);
	_textColor.r = 1.0;
	_textColor.g = 1.0;
	_textColor.b = 1.0;
	_textColor.a = 1.0;
	
	_description = "GUILabel";
}
GUILabel::~GUILabel()
{}

void GUILabel::setText (const std::string& text)
{
	_text = "";
	_text += text;
	resize (_textEngine.getStringWidth(_text), 1.0);
}

void GUILabel::setTextColor (float r, float g, float b, float a)
{
	_textColor.r = r;
	_textColor.g = g;
	_textColor.b = b;
	_textColor.a = a;
}

void GUILabel::render()
{
	glPushMatrix();
	glTranslatef (_x, _y, 0.0);
	glScaled (_scale, _scale, 1.0);
	
	/* Rendering background */
	glPushMatrix ();
	glTranslated (1.0 / 8.0, 1.0 / 8.0, 1.0);
	_textEngine.drawString (_text, _textColor.r - 0.6, _textColor.g - 0.6, _textColor.b - 0.6);
	glPopMatrix();
	
	glColor4f (_textColor.r, _textColor.g, _textColor.b, _textColor.a);
	glPushMatrix();
	_textEngine.drawString (_text, _textColor.r, _textColor.g, _textColor.b);
	glPopMatrix();
	glPopMatrix();
}
