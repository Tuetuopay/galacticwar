//
//  GUIElement.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//


#include "GUIElement.h"

GUIElement::GUIElement (GUI* parent) : GUI (parent)
{}
GUIElement::GUIElement (double width, double height, GUI* parent) : GUI (parent, width, height)
{}
GUIElement::GUIElement (double width, double height, double x, double y, GUI* parent) : GUI(parent, width, height, x, y)
{}

GUIElement::~GUIElement()
{
	
}
