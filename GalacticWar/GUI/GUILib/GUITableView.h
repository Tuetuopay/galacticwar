//
//  GUITableView.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_TABLE_VIEW_H
#define _GUI_TABLE_VIEW_H

#include "OSCompatibility.h"

#include "GUIElement.h"
#include "GUITableViewCell.h"

class GUITableView : public GUIElement
{
public:
	/* Height is given in CELLS, not in GL Units */
	GUITableView (double x, double y, GUI* parent = NULL, double width = 5.0, int height = 5.0);
	~GUITableView ();
	
	void addCell (std::string label);
	void addCell (GUITableViewCell *cell);

	void clearCells ();
	std::string cellTextAt (int cellIndex);
	std::string selectedCellText ();
	
	void render ();
	
	GUI_SLOT(on_cell_clicked, GUITableView)
	inline int selectedIndex ()	{ return _selectedCell; }
	
	GUI_SIGNAL (cellChanged, cellChangedSignal)
	
	bool processMouseButton (double x, double y, SDL_MouseButtonEvent event);
	void mouseMotion (double x, double y, bool isClicking);
	bool processMouseWheel (SDL_MouseWheelEvent wheelEvent, int x, int y);
	void processKeypress (SDL_KeyboardEvent keyboardEvent);
	
private:
	std::vector<GUITableViewCell*> _cells;
	
	/* When we are scrolling through the table view */
	int		_selectedCell;
	float	_nCellsHeight, _cellsOffset;
	/* Cells Offset:
	 *
	 * -----	^
	 * Cell		| Invisible = cellsOffset (positive)
	 * -----	|
	 * Cell		v
	 * =====	<= Visible limit
	 * Cell
	 */
	
	double _elevatorHeight, _elevatorY;
	
	void updateCellsPositions ();
	void updateToggledCell ();
	void updateElevator ();
};

#endif /* defined(_GUI_TABLE_VIEW_H) */
