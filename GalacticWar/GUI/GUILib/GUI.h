//
//  GUI.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_H
#define _GUI_H

#include "OSCompatibility.h"

#include "GFX/sdlglutils.h"
#include "GFX/OpenGL/VBO.h"

#include "GFX/TextEngine.h"

#include "GUISignalSlot.h"

typedef struct GUIAction GUIAction;

typedef struct GLcolor {
	float r, g, b, a;
} GLcolor;

class GUI	{
public:
	/* Width, height, x and y are given in GL units */
	GUI (GUI* parent = NULL);
	GUI (GUI* parent, double width, double height, double x = 0.0, double y = 0.0);
	~GUI ();
	
	/* Will redefine the gui's geometry */
	void resize (double width, double height);
	void move   (double x, double y);
	void moveCenter	(double x, double y);
	void moveToScreenCenter ();
	void centerHorizontally ();
	void centerVertically ();
	
	/* Will stick the widget to the edge of the target with a margin of 1.0 */
	void stickToLeft	(GUI *target = NULL);
	void stickToRight	(GUI *target = NULL);
	void stickToUp		(GUI *target = NULL);
	void stickToDown	(GUI *target = NULL);
	
	/* Renders the GUI. Needs to be overriden by GUIs */
	void render ();
	
	/* Event processing. Implemented by child classes
	 bool buttonUp stands for the event is BUTTONDOWN or BUTTONUP */
	bool processMouseButton (double x, double y, SDL_MouseButtonEvent event);
	void mouseMotion (double x, double y, bool isClicking);
	void processKeypress (SDL_KeyboardEvent keyboardEvent);
	void processTextInput (SDL_TextInputEvent textEvent);
	bool processMouseWheel (SDL_MouseWheelEvent wheelEvent, int x, int y);
	
	/* Will return the scale to apply when using a popout transition
	 * time is given in seconds
	 */
	float popTransitionScale (float time, float duration = 1.0);
	double hoverScale (double time, double duration = 1.0);
	
	bool isInsideMe (float x, float y);
	
	/* Getters */
	inline double width ()	{ return _width; }
	inline double height()	{ return _height; }
	inline double x ()		{ return _x; }
	inline double y ()		{ return _y; }
	/* Absolute coordinates on screen, not only relative ones to parent */
	double absX ();
	double absY ();
	inline double alpha ()	{ return _alpha; }
	inline void setAlpha (double alpha)	{ _alpha = (alpha >= 0 && alpha <= 1.0) ? alpha : _alpha; }
	
	/* Commentless */
	inline GUI*	parent ()	{ return _parent; }
	inline void	setParent (GUI* parent = NULL)	{ _parent = parent; }
	
	static double	screenGLWidth, screenGLHeight;
	static int		screenPXWidth, screenPXHeight;
	
	std::string hierarchy ();
	
protected:
	/* GUI's geometry */
	double _width, _height, _x, _y;
	double _alpha;
	
	/* So that all GUIs will have a text engine without the need to redeclare one */
	TextEngine	_textEngine;
	
	/* So that all GUIs will have a square (filled and not) in a VBO without needing to redeclare one */
	void	_buildVBOs ();
	static VBO *_vboSquareFilled, *_vboSquareEmpty;
	static void drawSquare (double w, double h, bool filled, GLuint texture = 0);
	
	/* For debugging purposes: a descriptor string of the GUI */
	std::string _description;
	
	/* The parent GUI */
	GUI*	_parent;
};

struct GUIAction
{
	/* If this is a button leading to another GUI screen */
	bool isLeadingToGuiScreen;
	GUI *targetGui;

	/* If this is a text field */
	std::string text;

	/* If this is an element returning a value */
	int intValue;
	float floatValue;
};

#endif /* defined(_GUI_H) */









