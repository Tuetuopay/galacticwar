//
//  GUILabel.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_LABEL
#define _GUI_LABEL

#include "OSCompatibility.h"

#include "GUIElement.h"

class GUILabel : public GUIElement
{
public:
	GUILabel (float x, float y, const std::string& text, GUI* parent = NULL, float scale = 1.0);
	~GUILabel ();
	
	/* Accessors */
	void setText (const std::string& text);
	inline const std::string& text ()	{ return _text; }
	
	void setTextColor (float r, float g, float b, float a = 1.0);
	inline const GLcolor& textColor ()			{ return _textColor; }
	
	/* Rendering */
	void render ();
	inline void scaleText (float scale)	{ _scale = scale; }
	
protected:
	std::string _text;
	GLcolor		_textColor;
	float		_scale;
};

#endif /* _GUI_LABEL */
