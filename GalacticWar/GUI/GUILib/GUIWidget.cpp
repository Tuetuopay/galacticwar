//
//  GUIWidget.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUIWidget.h"

VBO * GUIWidget::_vboSquareFilled = NULL;
VBO * GUIWidget::_vboSquareEmpty = NULL;

double	GUIWidget::screenGLHeight = 0.0;
double	GUIWidget::screenGLWidth  = 0.0;
int		GUIWidget::screenPXWidth  = 0;
int		GUIWidget::screenPXHeight = 0;


GUIWidget::GUIWidget (GUIWidget* parent) : _textEngine()
{
	if (parent)
		_buildWidget (1.0, 1.0, _parent->width(), _parent->height(), parent);
	else
		_buildWidget (0.0, 0.0, 1.0, 1.0, parent);
}
GUIWidget::GUIWidget (double x, double y, GUIWidget *parent) : _textEngine ()
{
	if (parent)
		_buildWidget (x, y, _parent->width(), _parent->height(), parent);
	else
		_buildWidget (x, y, 1.0, 1.0, parent);
}
GUIWidget::GUIWidget (double x, double y, double width, double height, GUIWidget *parent) : _textEngine ()
{
	_buildWidget (x, y, width, height, parent);
}

GUIWidget::~GUIWidget ()
{}

void GUIWidget::_buildWidget (double x, double y, double width, double height, GUIWidget *parent)	{
	_x = x;	_y = y;
	importParentGeometry();
	_buildVBOs();
}

void GUIWidget::importParentGeometry()	{
	if (!_parent)	return;
	
	_width = (_parent->width() - 2.0 >= 0.0) ? _parent->width() - 2.0 : 0.0;
	_width = (_parent->height() - 2.0 >= 0.0) ? _parent->height() - 2.0 : 0.0;
}

void GUIWidget::resize (double width, double height)
{
	_width = width;		_height = height;
}

void GUIWidget::move (double x, double y)
{
	_x = x;				_y = y;
}

void GUIWidget::moveCenter (double x, double y)
{
	move (x - _width / 2.0, y - _height / 2.0);
}

void GUIWidget::moveToScreenCenter()	{
	if (screenGLWidth == 0 || screenGLHeight == 0)	return;
	
	moveCenter (screenGLWidth / 2.0, screenGLHeight / 2.0);
}

void GUIWidget::centerHorizontally()	{
	_x = screenGLWidth / 2.0 - _width / 2.0;
}
void GUIWidget::centerVertically()	{
	_y = screenGLHeight / 2.0 - _height / 2.0;
}

double GUIWidget::absX()	{
	if (_parent) return _parent->absX() + _x;
	else return _x;
}
double GUIWidget::absY()	{
	if (_parent)	return _parent->absY() + _y;
	else return _y;
}

void GUIWidget::render()
{
	
}

void GUIWidget::mouseMotion (double x, double y, bool isClicking)
{
	
}

bool GUIWidget::processMouseButton (double x, double y, SDL_MouseButtonEvent event)	{
	/* If the click was made inside the screen */
	return isInsideMe (x, y);
}

void GUIWidget::processKeypress (SDL_KeyboardEvent keyboardEvent)
{
	
}

bool GUIWidget::isInsideMe (double x, double y)	{
	return (x >= _x && x <= _x + _width && y >= _y && y <= _y + _height);
}

void GUIWidget::_buildVBOs()	{
	/* Building the VBOs */
	/* I am using floats instead of double here because GPU's are faster with floats, while CPUs are faster with doubles */
	GLfloat vert[] = {
		0.0, 0.0,
		0.0, 1.0,
		1.0, 1.0,
		1.0, 0.0
	}, tex[] = {
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0
	};
	if (!_vboSquareEmpty)
	/* Well ... we'll set a texture at GL ID 1 because else the VBO class won't build the texture buffer */
		_vboSquareEmpty = new VBO (vert, tex, NULL, NULL, 4, 1, GL_LINE_LOOP, 2);
	if (!_vboSquareFilled)
		_vboSquareFilled = new VBO (vert, tex, NULL, NULL, 4, 1, GL_QUADS, 2);
}
void GUIWidget::drawSquare (double w, double h, bool filled, GLuint texture)	{
	glPushMatrix();
	glScaled (w, h, 1.0);
	if (filled)	{
		_vboSquareFilled->setTexture (texture);
		_vboSquareFilled->render();
		_vboSquareFilled->setTexture (0);
	}	else	{
		_vboSquareEmpty->setTexture (texture);
		_vboSquareEmpty->render();
		_vboSquareEmpty->setTexture (0);
	}
	glPopMatrix();
}

/* (ax - b) ^3 + (ax - b) ^2 + 1
 * a = cube root of pi
 * b = a
 * approx. 1.4645
 */

double GUIWidget::popTransitionScale (double time, double duration)
{
	const double a = cbrtf (M_PI);
	
	time *= 1.0 / duration;
	
	if (time >= 1.0)
		return 1.0;
	else if (time <= 0.0)
		return 0.0;
	else
		return (a * time - a) * (a * time - a) * (a * time - a) + (a * time - a) * (a * time - a) + 1.0;
}
