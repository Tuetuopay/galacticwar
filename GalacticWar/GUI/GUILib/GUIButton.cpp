//
//  GUIButton.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUIButton.h"

#define ZOOM_DURATION	0.3

GUIButton::GUIButton (float x, float y, GUI* parent)
: _textWidth(1.0), GUIElement (1.0 + 0.2, 1.2, x, y, parent), _text(" ")
{
	_setupButton();
}
GUIButton::GUIButton (float x, float y, std::string title, GUI* parent)
	: _textWidth(_textEngine.getStringWidth(title)), GUIElement (_textEngine.getStringWidth(title) + 0.2, 1.2, x, y, parent), _text(title)
{
	_setupButton();
}

void GUIButton::_setupButton()	{
	_isToggled = false;
	_isToggleable = false;
	_isClicked = false;
	_clickBeganOnMe = false;
	_isDefaultButton = false;
	_isHovered = false;
	_hoverBeginning = _hoverEnding = 0.0;
	_zoomOnHover = true;
	
	/* Well ... A click coudn't have been ouside the button ! */
	_clickX = _clickY = -1.0;
	
	_description = "GUIButton";
}

GUIButton::~GUIButton()
{
	
}

bool GUIButton::processMouseButton (double x, double y, SDL_MouseButtonEvent event)	{
	if (event.button != SDL_BUTTON_LEFT)
		return false;
	
	if (event.type == SDL_MOUSEBUTTONUP)	/* if we're releasing the click */
	{
		if (isInsideMe (x, y) && _clickBeganOnMe)	{
			_isClicked = true;
			
			_clickX = x - _x;	_clickY = y - _y;
			
			/* Triggering the callback function */
			GUI_SendSignal(onClickSignal);
		} else {
			_isClicked = false;
		}
		_isToggled = (_isToggleable) ? _isToggled : false;
		_clickBeganOnMe = false;
	
		if (_isToggled)	GUI_SendSignal (toggledDownSignal);
		else			GUI_SendSignal (toggledUpSignal);
	} else		/* If we clicked */
	{
		if (isInsideMe (x, y))	{
			_clickBeganOnMe = true;
			_isToggled = (_isToggleable) ? !_isToggled : true;
		} else {
			_clickBeganOnMe = false;
			_isToggled = (_isToggleable) ? _isToggled : false;
		}
	}
	
	return isInsideMe (x, y);
}

void GUIButton::mouseMotion (double x, double y, bool isClicking)
{
	if (isClicking)
	{
		_isToggled = (isInsideMe (x, y) && _clickBeganOnMe);
	} else
		_clickBeganOnMe = false;
	
	bool status = isInsideMe (x, y);
	if (_isHovered && !status)	/* Mouse Left */
		_hoverEnding = double(SDL_GetTicks()) / 1000.0;
	else if (!_isHovered && status)	/* Mouse entered */
		_hoverBeginning = double(SDL_GetTicks()) / 1000.0;
	_isHovered = status;
}

void GUIButton::processKeypress (SDL_KeyboardEvent event)	{
	if (event.keysym.scancode == SDL_SCANCODE_RETURN && _isDefaultButton)
		GUI_SendSignal(onClickSignal);
}

void GUIButton::render ()
{
	glPushMatrix();
	glTranslatef (_x, _y, 0.0);
	
	if (_zoomOnHover)	{
		glTranslated (_width / 2.0, _height / 2.0, 0.0);
		double time = double(SDL_GetTicks()) / 1000.0, scale = 1.0;
		if (_isHovered)
			scale = hoverScale (time - _hoverBeginning, ZOOM_DURATION);	/* Zoom in */
		else if (!_isHovered && time - _hoverEnding < ZOOM_DURATION)
			scale = hoverScale (ZOOM_DURATION - (time - _hoverEnding), ZOOM_DURATION / 2.0);	/* Zoom out */
		glScaled (scale, scale, 1.0);
		glTranslated (-_width / 2.0, -_height / 2.0, 0.0);
	}
	
	
	/* Background of the button */
	if (_isToggled)			glColor4f (0.5, 0.5, 0.5, 0.4);
	else if (_isHovered)	glColor4f (0.7, 0.7, 1.0, 0.8);
	else					glColor4f (0.9, 0.9, 0.9, 0.8);
	drawSquare (_width, _height, true);
	if (_isToggled)	glColor4f (0.3, 0.3, 0.3, 0.8);
	else			glColor4f (0.7, 0.7, 0.7, 0.8);
	drawSquare (_width, _height, false);
	
	glTranslatef (0.4, 0.1, 0.0);
	_textEngine.drawString (_text, 0.0, 0.0, 0.0);
	glPopMatrix();
}





