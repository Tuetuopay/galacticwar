//
//  GUITabBar.cpp
//  PokeFPS
//
//  Created by Alexis on 08/09/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUITabBar.h"

GUITabBar::GUITabBar (double x, double y, GUI* parent, double width) : GUIElement (width, 1.2, x, y, parent), _tabButtons(0), _tabScreens(0)
{
	_selectedTabIndex = 0;	/* On a tab system, there is ALWAYS a default tab */
	
	_description = "GUITabBar";
}

GUITabBar::~GUITabBar ()
{
	
}

void GUITabBar::addTab (std::string title, GUIScreen* tabView)	{
	if (!tabView)	return;
	addTab (new GUIButton (width(), 0.0, title), tabView);
}
void GUITabBar::addTab (GUIButton* tabButton, GUIScreen* tabView)	{
	if (tabButton == NULL || tabView == NULL)
		return;
	
	tabButton->move (width(), 0.0);
	tabButton->setToggleable (true);
	tabButton->setToggled (true);
	tabButton->onClick (on_btnClicked, this);
	tabView->move (0.0, 1.2);
	tabView->setParent (this);
	tabView->setBackgroundColor (0.0, 0.0, 0.0, 0.0);
	tabView->hide ();	/* No hiding animation */
	
	_tabButtons.push_back (tabButton);
	_tabScreens.push_back (tabView);
	width ();	/* Recomputing the width */
}

void GUITabBar::removeTab (int tabIndex)	{
	if (!_indexExists (tabIndex))	return;
	
	_tabButtons.erase (_tabButtons.begin() + tabIndex);
	_tabScreens.erase (_tabScreens.begin() + tabIndex);
	
	width();	/* Recomputing */
}
void GUITabBar::removeTab (std::string tabTitle)	{
	removeTab (tabIndex (tabTitle));
}

void GUITabBar::setTabTitle (int tabIndex, std::string title)	{
	if (!_indexExists (tabIndex))	return;
	
	_tabButtons.at (tabIndex)->setText (title);
}
void GUITabBar::setTabTitle (std::string tabTitle, std::string title)	{
	setTabTitle (tabIndex (tabTitle), title);
}
std::string GUITabBar::tabTitle (int tabIndex)	{
	if (!_indexExists (tabIndex))	return "";
	return _tabButtons.at (tabIndex)->text ();
}
int GUITabBar::tabIndex (std::string tabTitle)	{
	for (int i = 0; i < _tabButtons.size(); i++)
		if (_tabButtons.at (i)->text() == tabTitle)
			return i;
	return -1;	/* Not found >: */
}

GUIButton* GUITabBar::tabButton (int tabIndex)	{
	if (!_indexExists (tabIndex))	return NULL;
	return _tabButtons.at (tabIndex);
}
GUIButton* GUITabBar::tabButton (std::string tabTitle)	{
	return tabButton (tabIndex (tabTitle));
}

GUIScreen* GUITabBar::tabView (int tabIndex)	{
	if (!_indexExists (tabIndex))	return NULL;
	return _tabScreens.at (tabIndex);
}
GUIScreen* GUITabBar::tabView (std::string tabTitle)	{
	return tabView (tabIndex (tabTitle));
}
void GUITabBar::setTabView (int tabIndex, GUIScreen *tabScreen)	{
	if (!_indexExists (tabIndex) || !tabScreen)	return;
	_tabScreens.at (tabIndex) = tabScreen;
}
void GUITabBar::setTabView (std::string tabTitle, GUIScreen *tabScreen)	{
	setTabView (tabIndex (tabTitle), tabScreen);
}

double GUITabBar::width	()	{
	_width = 0;
	for (int i = 0; i < _tabButtons.size(); i++)
		_width += _tabButtons[i]->width();
	return _width;
}

void GUITabBar::render ()	{
	glPushMatrix();
	glTranslated (_x, _y, 0.0);
	/* Rendering an outline around the box */
	if (_indexExists (_selectedTabIndex))	{
		glColor4f (0.7, 0.7, 0.7, 0.7);
		drawSquare (_tabScreens.at (_selectedTabIndex)->width(), _tabScreens.at (_selectedTabIndex)->height() + 1.2, false);
	}
	
	/* Rendering the buttons */
	for (int i = 0; i < _tabButtons.size(); i++)
		_tabButtons.at (i)->render();
	
	/* Rendering the correct GUI Screen */
	if (_indexExists (_selectedTabIndex))	{
		_tabScreens.at (_selectedTabIndex)->show ();
		_tabScreens.at (_selectedTabIndex)->render ();
		_tabScreens.at (_selectedTabIndex)->hide ();
	}
	glPopMatrix();
}

/* Event transmission to GUIs will be a bit tricky ... A screen ignores events when it is hidden,
 * and because all of our screens are hidden, events from GUI Engine will be effectless.
 * Moreover, Screens do have coordinates (0,0) in screen, so we need to manually correct the events
 * coordinates
 */
bool GUITabBar::processMouseButton (double x, double y, SDL_MouseButtonEvent event)	{
	/* Button bar */
	for (int i = 0; i < _tabButtons.size(); i++)
		_tabButtons.at (i)->processMouseButton (x - _x, y - _y, event);
	
	/* Screens */
	_tabScreens.at (_selectedTabIndex)->show();
	bool screenCaught = _tabScreens.at (_selectedTabIndex)->processMouseButton (x - _x, y - _y, event);
	_tabScreens.at (_selectedTabIndex)->hide();
	
	return (isInsideMe (x, y) || screenCaught);
}
void GUITabBar::mouseMotion (double x, double y, bool isClicking)	{
	/* Button bar */
	for (int i = 0; i < _tabButtons.size(); i++)
		_tabButtons.at (i)->mouseMotion (x - _x, y - _y, isClicking);
	
	/* Screens */
	_tabScreens.at (_selectedTabIndex)->show();
	_tabScreens.at (_selectedTabIndex)->mouseMotion (x - _x, y - _y, isClicking);
	_tabScreens.at (_selectedTabIndex)->hide();
}
bool GUITabBar::processMouseWheel (SDL_MouseWheelEvent wheelEvent, int x, int y)	{
	/* Button bar */
	for (int i = 0; i < _tabButtons.size(); i++)
		_tabButtons[i]->processMouseWheel (wheelEvent, x - _x, y - _y);
	
	/* Screens */
	_tabScreens[_selectedTabIndex]->show ();
	bool screenCaught = _tabScreens[_selectedTabIndex]->processMouseWheel (wheelEvent, x - _x, y - _y);
	_tabScreens[_selectedTabIndex]->hide ();
	
	return (screenCaught || isInsideMe (x, y));
}

void GUITabBar::_updateTabButtonsPositions ()	{
	double width = 0;
	for (int i = 0; i < _tabButtons.size(); i++)	{
		_tabButtons.at (i)->move (width, 0.0);
		width += _tabButtons.at (i)->width();
	}
	
}
bool GUITabBar::_indexExists (int i)	{
	return (i >= 0 && i < _tabButtons.size());
}

void GUITabBar::on_btnClicked()	{
	/* Updating the toggled tab */
	for (int i = 0; i < _tabButtons.size(); i++)
		if (_tabButtons.at (i)->isToggled() && i != _selectedTabIndex)	{
			_selectedTabIndex = i;
			break;
		}
	
	for (int i = 0; i < _tabButtons.size(); i++)
		_tabButtons.at (i)->setToggled (false);
	_tabButtons.at (_selectedTabIndex)->setToggled (true);
	
	GUI_SendSignal (sTabChangedT);
}
