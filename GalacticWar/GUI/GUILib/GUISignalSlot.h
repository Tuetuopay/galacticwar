//
//  GUISignalSlot.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_SIGNAL_SLOT_H
#define _GUI_SIGNAL_SLOT_H

#include <cstdio>

/* Event management. So that I don't have to rewrite this whole thing for each
 * callback finction I write
 ***/
typedef void (*GUISignal)(void);
typedef void (*GUIObjectSignal)(void*);		/* The void* parameter is the object */

/* We'll just need the implementation of
 * void className::slotName ()
 ***/
#define GUI_SLOT(slotName,className)	\
protected:	\
	void slotName ();	\
public:		\
	static void slotName (void *me)	{ ((className*)me)->slotName (); }

#define GUI_SIGNAL(signalName,triggerName)	\
protected:	\
	GUITrigger triggerName; \
public:		\
	void signalName (GUISignal signal)	{ triggerName.callback = signal; }	\
	void signalName (GUIObjectSignal signal, void *target)	{	\
		triggerName.objCallback = signal; \
		triggerName.objTarget = target;	\
	}

class GUITrigger	{
public:
	GUITrigger ();
	
	GUISignal	callback;
	GUIObjectSignal	objCallback;
	void *objTarget;
};

/* More automatic thing */
void GUI_SendSignal (GUITrigger trigger);
void GUI_SendSignal (GUISignal signal);
void GUI_SendSignal (GUIObjectSignal signal, void* target);

#endif
