//
//  GUITableView.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUITableView.h"

GUITableView::GUITableView (double x, double y, GUI* parent, double width, int height)
	: GUIElement (width, height * GUI_CELL_DEFAULT_HEIGHT, x, y, parent)
{
	_nCellsHeight = height;
	_height = _nCellsHeight * GUI_CELL_DEFAULT_HEIGHT;	/* Keeping full cells in the view */
	
	_cellsOffset = 0;
	_selectedCell = -1;	/* No cell is selected */

	_elevatorHeight = _elevatorY;
	
	_description = "GUITableView";
}

GUITableView::~GUITableView ()
{
	for (int i = 0; i < _cells.size(); i++)
		delete _cells[i];
}

void GUITableView::addCell (std::string label)	{
	GUITableViewCell *cell = new GUITableViewCell (label, this, _width);
	cell->onClick (on_cell_clicked, this);
	_cells.push_back (cell);
	
	updateCellsPositions();
}

void GUITableView::addCell (GUITableViewCell *cell)	{
	cell->resize (_width, cell->height());
	cell->onClick (on_cell_clicked, this);
	_cells.push_back (cell);
	
	updateCellsPositions();
}

void GUITableView::render()	{
	glPushMatrix();
	glTranslated (_x, _y, 0.0);
	
	glDisable(GL_TEXTURE_2D);
	
	static double clipPlaneRight[] = {-1, 0, 0, 0},	clipPlaneTop[] = {0, 1, 0, 0}, clipPlaneBottom[] = {0, -1, 0, 0};
	clipPlaneRight[3] = _width;
	clipPlaneBottom[3] = _height;
	glClipPlane (GL_CLIP_PLANE0, clipPlaneRight);
	glClipPlane (GL_CLIP_PLANE1, clipPlaneTop);
	glClipPlane (GL_CLIP_PLANE2, clipPlaneBottom);
	
	glEnable(GL_CLIP_PLANE0);
	glEnable(GL_CLIP_PLANE1);
	glEnable(GL_CLIP_PLANE2);
	/* Cells */
	for (int i = 0; i < _cells.size(); i++)
		_cells[i]->render();
	glDisable(GL_CLIP_PLANE2);
	glDisable(GL_CLIP_PLANE1);
	glDisable(GL_CLIP_PLANE0);
	
	/* Outline */
	glPushMatrix();
	glLineWidth (2.0);
	// glScaled (_width, _height, 1.0);
	glColor4f (1.0, 1.0, 1.0, 0.9);
	drawSquare (_width, _height, false);
	glLineWidth (1.0);
	glPopMatrix();
	
	/* Elevator */
	glPushMatrix();
	glTranslated (_width, _elevatorY, 0.0);
	glColor4f (1.0, 1.0, 1.0, 0.7);
	drawSquare (0.2, _elevatorHeight, true);
	glPopMatrix();
	
	glPopMatrix();
}

void GUITableView::processKeypress(SDL_KeyboardEvent keyboardEvent)	{
	if (keyboardEvent.state != SDL_RELEASED)	return;
	
	switch (keyboardEvent.keysym.scancode)	{
		case SDL_SCANCODE_UP:
			if (--_selectedCell < 0)	_selectedCell = 0;
			GUI_SendSignal (cellChangedSignal);
			
			/* Updating cells offset */
			if (_selectedCell * GUI_CELL_DEFAULT_HEIGHT < _cellsOffset)
				_cellsOffset = _selectedCell * GUI_CELL_DEFAULT_HEIGHT;
			
			updateCellsPositions();
			updateToggledCell();
			break;
			
		case SDL_SCANCODE_DOWN:
			if (++_selectedCell > _cells.size() - 1)	_selectedCell = (int)_cells.size() - 1;
			GUI_SendSignal (cellChangedSignal);
			
			/* Updating cells offset */
			if (_selectedCell * GUI_CELL_DEFAULT_HEIGHT > _cellsOffset + _nCellsHeight * GUI_CELL_DEFAULT_HEIGHT)
				_cellsOffset = (_selectedCell - _nCellsHeight) * GUI_CELL_DEFAULT_HEIGHT;
			
			updateCellsPositions();
			updateToggledCell();
			break;
			
		default:
			break;
	}
}

bool GUITableView::processMouseButton (double x, double y, SDL_MouseButtonEvent event)
{
	if (!isInsideMe (x, y))
		return false;
	
	switch (event.button) {
		case SDL_BUTTON_LEFT:
			for (int i = 0; i < _cells.size(); i++)
				_cells[i]->processMouseButton (x - _x, y - _y, event);
			break;
			
		/*/
		case SDL_BUTTON_WHEELDOWN:
			if ((_cellsOffset += 0.5) > (_cells.size() - 1) * GUI_CELL_DEFAULT_HEIGHT)	_cellsOffset = (_cells.size() - 1) * GUI_CELL_DEFAULT_HEIGHT;
			updateCellsPositions();
			break;
			
		case SDL_BUTTON_WHEELUP:
			if ((_cellsOffset -= 0.5) < 0)	_cellsOffset = 0;
			updateCellsPositions();
			break;
		//*/
			
		default:
			break;
	}
	
	return isInsideMe (x, y);
}
bool GUITableView::processMouseWheel (SDL_MouseWheelEvent wheelEvent, int x, int y)	{
	if (!isInsideMe (x, y))
		return false;
	
	_cellsOffset += ((double)wheelEvent.y / 3.0);
	if (_cellsOffset > (_cells.size() - 1) * GUI_CELL_DEFAULT_HEIGHT)	_cellsOffset = (_cells.size() - 1) * GUI_CELL_DEFAULT_HEIGHT;
	else if (_cellsOffset < 0)	_cellsOffset = 0;
	updateCellsPositions();
	
	return isInsideMe (x, y);
}

void GUITableView::mouseMotion (double x, double y, bool isClicking)	{
	for (int i = 0; i < _cells.size(); i++)
		_cells[i]->mouseMotion (x - _x, y - _y, isClicking);
}

void GUITableView::updateCellsPositions()	{
	for (int i = 0; i < _cells.size(); i++)	{
		_cells[i]->move (0, (float)i * GUI_CELL_DEFAULT_HEIGHT - _cellsOffset);
	}
	updateElevator ();
}

void GUITableView::on_cell_clicked()	{
	/* Seeking the cell that has just been toggled */
	for (int i = 0; i < _cells.size(); i++)
		if (i != _selectedCell && _cells[i]->isToggled())	{
			_selectedCell = i;
			break;
		}
	updateToggledCell();
	
	GUI_SendSignal(cellChangedSignal);
}

void GUITableView::updateToggledCell()	{
	/* Updating the toggled cell */
	for (int i = 0; i < _cells.size(); i++)
		_cells[i]->setToggled (false);
	if (_selectedCell >= 0)
		_cells[_selectedCell]->setToggled (true);
}

void GUITableView::updateElevator ()	{
	/* Recomputing elevator's position */
	double a = (double)_nCellsHeight / (double)_cells.size();
	if (a > 1.0)	a = 1.0;
	_elevatorHeight = _height * (a);
	_elevatorY = (_height - _elevatorHeight) * ((double)_cellsOffset / (double)_cells.size());
}

void GUITableView::clearCells ()	{
	_cellsOffset = 0;
	_selectedCell = -1;	/* No cell is selected */
	_cells.clear ();
	_cells.resize (0);
}

std::string GUITableView::cellTextAt (int cellIndex)	{
	if (cellIndex >= _cells.size ())	cellIndex = (int)_cells.size () - 1;
	if (cellIndex < 0)	cellIndex = 0;

	return _cells[cellIndex]->text ();
}
std::string GUITableView::selectedCellText ()	{
	return (_selectedCell >= 0) ? cellTextAt (_selectedCell) : "";
}













