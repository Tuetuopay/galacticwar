//
//  GUIViewport.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUIViewport.h"

#include "GUIEngine.h"

GUIViewport::GUIViewport (double x, double y, double width, double height, GUI* parent) : GUIElement (width, height, x, y, parent)
{
	flaggedForRender = false;
	
	lookAt (Vector3D (3.0, 2.0, 0.0), Vector3D ());
	perspective(); /* Set defaults values :P */
	
	GUIEngine::addGUIViewport (this);
	
	_description = "GUIViewport";
}

GUIViewport::~GUIViewport ()
{
	
}

void GUIViewport::render()	{
	flaggedForRender = true;
}
void GUIViewport::doRender()	{
	if (!flaggedForRender)	return;
	flaggedForRender = false;
	
	glViewport (absX() * (screenPXWidth / screenGLWidth),
				screenPXHeight - ((absY() + _height) * (screenPXHeight / screenGLHeight)),
				_width * (screenPXWidth / screenGLWidth), _height * (screenPXHeight / screenGLHeight));
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	gluPerspective (_fovy, _width/_height, _near, _far);
	
	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity ();
	
	glEnable (GL_DEPTH_TEST);
	glClear (GL_DEPTH_BUFFER_BIT);
	
	gluLookAt (_eye.x, _eye.y, _eye.z, _look.x, _look.y, _look.z, 0.0, 1.0, 0.0);
	
	GUI_SendSignal (renderingSignal);
	
	glFlush();
}

void GUIViewport::lookAt (Vector3D eye, Vector3D look)	{
	_eye = eye;		_look = look;
}
void GUIViewport::lookAt (double eyeX, double eyeY, double eyeZ, double lookX, double lookY, double lookZ)	{
	_eye.x = eyeX;		_eye.y = eyeY;		_eye.z = eyeZ;
	_look.x = lookX;	_look.y = lookY;	_look.z = lookZ;
}

void GUIViewport::perspective (double fovy, double znear, double zfar)	{
	_fovy = fovy;
	_near = znear;
	_far = zfar;
}
