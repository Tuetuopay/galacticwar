//
//  GUILib.h
//  PokeFPS
//
//  Created by Alexis on 09/08/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_LIB_H
#define _GUI_LIB_H

#include "GUIScreen.h"

#include "GUIButton.h"
#include "GUICheckBox.h"
#include "GUIImageButton.h"
#include "GUILabel.h"
#include "GUISpinBox.h"
#include "GUITabBar.h"
#include "GUITableView.h"
#include "GUITextEdit.h"
#include "GUIViewport.h"

#endif
