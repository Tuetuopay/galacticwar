//
//  GUITableViewCell.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_TABLE_VIEW_CELL_H
#define _GUI_TABLE_VIEW_CELL_H

#include "OSCompatibility.h"

#include "GUIButton.h"

#define GUI_CELL_DEFAULT_HEIGHT	1.2

class GUITableViewCell : public GUIButton
{
public:
	GUITableViewCell (std::string label, GUI* parent = NULL, double width = -1);
	~GUITableViewCell ();
	
	//void render ();
	
	void setShown (bool shown);
	bool shown ();
	
protected:
	bool _shown;
};

#endif /* defined(_GUI_TABLE_VIEW_CELL_H) */
