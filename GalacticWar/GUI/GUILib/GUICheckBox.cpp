//
//  GUICheckBox.cpp
//  PokeFPS
//
//  Created by Alexis on 12/12/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUICheckBox.h"

GLuint GUICheckBox::_texChecked = 0;
GLuint GUICheckBox::_texUnchecked = 0;

GUICheckBox::GUICheckBox (double x, double y, std::string text, GUI* parent) : GUIImageButton (x, y, 0, parent)	{
	if (!_texChecked)
		_texChecked = loadTexture ("GUI/check.png");
	if (!_texUnchecked)
		_texUnchecked = loadTexture ("GUI/uncheck.png");
	
	setToggleable (true);
	setToggled (false);
	
	_label = new GUILabel (1.5, 0, text);
	
	toggledUp (unchecked, this);
	toggledDown (checked, this);
	
	_description = "GUICheckBox";
}

GUICheckBox::~GUICheckBox ()	{
	
}

void GUICheckBox::render ()	{
	if (isToggled ())
		setTexure (_texChecked);
	else
		setTexure (_texUnchecked);
	GUIImageButton::render ();
	
	if (_isHovered)			glColor3f (0.8, 0.8, 1.0);
	else					glColor3f (1.0, 1.0, 1.0);
	
	glPushMatrix();
	glTranslatef (_x, _y, 0.0);
	_label->render ();
	glPopMatrix();
}

void GUICheckBox::checked ()	{
	GUI_SendSignal (onCheckS);
}
void GUICheckBox::unchecked ()	{
	GUI_SendSignal (onUncheckS);
}







