//
//  GUISignalSlot.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUISignalSlot.h"
#include <iostream>

GUITrigger::GUITrigger ()	{
	callback = NULL;
	objCallback = NULL;
	objTarget = NULL;
}

void GUI_SendSignal (GUITrigger trigger)	{
	GUI_SendSignal (trigger.callback);
	GUI_SendSignal (trigger.objCallback, trigger.objTarget);
}
void GUI_SendSignal (GUISignal signal)	{
	if (signal)
		(*signal) ();
}
void GUI_SendSignal (GUIObjectSignal signal, void* target)	{
	if (signal && target)
		(*signal) (target);
}

