//
//  GUISpinBox.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "GUISpinBox.h"

GUISpinBox::GUISpinBox (double x, double y, GUI* parent, int min, int max) : GUIElement(_textEngine.getNumberWidth(max) + _GUI_SPIN_BOX_BTN_SCALE, 1.2, x, y, parent)
{
	_value = min;
	_min = min;		_max = max;
	
	_btnDown = new GUIImageButton	(x + _width - _GUI_SPIN_BOX_BTN_SCALE, y + (_height - _GUI_SPIN_BOX_BTN_SCALE), "GUI/ArrowDown.png");
	_btnUp = new GUIImageButton		(x + _width - _GUI_SPIN_BOX_BTN_SCALE, y, "GUI/ArrowUp.png");
	_lblDisplay = new GUILabel		(x + 0.1, y + 0.1, _valueString);
	
	_btnUp->resize (_GUI_SPIN_BOX_BTN_SCALE, _GUI_SPIN_BOX_BTN_SCALE);
	_btnDown->resize (_GUI_SPIN_BOX_BTN_SCALE, _GUI_SPIN_BOX_BTN_SCALE);
	
	_btnUp->onClick (on_btnUp_clicked, this);
	_btnDown->onClick (on_btnDown_clicked, this);
	
	_updateValueString();
	
	_description = "GUISpinBox";
}

GUISpinBox::~GUISpinBox()	{
	delete _btnDown;
	delete _btnUp;
	delete _lblDisplay;
}

bool GUISpinBox::processMouseButton (double x, double y, SDL_MouseButtonEvent event)	{
	if (event.button != SDL_BUTTON_LEFT)
		return false;
	
	_btnDown->processMouseButton (x, y, event);
	_btnUp->processMouseButton (x, y, event);
	
	return isInsideMe (x, y);
}

void GUISpinBox::mouseMotion (double x, double y, bool isClicking)	{
	_btnUp->mouseMotion (x, y, isClicking);
	_btnDown->mouseMotion (x, y, isClicking);
}

void GUISpinBox::render ()	{	
	glPushMatrix();
	glTranslated (_x, _y, 0.0);
	
	static double lastX = _x, lastY = _y;
	if (lastX != _x)	{
		lastX = _x;
		_updateSubWidgets ();
	}
	if (lastY != _y)	{
		lastY = _y;
		_updateSubWidgets ();
	}
	
	/* First, rendering the background */
	_textEngine.drawBackground ((float)_width, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0);
	glPopMatrix();
	
	/* Then, the text */
	_lblDisplay->render();
	
	/* Finally, the buttons */
	_btnDown->render ();
	_btnUp->render ();
}

void GUISpinBox::_updateValueString()	{
	static char str[100] = "";
	sprintf (str, "%d", _value);
	_valueString = std::string (str);
	
	_lblDisplay->setText (_valueString);
}

void GUISpinBox::on_btnUp_clicked ()	{
	if (_value + 1 <= _max)	_value++;
	_updateValueString();
	GUI_SendSignal(valueChangedSignal);
}

void GUISpinBox::on_btnDown_clicked()	{
	if (_value - 1 >= _min)	_value--;
	_updateValueString();
	GUI_SendSignal(valueChangedSignal);
}

void GUISpinBox::setValue(int value)	{
	if (value >= _min && value <= _max)
		_value = value;
	_updateValueString();
}

void GUISpinBox::setRange (int min, int max)	{
	_min = min;
	_max = max;
	
	if (_value > _max)	_value = _max;
	else if (_value < _min)	_value = _min;
}

void GUISpinBox::_updateSubWidgets ()	{
	_btnDown->move (_x + _width - _GUI_SPIN_BOX_BTN_SCALE, _y + (_height - _GUI_SPIN_BOX_BTN_SCALE));
	_btnUp->move (_x + _width - _GUI_SPIN_BOX_BTN_SCALE, _y);
	_lblDisplay->move (_x + 0.1, _y + 0.1);
}
