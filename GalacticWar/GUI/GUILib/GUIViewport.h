//
//  GUIViewport.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _GUI_VIEWPORT_H
#define _GUI_VIEWPORT_H

#include "OSCompatibility.h"	/* OpenGL headers here. */

#include "GUIElement.h"

#include "GFX/3DUtils/Vector3D.h"

/* This Element's structure and behavior needs some explanations. Because others viewports rendering
 * must occur after everything else is finished, when the standard render func is called, we will flag
 * the current object as "flagged for rendering". This way, when a GUI is hidden, real rendering code
 * won't be called => no viewport rendering when GUI is hidden.
 * So when the viewport is flagged for rendering, the doRender method will setup the viewport and then
 * call the rendering code (meshes, etc ...) through the the signal. Powerful way of handling a flexible
 * render method ;)
 ****/

class GUIViewport : public GUIElement
{
public:
	GUIViewport (double x, double y, double width, double height, GUI *parent = NULL);	/* Well ... No defaut value :P */
	~GUIViewport ();
	
	/* Standard render func ... No rendering code here */
	void render ();
	/* REAL rendering code goes here (viewport setup in fact ...) */
	void doRender ();
	
	/* To avoid making child classes for rendering, this is the callback */
	GUI_SIGNAL (rendering, renderingSignal)
	
	/* gluLookAt parameters */
	void lookAt (Vector3D eye, Vector3D look);
	void lookAt (double eyeX, double eyeY, double eyeZ, double lookX, double lookY, double lookZ);
	void perspective (double fovy = 70.0, double near = 0.5, double far = 30.0);
	
protected:
	/* If the render () func was called, this will be set to true, so render will be effective. */
	bool flaggedForRender;
	
	/* gluLookAt parameters */
	Vector3D _eye, _look;
	double _fovy, _near, _far;
	
	/* To obtain parent's coordinates, else the viewports wouldn't be placed correctly on screen */
	GUI *_parent;
};

#endif /* defined(_GUI_VIEWPORT_H) */
