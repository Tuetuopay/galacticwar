//
//  ClientListener.cpp
//  GalacticWar
//
//  Created by Alexis on 17/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "ClientListener.h"
#include "Network.h"
#include "Packet.h"

#include "GFX/3DUtils/Vector3D.h"

#include "Game/GameEngine.h"

#define BUFFER_SIZE	1024

ClientListener::ClientListener (Network::ClientDesc *client, unsigned long id) : Thread ()	{
	_id = id;
	this->client = *client;
}

void ClientListener::run ()	{
	Log::log << "Now listening " << Network::getIp (client.address) << std::endl;;
	
	unsigned char buf[BUFFER_SIZE];
	long n = 0;
	
	while (1)	{
		bzero (buf, BUFFER_SIZE);
		n = read (client.socket, buf, BUFFER_SIZE - 1);
		if (n > 0)	{
			Log::log << "Read " << n << " bytes from client." << std::endl;
			Packet<Network::Generic> pGen (buf, n);
			Packet<std::string> p = Packet<std::string>::toType (pGen);
			
			// Log::log << "Client " << Network::getIp (client.address) << " sent packet \"" << p.id () << "\" containing \"" << p.value << "\"." << std::endl;
			
			queuePush (pGen);
		}
	}
}

bool ClientListener::send (const char *buf, size_t buflen)	{
	return write (client.socket, buf, buflen) >= 0;
}
bool ClientListener::send (std::string buf)	{
	return send (buf.c_str (), buf.length ());
}

std::string ClientListener::describe () const	{
	return Network::getIp (client.address);
}

ClientListener::~ClientListener ()	{
	stop ();
	join ();
	close (client.socket);
}