//
//  ConnexionListener.h
//  GalacticWar
//
//  Created by Alexis on 17/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _CONNEXION_LISTENER_H
#define _CONNEXION_LISTENER_H

#include "OSCompatibility.h"
#include "Network.h"
#include "Thread.h"

class ConnexionListener : public Thread	{
public:
	ConnexionListener ();
	ConnexionListener (int port);
	~ConnexionListener ();
	
	void run ();
	int setup (int port);
	
	bool hasPendingClient () const { return _clientsSocks.size () > 0; }
	Network::ClientDesc* dequeueClient ();
	
private:
	int mainSocket, _port, n;
	
	struct sockaddr_in	servAddr;
	std::vector<struct sockaddr_in*>	clientsAddr;
	
	bool _startupSucceeded;
	
	std::queue<Network::ClientDesc*>	_clientsSocks;
};

#endif /* defined(_ConnexionListener_H) */