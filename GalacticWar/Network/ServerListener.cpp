//
//  ServerListener.cpp
//  GalacticWar
//
//  Created by Alexis on 18/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "ServerListener.h"

#define BUFFER_SIZE	1024

ServerListener::ServerListener (std::string hostname, int port) : Thread ()	{
	_startupSucceeded = false;
	_port = port;
	_hostname = hostname;
	
	_socket = socket (AF_INET, SOCK_STREAM, 0);
	if (!_socket)	{
		Log::log << "ERROR : Could not create client socket." << std::endl;
		return;
	}
	
	_server = gethostbyname (hostname.c_str ());
	if (!_server)	{
		Log::log << "ERROR : no such host \"" << hostname << ":" << _port << "\"." << std::endl;
		return;
	}
	
	bzero ((void *)&_servAddr, sizeof (_servAddr));
	_servAddr.sin_family = AF_INET;
	bcopy ((char *)_server->h_addr, (char *)&_servAddr.sin_addr.s_addr, _server->h_length);
	_servAddr.sin_port = htons (_port);
	if (connect (_socket, (struct sockaddr*)&_servAddr, sizeof (_servAddr)) < 0)	{
		Log::log << "ERROR : could not connect to server \"" << hostname << ":" << _port << "\"." << std::endl;
		return;
	}
	
	Log::log << "Now connected to server \"" << hostname << ":" << _port << "\"." << std::endl;
	
	_startupSucceeded = true;
}

void ServerListener::run ()	{
	if (!_startupSucceeded)
		return;
	
	Log::log << "Now listening server " << _hostname << ":" << _port << std::endl;
	
	unsigned char buf[BUFFER_SIZE];
	long n = 0;
	
	while (1)	{
		bzero (buf, BUFFER_SIZE);
		n = read (_socket, buf, BUFFER_SIZE - 1);
		// if (n >= 0)
			// Log::log << "Server " << _hostname << ":" << _port << " sent \"" << buf << "\"." << std::endl;;
		
		if (n > 0)	{
			queuePush (Packet<Network::Generic> (buf, n));
			
			Packet<std::string> p (buf, n);
			
			Log::log << "Server sent " << n << " bytes : \"" << p.id () << "\" containing \"" << p.value << "\"." << std::endl;
		}
	}
}

bool ServerListener::send (const char *buf, size_t buflen)	{
	return write (_socket, buf, buflen) >= 0;
}
bool ServerListener::send (std::string buf)	{
	return send (buf.c_str (), buf.length ());
}

ServerListener::~ServerListener ()	{
	stop ();
	join ();
	close (_socket);
}