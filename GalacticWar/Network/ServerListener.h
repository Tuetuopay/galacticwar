//
//  ServerListener.h
//  GalacticWar
//
//  Created by Alexis on 18/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _SERVER_LISTENER_H
#define _SERVER_LISTENER_H

#include "OSCompatibility.h"

#include "System/Thread.h"
#include "System/Queuing.h"

#include "Packet.h"

class ServerListener : public Thread, public Queuing<Packet<Network::Generic>>	{
public:
	ServerListener (std::string hostname, int port);
	~ServerListener ();
	
	void run ();
	
	bool send (const char *buf, size_t buflen);
	bool send (std::string buf);
	
	inline bool startupSucceeded () const { return _startupSucceeded; }
	
private:
	int _socket, _port;
	
	struct sockaddr_in _servAddr;
	struct hostent *_server;
	
	std::string _hostname;
	
	bool _startupSucceeded;
};

#endif /* defined (_SERVER_LISTENER_H) */