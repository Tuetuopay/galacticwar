//
//  ConnexionListener.cpp
//  GalacticWar
//
//  Created by Alexis on 17/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "ConnexionListener.h"

#include "NetworkEngine.h"

ConnexionListener::ConnexionListener () : Thread ()	{
	_startupSucceeded = false;
}
ConnexionListener::ConnexionListener (int port) : Thread ()	{
	_startupSucceeded = false;
	setup (port);
}

int ConnexionListener::setup (int port)	{
	if (_startupSucceeded)	return _port;
	
	if (port <= 1024 || port > 0xFFFF)
		port = 5400;
	_port = port;
	
	Log::log << "Starting Network Engine on port " << port << std::endl;
	if ((mainSocket = socket (AF_INET, SOCK_STREAM, 0)) < 0)	{
		Log::log << "ERROR : could not create server socket." << std::endl;
		return 0;
	}
	bzero ((void*)&servAddr, sizeof (servAddr));
	
	servAddr.sin_family = INADDR_ANY;
	servAddr.sin_addr.s_addr = INADDR_ANY;
	servAddr.sin_port = htons (port);
	if (bind (mainSocket, (struct sockaddr*)&servAddr, sizeof (servAddr)) < 0)	{
		Log::log << "ERROR : could not bind socket to port " << port << "." << std::endl;
		return 0;
	}
	
	_startupSucceeded = true;
	
	return _port;
}

void ConnexionListener::run ()	{
	if (!_startupSucceeded)	return;
	
	int cliLen = sizeof (struct sockaddr_in);
	Network::ClientDesc *client;
	
	while (1)	{
		listen (mainSocket, 5);
		
		client = new Network::ClientDesc;
		
		client->socket = accept (mainSocket, (struct sockaddr*)&(client->address), (socklen_t*) &cliLen);
		if (client->socket >= 0)	{
			_clientsSocks.push (client);
			Log::log << "Client " << Network::getIp (client->address) << " connected." << std::endl;;
		}
	}
}

Network::ClientDesc* ConnexionListener::dequeueClient ()	{
	Network::ClientDesc *s = _clientsSocks.front ();
	_clientsSocks.pop ();
	return s;
}

ConnexionListener::~ConnexionListener ()	{
	
}