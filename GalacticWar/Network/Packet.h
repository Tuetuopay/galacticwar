//
//  Packet.h
//  GalacticWar
//
//  Created by Alexis on 20/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _PACKET_H
#define _PACKET_H

#include "OSCompatibility.h"

/* I learnt somethign really important while writing this class that caused me a lot of troubles.
 * You have a class T (Vector3D for example, containing 3 doubles with a bunch of methods : length, normalize, etc...) :
 * You have an instance vec of this class : Vector3D vec (42.1337, 9.11, 1664.1);
 * Then		sizeof (Vector3D) = sizeof (double) * 3
 * While	sizeof (vec) = sizeof (Vector3D° + size of the pointers to the methods
 * Anyway, sizeof(instance) > sizeof(class).
 *
 * And I could deduce that, the sizeof(class) first bytes in memory of an instance are the data bytes, then followed
 * by pointers.
 */

namespace Network {
	typedef struct {
		void* v;
		size_t size;
	} Generic;
}

template<class T>
class Packet	{
public:
	inline Packet (uint32 id, T v) { _id = id; value = v; };
	// inline Packet (const Packet<Network::Generic>& p) { *this = Packet<T>::toType (p); }
	Packet (const void *buffer, size_t buflen = -1);
	inline ~Packet () {}
	
	T value;
	
	virtual void* serialize () const;
	virtual size_t serialize (void *buf) const;
	
	inline uint32 id () const { return _id; }
	inline void setId (uint32 id) { _id = id; }
	
	virtual size_t serializedSize () const;
	
	static Packet<T> packetFromBuffer (const void *buffer);
	static Packet<T> packetFromBuffer (const void *buffer, size_t buflen);
	
	static Packet<T> toType (const Packet<Network::Generic>& packet);
	static void toType (Packet<T>& dst, const Packet<Network::Generic>& packet);
	
	virtual Packet<T> operator= (const Packet<T>& p);
	
	inline T& operator-> () { return value; }
	
protected:
	uint32 _id;
};

#define ID_SIZE	sizeof (uint32)

template<class T>
Packet<T>::Packet (const void *buffer, size_t buflen)	{
	if (buflen == -1)	buflen = sizeof *((const char*)buffer);
	
	/* Packet ID */
	bcopy (buffer, &_id, ID_SIZE);
	
	/* Content */
	bcopy ((char*)buffer + ID_SIZE, (void*)&value, sizeof(T));
}
template<>
Packet<std::string>::Packet (const void *buffer, size_t buflen);
template<>
Packet<Network::Generic>::Packet (const void *buffer, size_t buflen);

template<class T>
size_t Packet<T>::serializedSize () const	 {
	return sizeof(_id) + sizeof(T);
}
template<>
size_t Packet<std::string>::serializedSize () const;
template<>
size_t Packet<Network::Generic>::serializedSize () const;

template<class T>
void* Packet<T>::serialize () const	{
	char *buf = NULL;
	
	if (!(buf = (char*)malloc (serializedSize ())))
		return NULL;
	
	serialize (buf);
	
	return buf;
}
template<class T>
size_t Packet<T>::serialize (void *buf) const	{
	bcopy (&_id, buf, sizeof (_id));
	bcopy (&value, ((char*)buf) + sizeof(_id), sizeof (T));
	
	return serializedSize ();
}
template<>
size_t Packet<std::string>::serialize (void *buf) const;
template<>
size_t Packet<Network::Generic>::serialize (void *buf) const;
template<class T>
Packet<T> Packet<T>::operator= (const Packet<T>& p)	{
	value = p.value;
	_id = p._id;
	return *this;
}

template<class T>
Packet<T> Packet<T>::packetFromBuffer (const void *buffer, size_t buflen)	{
	return Packet<T> (buffer, buflen);
}
template<class T>
Packet<T> Packet<T>::packetFromBuffer (const void *buffer)	{
	return Packet<T>::Packet (buffer, -1);
}

template<class T>
Packet<T> Packet<T>::toType (const Packet<Network::Generic>& packet)	{
	return Packet<T> (packet.value.v, packet.value.size);
}
template<class T>
void Packet<T>::toType (Packet<T> &dst, const Packet<Network::Generic> &packet)	{
	dst = toType (packet);
}

#endif /* defined (_PACKET_H) */






