//
//  ClientPacket.h
//  GalacticWar
//
//  Created by Alexis on 29/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _CLIENT_PACKET_H
#define _CLIENT_PACKET_H

#include "Packet.h"

class ClientPacket	{
public:
	ClientPacket (unsigned long id, Packet<Network::Generic> p);
	~ClientPacket ();
	
	unsigned long clientID;
	Packet<Network::Generic> packet;
};

#endif /* defined (_CLIENT_PACKET_H) */