//
//  ClientListener.h
//  GalacticWar
//
//  Created by Alexis on 17/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _CLIENT_LISTENER_H
#define _CLIENT_LISTENER_H

#include "OSCompatibility.h"

#include "Network.h"
#include "Packet.h"

#include "System/Thread.h"
#include "System/Queuing.h"

class ClientListener : public Thread, public Queuing<Packet<Network::Generic>>	{
public:
	ClientListener (Network::ClientDesc *client, unsigned long id);
	~ClientListener ();
	
	void run ();
	
	bool send (const char *buf, size_t buflen);
	bool send (std::string buf);
	
	std::string describe () const;
	
	inline unsigned long id () const { return _id; }
	
private:
	Network::ClientDesc client;
	
	unsigned long _id;
};

#endif /* defined (_CLIENT_LISTENER_H) */