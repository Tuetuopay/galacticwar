//
//  Network.cpp
//  GalacticWar
//
//  Created by Alexis on 18/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "Network.h"

namespace Network {
	std::string getIp (const in_addr_t &s_addr)	{
		char str[INET_ADDRSTRLEN];
		inet_ntop (AF_INET, &s_addr, str, INET_ADDRSTRLEN);
		return std::string (str);
	}
	std::string getIp (const sockaddr_in &client)	{
		return getIp (client.sin_addr.s_addr);
	}
}
