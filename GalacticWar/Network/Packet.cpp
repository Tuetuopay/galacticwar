//
//  Packet.cpp
//  GalacticWar
//
//  Created by Alexis on 20/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "Packet.h"

/*/
template<class T>
Packet<T>::Packet (std::string name, T v)	{
	_name = name;
	value = v;
}

template <class T>
const void* Packet<T>::serialize () const	{
	return (char*)this;
}

template <class T>
Packet<T>::~Packet ()	{
	
}
//*/

template<>
Packet<std::string>::Packet (const void *buffer, size_t buflen)	{
	if (buflen == -1)	buflen = sizeof *((const char*)buffer);
	
	/* Packet ID */
	bcopy (buffer, &_id, ID_SIZE);
	
	/* Content */
	size_t tSize = buflen - ID_SIZE;
	char *text = new char[tSize + 1];
	bcopy ((char*)buffer + ID_SIZE, (void*)text, tSize);
	text[tSize] = '\0';
	value = text;
}
template<>
Packet<Network::Generic>::Packet (const void *buffer, size_t buflen)	{
	if (buflen == -1)	buflen = sizeof *((const char*)buffer);
	
	/* Packet ID */
	bcopy (buffer, &_id, ID_SIZE);
	
	/* Content */
	value.v = malloc (buflen);
	bcopy (buffer, value.v, buflen);
	
	value.size = buflen;
}

template<>
size_t Packet<std::string>::serializedSize () const	{
	return sizeof(_id) + value.length ();
}
template<>
size_t Packet<Network::Generic>::serializedSize () const	{
	return value.size;
}
template<>
size_t Packet<std::string>::serialize (void *buf) const	{
	bcopy (&_id, buf, sizeof (_id));
	bcopy (value.c_str (), ((char*)buf) + sizeof(_id), value.length ());
	
	return serializedSize ();
}
template<>
size_t Packet<Network::Generic>::serialize (void *buf) const	{
	bcopy (&_id, buf, sizeof (_id));
	bcopy (value.v, ((char*)buf) + sizeof(_id), value.size - ID_SIZE);
	Log::log << "Spec ser called" << std::endl;
	return serializedSize ();
}




