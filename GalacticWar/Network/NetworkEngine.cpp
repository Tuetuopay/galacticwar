//
//  NetworkEngine.cpp
//  GalacticWar
//
//  Created by Alexis on 17/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "NetworkEngine.h"

#include "Game/PacketList.h"

NetworkEngine::NetworkEngine (int port) : _connexionListener ()	{
	_serverListener = NULL;
	_isServer = true;
	
	_startupSucceeded = _connexionListener.setup (port);
	if (_startupSucceeded)
		_connexionListener.start ();
}
NetworkEngine::NetworkEngine (std::string host, int port) : _connexionListener ()	{
	_isServer = false;
	
	_serverListener = new ServerListener (host, port);
	_startupSucceeded = _serverListener->startupSucceeded ();
	if (_startupSucceeded)
		_serverListener->start ();
}

void NetworkEngine::update ()	{
	if (!_startupSucceeded)	return;
	
	if (_isServer)	{
		while (_connexionListener.hasPendingClient ())	{
			ClientListener *c = new ClientListener (_connexionListener.dequeueClient (), _clients.size ());
			c->start ();
			_clients.push_back (c);
		}
		
		/* Transmitting all packets to clients */
		Packet<unsigned long> pId (Game::CLIENT_ID, 0);
		for (ClientListener *cl : _clients)	{
			if (cl->queueEmpty ())	continue;
			
			pId.setId (cl->id ());
			send (&pId);
			
			while (!cl->queueEmpty ())	{
				ClientPacket cp (cl->id (), cl->queueFront ());
				if (cp.packet.id () == Game::CHAT_MSG && 1==2)	{
					Packet<std::string> r = Packet<std::string>::toType (cp.packet);
					Log::log << "Transmitting chat packet with value " << r.value << std::endl;
					send (&r);
				}
				else
					send ((char*)cp.packet.value.v, cp.packet.value.size);
				queuePush (cp);
				cl->queuePop ();
			}
		}
	}
	else	{
		static unsigned long lastClientID = 0;
		while (!_serverListener->queueEmpty ())	{
			Packet<Network::Generic> pg = _serverListener->queueFront ();
			if (pg.id () == Game::CLIENT_ID)	{
				Packet<unsigned long> p = Packet<unsigned long>::toType (pg);
				lastClientID = p.value;
			}
			else
				queuePush (ClientPacket (lastClientID, pg));
			_serverListener->queuePop ();
		}
	}
}

/*/
template <class T>
bool NetworkEngine::send (Packet<T> *packet)	{
	return send ((const char*)packet->serialize (), sizeof (packet));
}
//*/
bool NetworkEngine::send (const char *buf, size_t buflen)	{
	if (!_startupSucceeded)	return false;
	
	if (_isServer)	{
		bool ok = true;
		for (ClientListener *client : _clients)	{
			if (!client->send (buf, buflen))	{
				Log::log << "ERROR : failed to send " << buflen << " bytes to client \"" << client->describe () << "\"." << std::endl;
				ok = false;
			}
		}
		return ok;
	}
	else
		return _serverListener->send (buf, buflen);
}

NetworkEngine::~NetworkEngine ()	{
	if (_serverListener)
		delete _serverListener;
	for (ClientListener *client : _clients)
		delete client;
}




