//
//  ClientPacket.cpp
//  GalacticWar
//
//  Created by Alexis on 29/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "ClientPacket.h"

ClientPacket::ClientPacket (unsigned long id, Packet<Network::Generic> p) : packet (p)	{
	clientID = id;
}

ClientPacket::~ClientPacket ()	{
	
}