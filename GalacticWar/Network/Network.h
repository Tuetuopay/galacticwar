//
//  NetTypes.h
//  GalacticWar
//
//  Created by Alexis on 18/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _NETWORK_H
#define _NETWORK_H

#include "OSCompatibility.h"

namespace Network {
	typedef struct	{
		int socket;
		sockaddr_in address;
	} ClientDesc;
	
	std::string getIp (const in_addr_t &s_addr);
	std::string getIp (const sockaddr_in &client);
}

#endif
