//
//  NetworkEngine.h
//  GalacticWar
//
//  Created by Alexis on 17/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _NETWORK_ENGINE_H
#define _NETWORK_ENGINE_H

#include "OSCompatibility.h"

#include "ConnexionListener.h"
#include "ClientListener.h"
#include "ServerListener.h"

#include "Packet.h"

#include "System/Queuing.h"
#include "ClientPacket.h"

class NetworkEngine : public Queuing<ClientPacket>	{
public:
	/* Server constructor */
	NetworkEngine (int port = 5400);
	/* Client constructor */
	NetworkEngine (std::string host, int port = 5400);
	~NetworkEngine ();
	
	inline bool startupComplete () const { return _startupSucceeded; }
	
	inline static NetworkEngine* instance () { return _instance; }
	
	void update ();
	
	template <class T>
	bool send (Packet<T> *packet);
	
private:
	int port;
	
	bool _startupSucceeded, _isServer;
	
	/* ===== SERVER ===== */
	// Main connexion listener
	ConnexionListener _connexionListener;
	
	// All clients
	std::vector<ClientListener*>	_clients;
	
	/* ===== CLIENT ===== */
	ServerListener	*_serverListener;
	
	bool send (const char *buf, size_t buflen);
	
	static NetworkEngine *_instance;
};

template<class T>
bool NetworkEngine::send (Packet<T> *packet)	{
	char *data = (char*)packet->serialize ();
	bool res = send (data, packet->serializedSize ());
	delete data;
	return res;
}

#endif /* defined (_NETWORK_ENGINE_H) */