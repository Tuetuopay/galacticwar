//
//  OSCompatibility.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

/* This file is intended to bypass differences between differents OSes,
 * with system definitions and other stuff.
 *
 * Every #include of members of the C++ STL should be added in this file.
 */

#ifndef _OS_COMPATIBILITY_H
#define _OS_COMPATIBILITY_H

/* # # # # # # # # # # COMMON CODE # # # # # # # # # # */
/* STL includes */
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <limits>
#include <errno.h>
#include <map>
#include <set>
#include <queue>
#include <pthread.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#ifndef _USE_MATH_DEFINES	/* M_PI & co consts */
# define _USE_MATH_DEFINES
#endif
#include <cmath>

/* WOW my very first shared header ! Such compatibility */
#include "System/Log.h"
/* Very logger */

std::string stripFolderSeparators (std::string path);
std::string shortenPath (std::string path);
std::string getDirName (std::string path);
void mkdir (std::string path);

long clamp (long value, long min, long max);

#if UINT_MAX >> 32 == 0
 typedef unsigned int uint32;
#elif ULONG_MAX >> 32 == 0
 typedef unsigned long uint32;
#else
 typedef unsigned long int uint32;
#endif
#if INT_MAX >> 32 == 0
 typedef int int32;
#elif LONG_MAX >> 32 == 0
 typedef long int32;
#else
 typedef long int int32;
#endif

#if (ULONG_MAX >> 32) >> 32 == 0		/* two >> operators, else will cause an overflow */
 typedef unsigned long uint64;
#elif (ULONG_LONG_MAX >> 32) >> 32 == 0
 typedef unsigned long long uint64;
#else
 typedef unsigned long long int uint64;	/* BTW, don't know if "unsigned long long int" exists xD */
#endif

#if (LONG_MAX >> 32) >> 32 == 0
 typedef long int64;
#elif (LLONG_MAX >> 32) >> 32 == 0
 typedef long long int64;
#else
 typedef long long int int64;
#endif

#define UINT32_LAST_BIT	(1 << 31)

/* # # # # # # # # # # OS SPECIFIC CODE # # # # # # # # # # */
/* Windows specific code */
#ifdef _WIN32
/* GFX headers */
# include <SDL2/SDL.h>
# include <SDL2/SDL_image.h>
# include <gl/glew.h>

# ifndef __WIN32__	/* Let's make a unified define */
#  define __WIN32__
# endif

/* dirent.h is NOT part of the Win32 standard library, so I ship it with the project */
/* renamed to "windirent" because it would be included in other OSes since I set the project root folder
 * as include provider */
# include "windirent.h"

/* dirent includes windef, but this header is providing HUGE namespace pollution, including defining 'near' and 'far' ... */
/* okay these are glitchy >_<" */
// # undef near
// # undef far

/* Following code has been extracted from GLUT.h file, cuz' these defines are needed on Windows,
 * but heh, windows.h is making a huge namespace pollution so let's define only the needed :P
 * (and because I am lazy, this is a huge copy/paste from GLUT.h until the #pragma stuff, so credit 
 * to GLUT's creators :P)
 */
# ifndef APIENTRY
#  define GLUT_APIENTRY_DEFINED
#  if (_MSC_VER >= 800) || defined(_STDCALL_SUPPORTED)
#   define APIENTRY    __stdcall
#  else
#   define APIENTRY
#  endif
# endif
/* XXX This is from Win32's <winnt.h> */
# ifndef CALLBACK
#  if (defined(_M_MRX000) || defined(_M_IX86) || defined(_M_ALPHA) || defined(_M_PPC)) && !defined(MIDL_PASS)
#   define CALLBACK __stdcall
#  else
#   define CALLBACK
#  endif
# endif
/* XXX This is from Win32's <wingdi.h> and <winnt.h> */
# ifndef WINGDIAPI
#  define GLUT_WINGDIAPI_DEFINED
#  define WINGDIAPI __declspec(dllimport)
# endif
/* XXX This is from Win32's <ctype.h> */
# ifndef _WCHAR_T_DEFINED
	typedef unsigned short wchar_t;
#  define _WCHAR_T_DEFINED
# endif

# pragma warning (disable:4244)	/* Disable bogus conversion warnings. */
# pragma warning (disable:4305) /* VC++ 5.0 version of above warning. */
# pragma warning (disable:4018)	/* Disable comparation warnings (>, <, >=, <=) between signed/unsigned */
# pragma warning (disable:4996)	/* Disable warnings "'func': this function or variable may be unsafe. Consider using func_s instead. Just in case following define doesn't work (My case) */
# ifndef _CRT_SECURE_NO_WARNINGS
#  define _CRT_SECURE_NO_WARNINGS	/* Disable warnings "'func': this function or variable may be unsafe. Consider using func_s instead */
# endif

/* Now some corrections */
/* Cube root isn't defined on Windows' cmath header */
# ifndef cbrtf
#  define cbrtf(a)	pow(a,1.0/3.0)
# endif
/* fmin & fmax aren't defined on windows either */
# ifndef fmin
#  define fmin(a,b)	((a < b) ? a : b)
# endif
# ifndef fmax
#  define fmax(a,b)	((a > b) ? a : b)
# endif
#endif	/* defined(_WIN32) */

#ifndef _WIN32	
/* I am placing it there because it is a standard on all Unix platforms (roughly everything except Win32) */
# include <dirent.h>
#endif

/* # # # # # # # # # # OS X Specific # # # # # # # # # # */
/* I am not an Apple fanboy but I'd say that things are a lot easier on Macs
 * *thank you Apple for using POSIX STL, using C++11 and giving developer a FREE POWERFUL
 * IDE Xcode (far better than MS VS)*
 */
#ifdef __APPLE__
# include <SDL2/SDL.h>
# include <SDL2_image/SDL_image.h>
# include <OpenGL/gl.h>
# include <OpenGL/glu.h>
# include <GLUT/GLUT.h>

/* Using a unified define */
# ifndef __OSX__
#  define __OSX__
# endif
#endif

/* Bit easier on Linux, except that the GCC command is a pain to type and build >_<" */
#ifdef __linux__
/* Getting SDL2 under Linux is a bit harder as it is not in main repos
 * you need to buid it for yourself. As of now, I don't have any Linux machine around,
 * so I cant' really test. I'll assume that since it is installed manually, you'll
 * place it in /usr/local/include/SDL2/ or /usr/include/SDL2/
 */
# include <SDL2/SDL.h>
# include <SDL2/SDL_image.h>
// # include <GL/gl.h>
// # include <GL/glu.h>
# include <GL/glew.h>

/* I do prefer capital letters xD */
# ifndef __LINUX__
#  define __LINUX__
# endif
#endif /* defined(__linux__) */

/* # # # # # # # # # # *more* COMMON CODE # # # # # # # # # # */
#define DEG2RAD(alpha)  ((alpha/180.0)*M_PI)
#define RAD2DEG(alpha)  ((alpha*180.0)/M_PI)
/* Math defines */
#ifndef M_E
#define M_E         2.71828182845904523536028747135266250   /* e              */
#endif
#ifndef M_LOG2E
#define M_LOG2E     1.44269504088896340735992468100189214   /* log2(e)        */
#endif
#ifndef M_LOG10E
#define M_LOG10E    0.434294481903251827651128918916605082  /* log10(e)       */
#endif
#ifndef M_LN2
#define M_LN2       0.693147180559945309417232121458176568  /* loge(2)        */
#endif
#ifndef M_LN10
#define M_LN10      2.30258509299404568401799145468436421   /* loge(10)       */
#endif
#ifndef M_PI
#define M_PI        3.14159265358979323846264338327950288   /* pi             */
#endif
#ifndef M_PI_2
#define M_PI_2      1.57079632679489661923132169163975144   /* pi/2           */
#endif
#ifndef M_PI_4
#define M_PI_4      0.785398163397448309615660845819875721  /* pi/4           */
#endif
#ifndef M_1_PI
#define M_1_PI      0.318309886183790671537767526745028724  /* 1/pi           */
#endif
#ifndef M_2_PI
#define M_2_PI      0.636619772367581343075535053490057448  /* 2/pi           */
#endif
#ifndef M_2_SQRTPI
#define M_2_SQRTPI  1.12837916709551257389615890312154517   /* 2/sqrt(pi)     */
#endif
#ifndef M_SQRT2
#define M_SQRT2     1.41421356237309504880168872420969808   /* sqrt(2)        */
#endif
#ifndef M_SQRT1_2
#define M_SQRT1_2   0.707106781186547524400844362104849039  /* 1/sqrt(2)      */
#endif

/* Used for multi-platforming */
#ifdef __WIN32__
# define FOLDER_SEPARATOR	'\\'
# define FOLDER_SEPARATOR_OPPOSITE	'/'
#else
# define FOLDER_SEPARATOR	'/'
# define FOLDER_SEPARATOR_OPPOSITE	'\\'
#endif

#endif /* defined(_OS_COMPATIBILITY_H) */
