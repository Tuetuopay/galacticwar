#version 120

varying vec2 texCoord;

varying vec3 N;
varying vec3 v;

void main(void)
{
	texCoord=gl_MultiTexCoord0.xy;
	gl_FrontColor = gl_Color;

	v = vec3(gl_ModelViewMatrix * gl_Vertex);
	N = normalize(gl_NormalMatrix * gl_Normal);

	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}