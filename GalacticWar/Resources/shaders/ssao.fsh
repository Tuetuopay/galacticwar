uniform sampler2D texture0;
uniform sampler2D texture1;
 
uniform vec2 camerarange;
uniform vec2 screensize;
 
varying vec2 texCoord;

#define AO_CAP			1.0
#define AO_MULTIPLIER	10000.0
#define DEPTH_TOLERANCE	0.000
#define AO_RANGE		10.0	// units in space the AO effect extends to (this gets divided by the camera far range)

float readDepth( in vec2 coord ) {
	return (2.0 * camerarange.x) / (camerarange.y + camerarange.x - texture2D( texture0, coord ).x * (camerarange.y - camerarange.x));
}

float compareDepths( in float depth1, in float depth2 ) {
	float diff = sqrt (clamp (1.0 - (depth1-depth2) / (AO_RANGE/ (camerarange.y-camerarange.x)),
							   0.0,1.0) );
	return min (AO_CAP,
				max (0.0, depth1 - depth2 - DEPTH_TOLERANCE) * AO_MULTIPLIER) * diff;
}

void main(void)
{	
	float depth = readDepth( texCoord );
	float d;

	float pw = 1.0 / screensize.x;
	float ph = 1.0 / screensize.y;

	float aoCap = 1.0;

	float ao = 0.1;

	float aoMultiplier=10000.0;

	float depthTolerance = 0.001;

	float aoscale=1.0;
	
	int passes = 4, i = 0;
	
	for (i = 0; i < passes; i++)	{
		d=readDepth( vec2(texCoord.x+pw,texCoord.y+ph));
		ao+=compareDepths(depth,d)/aoscale;

		d=readDepth( vec2(texCoord.x-pw,texCoord.y+ph));
		ao+=compareDepths(depth,d)/aoscale;

		d=readDepth( vec2(texCoord.x+pw,texCoord.y-ph));
		ao+=compareDepths(depth,d)/aoscale;

		d=readDepth( vec2(texCoord.x-pw,texCoord.y-ph));
		ao+=compareDepths(depth,d)/aoscale;
	 
		pw*=2.0;
		ph*=2.0;
		aoMultiplier/=2.0;
		aoscale*=1.2;
	}
 
	ao/=16.0;
	ao = clamp ((1.0 - ao + 0.2), 0.0, 1.0);
	
	gl_FragColor.a = texture2D (texture1, texCoord.st).a;

	if (texCoord.s <= 1.0/6.0 && texCoord.t <= 1.0/6.0)	{
		gl_FragColor.rgb = vec3 (ao); // texture2D (texture0, texCoord.st * 6.0).rgb;
	} else	{
		gl_FragColor.rgb = ao * texture2D (texture1, texCoord.st).rgb * 1.2;
	}
}
