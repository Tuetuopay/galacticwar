//#version 120

uniform sampler2D texture0;
uniform vec2 screensize;
uniform float time;

varying vec2 texCoord;

float transform (vec2 coord)	{
	return 0.92 + sin (coord.y * 200.0 + time * 2.0) * 0.08;
}
float transform2 (vec2 coord)	{
	float sine = sin (coord.y * 200.0 + time * 2.0);
	return 0.92 + (1.0 - exp (-1.0 * abs (sine))) * 0.08 * sine / abs (sine);
}

void main(void)	{
	vec2 coord = texCoord, one2 = vec2 (1.0, 1.0);
	
	coord.xy = -(one2/2.0 - coord.xy) * (2.0 * one2 - cos (one2 / 3.0 - coord.yx / 1.5)) + one2 / 2.0;
	
	//coord.y = -(0.5 - coord.y) * (2.0 - cos (0.5 / 1.5 - coord.x / 1.5)) + 0.5;
	//coord.x = -(0.5 - coord.x) * (2.0 - cos (0.5 / 1.5 - coord.y / 1.5)) + 0.5;
	
	vec3 pixel = vec3 (0, 0, 0);
	if (coord.x >= 0.0 && coord.x <= 1.0 && coord.y >= 0.0 && coord.y <= 1.0)
		pixel = texture2D (texture0, coord).rgb * transform2 (coord);
	
	gl_FragColor.rgb = pixel;
	gl_FragColor.a = 0.7;
}