#version 120

uniform sampler2D texture0;

varying vec2 texCoord;

uniform vec3 gray = vec3(0.3, 0.59, 0.11);

uniform vec3 colorScale = vec3(1.0, 1.0, 1.0);
uniform float saturation = 1.8;

void main() {
	vec3 pixel = texture2D(texture0, texCoord).rgb;

	pixel = (pixel * colorScale);
	
	// Saturation
	float luma = dot (pixel, gray);
	vec3 chroma = pixel - luma;
	pixel = (chroma * saturation) + luma;
	
	gl_FragColor = vec4(pixel, 1.0);
}
