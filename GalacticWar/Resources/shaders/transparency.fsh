#version 120

uniform sampler2D texture0;
uniform sampler2D alpha;

varying vec2 texCoord;

void main(void)
{
	gl_FragColor.rgb = texture2D(texture0, texCoord).rgb;
	gl_FragColor.a = 0.7;
}