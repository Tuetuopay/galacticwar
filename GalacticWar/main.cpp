/*
 *  main.cpp
 *  PokeFPS
 *
 *  Created by Alexis on 05/08/13.
 *  Copyright (c) 2013 Tuetuopay. All rights reserved.
 *
 */

#include "OSCompatibility.h"

#include "GFX/sdlglutils.h"

#include "Game/GameEngine.h"

int main (int argc, char ** argv)
{
	/* Changing the execution dir (the folder containing the .app
	 * to the Resources folder bundled into the .app (Application.app/Contents/Resources)
	 * package accessible via Xcode
	 * Written by Tuetuopay
	 ******/
	
#ifdef __OSX__
	char *path = (char*)malloc (strlen (argv[0]) * sizeof(char));
	if (path)	{						/* Well ... if malloc failed, let's just start the app normally ! */
		strcpy (path, argv[0]);
		int i = (int)strlen (path);
		while (path[i] != '/')	path[i--] = '\0';	/* Eliminating executable's name */
		chdir ( path );								/* Going to Application.app/Contents/MacOS */
		chdir ("../Resources/");					/* Going to Application.app/Contents/Resources */
		free (path);
	}
#endif
	
	GameEngine *gameEngine = new GameEngine ();
	
	SDL_version compiled;
	SDL_version linked;
	SDL_VERSION(&compiled);
	SDL_GetVersion(&linked);
	Log::log << "We compiled against SDL version " << (int)compiled.major << "." << (int)compiled.minor << "." << (int)compiled.patch << " ..." << std::endl;
	Log::log << "But we are linking against SDL version " << (int)linked.major << "." << (int)linked.minor << "." << (int)linked.patch << " ..." << std::endl;
	
	gameEngine->run ();
	
	SDL_Quit ();
	
	return 0;
}