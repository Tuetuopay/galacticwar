//
//  PacketList.h
//  GalacticWar
//
//  Created by Alexis on 29/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _PACKET_LIST_H
#define _PACKET_LIST_H

namespace Game {
	typedef enum : int {
		NONE, SPEED_CHANGE, FORWARD_CHANGE, ROT_CHANGE, CHAT_MSG, ROCKET_FIRE, 
		CLIENT_ID = 100
	} PacketList;
}

#endif /* defined (_PACKET_LIST_H) */
