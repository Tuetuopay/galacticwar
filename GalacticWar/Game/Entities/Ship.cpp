//
//  Ship.cpp
//  GalacticWar
//
//  Created by Alexis on 13/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "Ship.h"

#include "GameEngine.h"

float Ship::rocketAcc = 1.0;
float Ship::shipAcc = 1.0;

Ship::Ship (int id) : Entity ()	{
	_id = id;
	_buildModel ();
}
Ship::Ship (int id, const Vector3D &pos, const Vector3D &spe, const Vector3D &acc) : Entity (pos, spe, acc)	{
	_id = id;
	_buildModel ();
}
Ship::Ship (int id, const Vector3D &pos, const Vector3D &spe, const Vector3D &acc, double rot, double rSpe) : Entity (pos, spe, acc, rot, rSpe)	{
	_id = id;
	_buildModel ();
}

Ship::~Ship ()	{
}

void Ship::update (double frameLength)	{
	if (_isAccelerating)
		_acc = getLook () * pow (shipAcc, 1.9);
	else
		_acc = Vector3D ();
	
	Entity::update (frameLength);
	
	if (_rocketCooldown > 0)
		_rocketCooldown -= frameLength;
	
	_label = std::to_string ((int)ceil(_rocketCooldown)) + " : " + std::to_string (_rockets);
}

void Ship::_buildModel ()	{
	if (!ModelProvider::modelNamed ("ship1"))	{
		float vert1[] = {
			-1, 0,
			-2, 1,
			-1, 1,
			-0.5, 0.5,
			1.5, 0.5,
			2.0, 0,
			1.5, -0.5,
			-0.5, -0.5,
			-1, -1,
			-2, -1,
			-1, 0,
			-1, 0
		}, vert2[] = {
			-1, 0,
			-2, 1,
			-1, 1,
			-0.5, 0.5,
			1.5, 0.5,
			2.0, 0,
			1.5, -0.5,
			-0.5, -0.5,
			-1, -1,
			-2, -1,
			-1, 0,
			-2, 0
		}, vert3[] = {
			-1, 0,
			-2, 1,
			-1, 1,
			-0.5, 0.5,
			1.5, 0.5,
			2.0, 0,
			1.5, -0.5,
			-0.5, -0.5,
			-1, -1,
			-2, -1,
			-1, 0,
			-3, 0
		}, vert4[] = {
			-1, 0,
			-2, 1,
			-1, 1,
			-0.5, 0.5,
			1.5, 0.5,
			2.0, 0,
			1.5, -0.5,
			-0.5, -0.5,
			-1, -1,
			-2, -1,
			-1, 0,
			-4, 0
		}, col[48] = {0.0};
		
		for (int i = 0; i < 48; i += 4)
			col[i + 1] = col[i + 2] = col[i + 3] = 1;
		
		VBO *frame = new VBO (vert1, NULL, col, NULL, 12, 0, GL_LINE_STRIP, 2);
		Model *model = new Model (0.3, frame);
		model->addFrame (vert2, NULL, col, NULL, 12, 0, GL_LINE_STRIP, 2);
		model->addFrame (vert3, NULL, col, NULL, 12, 0, GL_LINE_STRIP, 2);
		model->addFrame (vert4, NULL, col, NULL, 12, 0, GL_LINE_STRIP, 2);
		
		ModelProvider::instance ()->addModel ("ship1_idle", frame);
		ModelProvider::instance ()->addModel ("ship1_move", model);
	}
	
	_model = ModelProvider::modelNamed ("ship1_idle");
	_rocketCooldown = 0.0;
	_rockets = 20;
	setSize (1.2);
	_scale = 0.6;
}

void Ship::fire ()	{
	if (_rocketCooldown > 0.0 || _isDead || _rockets <= 0)	return;
	
	_lastRocketShot = new Rocket (this, _pos + (getLook () * 1.5), getLook () * rocketAcc * rocketAcc + _spe, _rot);
	GameEngine::instance ()->addEntity (_lastRocketShot);
	
	if ((_spe - (getLook () * rocketAcc)).length() <= _maxSpeed)
		_spe -= (getLook () * rocketAcc);
	
	_rocketCooldown = 4.0;
	_rockets --;
}

void Ship::setAccelerating (bool acc)	{
	_isAccelerating = acc;
	_model = ModelProvider::modelNamed ((acc) ? "ship1_move" : "ship1_idle");
	
	if (acc)
		_model->reset ();
}






