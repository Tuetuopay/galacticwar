//
//  Entity.cpp
//  GalacticWar
//
//  Created by Alexis on 13/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "Entity.h"

#include "GFX/TextEngine.h"
#include "Game/GameEngine.h"

Entity::Entity ()	{
	_pos = Vector3D (20.0, 20.0, 0.0);
	_spe = _acc = Vector3D ();
	
	_rot = _radSpe = 0.0;
	_maxSpeed = 75.0;
	
	_isDead = false;
	_renderWhenDead = false;
	_size = _scale = 1.0;
}
Entity::Entity (const Vector3D &pos, const Vector3D &spe, const Vector3D &acc)	{
	_pos = pos;
	_spe = spe;
	_acc = acc;
	
	_rot = _radSpe = 0;
	_maxSpeed = 75.0;
	
	_isDead = false;
	_renderWhenDead = false;
	_size = _scale = 1.0;
}
Entity::Entity (const Vector3D &pos, const Vector3D &spe, const Vector3D &acc, double rot, double rSpe)	{
	_pos = pos;
	_spe = spe;
	_acc = acc;
	
	_rot = rot;
	_radSpe = rSpe;
	_maxSpeed = 75.0;
	
	_isDead = false;
	_renderWhenDead = false;
	_size = _scale = 1.0;
}

Entity::~Entity ()	{
	
}

void Entity::update (double frameLength)	{
	if ((_spe + _acc * frameLength).length () <= _maxSpeed)
		_spe += _acc * frameLength;
	_pos += _spe * frameLength;
	
	_rot += _radSpe * frameLength;
}

void Entity::render () const	{
	if (!_model)	return;
	if (_isDead && !_renderWhenDead)	return;
	
	glLineWidth (1.5);
	glPushMatrix ();
	glTranslated (_pos.x, _pos.y, 0);
	glPushMatrix ();
	glRotated (_rot, 0, 0, 1);
	glScaled (_scale, _scale, 1);
	_model->render ();
	glPopMatrix ();
	glLineWidth (1.0);
	
	if (_label != "")	{
		glTranslated (-TextEngine::instance ()->getStringWidth (_label) / 4.0, -1.5, 0.0);
		glScaled (0.5, 0.5, 1);
		TextEngine::instance ()->drawString (_label);
	}
	glPopMatrix ();
}

void Entity::kill ()	{
	_isDead = true;
	GameEngine::instance ()->removeEntity (this);
}
