//
//  Rocket.cpp
//  GalacticWar
//
//  Created by Alexis on 13/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "Rocket.h"

#include "GFX/ModelProvider.h"

#include "GFX/TextEngine.h"

Rocket::Rocket (Entity *owner, const Vector3D& pos, const Vector3D& spe, const double& rot) : Entity (pos, spe, Vector3D (), rot, 0)	{
	if (!ModelProvider::modelNamed ("rocket"))	{
		float vert1[] = {
			-1, 0,
			1, 0,
			-2, 0.4,
			-3, 0.5
		}, vert2[] = {
			-1, 0,
			1, 0,
			-2, -0.4,
			-3, -0.5
		}, col[] = {
			0, 1, 1, 1,
			0, 1, 1, 1,
			0, 1, 1, 1,
			0, 1, 1, 1
		};
		
		Model *model = new Model (0.025, new VBO (vert1, NULL, col, NULL, 4, 0, GL_LINES, 2));
		model->addFrame (vert2, NULL, col, NULL, 4, 0, GL_LINES, 2);
		
		ModelProvider::instance ()->addModel ("rocket", model);
	}
	
	_model = ModelProvider::modelNamed ("rocket");
	_ownerShip = owner;
	
	life = 20.0;
	setSize (0.2);
	_scale = 0.2;
}

Rocket::~Rocket ()	{
	
}

void Rocket::update (double frameLength)	{
	if (life > 0.0)	{
		life -= frameLength;
		if (isDead ())
			life = 0.0;
		if (life <= 0.0)	{
			kill ();
			_label = "DEAD";
		}
		Entity::update (frameLength);
		
		_label = std::to_string (life);
	}
    
    if (_spe.x != 0.0)
		setRot (RAD2DEG (atan2 (_spe.y, _spe.x)));
}

void Rocket::render () const	{
	Entity::render ();
}


