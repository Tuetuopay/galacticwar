//
//  Ship.h
//  GalacticWar
//
//  Created by Alexis on 13/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _SHIP_H
#define _SHIP_H

#include "OSCompatibility.h"

#include "GFX/OpenGL/VBO.h"
#include "Entity.h"
#include "Rocket.h"

class Ship : public Entity	{
public:
	Ship (int id);
	Ship (int id, const Vector3D &pos, const Vector3D &spe, const Vector3D &acc);
	Ship (int id, const Vector3D &pos, const Vector3D &spe, const Vector3D &acc, double rot, double rSpe);
	~Ship ();
	
	void update (double frameLength);
	void fire ();
	
	inline int rockets () const	{ return _rockets; }
	void setAccelerating (bool acc);
	inline long id () const { return _id; }
	
	virtual void kill ()	{ _isDead = true; }
	inline bool canBeKilledBy (Entity *entity) const { return !(entity == _lastRocketShot && _rocketCooldown > 4.9); }
	
	static float rocketAcc, shipAcc;
	
protected:
	
	/* The ship ID (for multiplayer) */
	long _id;
	
	float _rocketCooldown;
	bool _isAccelerating;
	int _rockets;
	
	Rocket *_lastRocketShot;
	
	void _buildModel ();
};

#endif /* defined(_SHIP_H) */