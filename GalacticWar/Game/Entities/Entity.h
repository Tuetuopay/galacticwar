//
//  Entity.h
//  GalacticWar
//
//  Created by Alexis on 13/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _ENTITY_H
#define _ENTITY_H

#include "GFX/3DUtils/Vector3D.h"

#include "GFX/ModelProvider.h"

#include "GFX/sdlglutils.h"

class Entity	{
public:
	Entity ();
	Entity (const Vector3D &pos, const Vector3D &spe, const Vector3D &acc);
	Entity (const Vector3D &pos, const Vector3D &spe, const Vector3D &acc, double rot, double rSpe);
	~Entity ();
	
	virtual void render () const;
	virtual void update (double frameLength);
	
	inline Vector3D pos () const	{ return _pos; }
	inline Vector3D spe () const	{ return _spe; }
	inline Vector3D acc () const	{ return _acc; }
	inline double rot () const	{ return _rot; }
	inline double rSpe () const	{ return _radSpe; }
	inline double maxSpeed () const	{ return _maxSpeed; }
	
	inline void setPos (const Vector3D& pos)	{ _pos = pos; }
	inline void setPosX (const double& x)	{ _pos.x = x; }
	inline void setPosY (const double& y)	{ _pos.y = y; }
	inline void setSpe (const Vector3D& spe)	{ _spe = spe; }
	inline void setSpeX (const double& x)	{ _spe.x = x; }
	inline void setSpeY (const double& y)	{ _spe.y = y; }
	inline void setAcc (const Vector3D& acc)	{ _acc = acc; }
	inline void setRot (const double& rot)		{ _rot = rot; }
	inline void setRSpe (const double& rSpe)	{ _radSpe = rSpe; }
	inline void setMaxSpeed (const double& maxSpeed)	{ _maxSpeed = maxSpeed; }
	
	inline Vector3D getLook () const	{ return Vector3D (cos (deg2rad (_rot)), sin (deg2rad (_rot)), 0); }
	
	inline virtual bool isDead () const	{ return _isDead; }
	virtual void kill ();
	
	inline double size () const	{ return _size; }
	inline void setSize (double size)	{ _size = size; }
	
	inline virtual bool canBeKilledBy (Entity *entity) const { return true; }
	
protected:
	/* Positions */
	Vector3D	_pos, _spe, _acc;
	double		_rot, _radSpe;
	double		_maxSpeed;
	
	/* Dimensions */
	double	_size, _scale;
	
	/* Rendering stuff */
	Model	*_model;
	
	/*  */
	bool _isDead, _renderWhenDead;
	
	/* A name to be displayed above the entity */
	std::string _label;
};

#endif /* defined(_ENTITY_H) */