//
//  Rocket.h
//  GalacticWar
//
//  Created by Alexis on 13/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _ROCKET_H
#define _ROCKET_H

#include "Entity.h"
#include "GFX/OpenGL/VBO.h"

class Rocket : public Entity	{
public:
	Rocket (Entity *owner, const Vector3D& pos, const Vector3D& spe, const double& rot);
	~Rocket ();
	
	void update (double frameLength);
	void render () const;
	
private:
	/* Rocket life in seconds */
	double life;
	
	/* The owner ship */
	Entity *_ownerShip;
};

#endif /* defined(_ROCKET_H) */