//
//  GameData.h
//  GalacticWar
//
//  Created by Alexis on 12/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef GalacticWar_GameData_h
#define GalacticWar_GameData_h

#include "GUI/GUILib/GUIEngine.h"

#include "GFX/OpenGL/VBO.h"
#include "GFX/3DUtils/Vector3D.h"
#include "GFX/Model.h"

#include "Game/Entities/Ship.h"

class GameData
{
public:
	GameData ();
	~GameData ();
	
	/* This is managing all of the game's GUIs. (rendering, event handling, etc...) */
	GUIEngine	*guiEngine;
	
	/* Could be used by a LOT of things: duration of the last processed frame */
	float	frameLength;
	
	/* Some may need this */
	unsigned long int firstGameFrame;
	
	/* Window width and height in GL units */
	double winGLWidth, winGLHeight;
	
	Ship *ship, *ship2;
	int scores[2];
	
	/* Should we wrap around or bounce ? */
	bool wrap;
};


#endif
