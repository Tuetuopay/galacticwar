//
//  GameData.cpp
//  GalacticWar
//
//  Created by Alexis on 13/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "Game/GameData.h"

GameData::GameData ()	{
	// memset (this, 0, sizeof(GameData));	/* Will set up everything to 0 (ptrs and vars) */
	guiEngine = NULL;
	
	frameLength = 0.2;	/* Dirty, but let's assume that the first frame was .2 seconds to preven NaN vars */
	
	firstGameFrame = 0;
	
	winGLHeight = winGLWidth = 0.0;
	
	wrap = false;
	
	ship = ship2 = NULL;
	scores[0] = scores[1] = 0;
}

GameData::~GameData ()	{
	
}