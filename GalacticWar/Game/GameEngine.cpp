//
//  GameEngine.cpp
//  GalacticWar
//
//  Created by Alexis on 13/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "GameEngine.h"

#include "PacketList.h"

GameEngine* GameEngine::_instance = NULL;

#define PRINT_TIME \
std::cout << "(" << __LINE__ << ") Time: " << float(SDL_GetTicks()) / 1000.0 << "s, Mem: " << SystemProfiler::ramToString(SystemProfiler::currentRAMUsage()) << std::endl;

GameEngine::GameEngine ()	{
	if (!_instance)
		_instance = this;
	
	/* First of all, the game data that we are going to fill later */
	_gameData = new GameData ();
	
	/* Then, creating the render engine, so that OpenGL is set up */
	_renderEngine = new RenderEngine (_gameData);
	
	new TextEngine ("default-hd");
	
	_gameData->winGLWidth = GUI::screenGLWidth = (double)WIN_W / (double)TILE_SIZE;
	_gameData->winGLHeight = GUI::screenGLHeight = (double)WIN_H / (double)TILE_SIZE;
	GUI::screenPXWidth = WIN_W;	GUI::screenPXHeight = WIN_H;
	
	_gameData->guiEngine = new GUIEngine ();
	
	_debugGui = new DebugGUI ();
	_debugGui->hide ();
	
	_optionsGui = new OptionsGUI ();
	_optionsGui->moveToScreenCenter ();
	_optionsGui->setOutlineColor (0.7, 0.7, 0.7, 0.7);
	//_optionsGui->show ();
	
	_scoresGui = new ScoresGUI ();
	_scoresGui->stickToDown ();
	
	_networkGui = new NetworkGUI ();
	_networkGui->onClient (on_startClient, this);
	_networkGui->onServer (on_startServer, this);
	_networkGui->stickToRight ();
	
	_keepRunning = true;
	_mouseButtonUp = true;	/* Mouse button is released, isn't it ? */
	
	_timeBeforeRespawn = -1.0;
	_multiplayer = _isServer = false;
	_netServer = _netClient = NULL;
	
	respawnShips ();
}

GameEngine::~GameEngine ()	{
	delete _renderEngine;
	delete _gameData->guiEngine;
	delete _gameData;
	
	if (_netClient)
		delete _netClient;
	if (_netServer)
		delete _netServer;
}

void GameEngine::run ()	{
	int beginTime = SDL_GetTicks(), endTime = SDL_GetTicks();
	
	_gameData->firstGameFrame = SDL_GetTicks();
	
	while (_keepRunning)	{
		beginTime = SDL_GetTicks();
		processEvents();
		frame();
		_renderEngine->render ();
		endTime = SDL_GetTicks();
		
		_gameData->frameLength = (float)(endTime - beginTime) / 1000.0;
		SDL_Delay ((1.0 / (float)FPS) - ((_gameData->frameLength > 1.0 / (float)FPS) ? (1.0 / (float)FPS) : _gameData->frameLength));
	}
}

void GameEngine::processEvents ()	{
	static SDL_Event event;
	
	while (SDL_PollEvent (&event))	{
		switch (event.type) {
			case SDL_QUIT:
				_keepRunning = false;
				break;
				
			case SDL_KEYUP:
			case SDL_KEYDOWN:
				processKeypress(event.key, (event.type == SDL_KEYUP));
				break;
				
			case SDL_TEXTINPUT:
				processTextInput (event.text);
				break;
				
			case SDL_MOUSEBUTTONDOWN:
			case SDL_MOUSEBUTTONUP:
				_mouseButtonUp = (event.type == SDL_MOUSEBUTTONUP);
				/* First proceed GUIs, so that is a click landed on a GUI, we'll stop proceeding */
				if (_gameData->guiEngine->processMouseButtonAll (RenderEngine::pixelsToGLUnit (event.button.x), RenderEngine::pixelsToGLUnit (event.button.y), event.button))
					break;
				
				switch (event.button.button)	{
				case SDL_BUTTON_LEFT:
					processLeftClick (event.button.x, event.button.y, (event.type == SDL_MOUSEBUTTONUP));
					break;
				case SDL_BUTTON_RIGHT:
					processRightClick (event.button.x, event.button.y, (event.type == SDL_MOUSEBUTTONUP));
					break;
				}
				break;
				
			case SDL_MOUSEMOTION:
				processMouseMotion (event.motion.x, event.motion.y, event.motion.xrel, event.motion.yrel, !_mouseButtonUp);
				break;
				
			default:
				break;
		}
	}
}

void GameEngine::frame ()	{
	for (Entity *cur : _entities)	{
		cur->update (_gameData->frameLength);
		
		if (_gameData->wrap)	{
			if (cur->pos ().x < 0)	cur->setPosX (TILES_W);
			if (cur->pos ().y < 0)	cur->setPosY (TILES_H);
			if (cur->pos ().x > TILES_W)	cur->setPosX (0);
			if (cur->pos ().y > TILES_H)	cur->setPosY (0);
		} else	{
			if (cur->pos ().x < 0 || cur->pos ().x > TILES_W)	cur->setSpeX (-cur->spe ().x);
			if (cur->pos ().y < 0 || cur->pos ().y > TILES_H)	cur->setSpeY (-cur->spe ().y);
		}
		
		/* Collisions with other entities */
		if (cur->isDead ())	continue;
		for (Entity *cur2 : _entities)	{
			if (cur2->isDead ()) continue;
			if (cur == cur2)	continue;
			
			if ((cur2->pos () - cur->pos ()).length () <= cur2->size () + cur->size () && cur->canBeKilledBy (cur2) && cur2->canBeKilledBy (cur))	{
				cur->kill ();
				cur2->kill ();
				
				if (cur == _gameData->ship || cur2 == _gameData->ship)	{
					_gameData->scores[1]++;
					_scoresGui->update (_gameData);
					_timeBeforeRespawn = 10.0;
				}
				if (cur == _gameData->ship2 || cur2 == _gameData->ship2)	{
					_gameData->scores[0]++;
					_scoresGui->update (_gameData);
					_timeBeforeRespawn = 10.0;
				}
			}
			
			// std::cout << "Entity distance : " << (cur2->pos () - cur->pos ()).length () << ", target distance : " << cur2->size () + cur->size () << std::endl;
		}
	}
	
	for (Entity *e : _toKill)
		_entities.erase (e);
	_toKill.clear ();
	
	if (_timeBeforeRespawn > 0.0)	{
		_timeBeforeRespawn -= _gameData->frameLength;
		if (_timeBeforeRespawn <= 0.0)
			respawnShips ();
	}
	
	if (_netServer)	{
		_netServer->update ();
	}
	if (_netClient)		{
		_netClient->update ();
	
		while (!_netClient->queueEmpty ()) {
			ClientPacket cp = _netClient->queueBack ();
			_netClient->queuePop ();
			
			Log::log << "Found a packet (ID #" << cp.packet.id () << ") from client #" << cp.clientID << "." << std::endl;
			
			switch ((Game::PacketList)cp.packet.id ()) {
				case Game::SPEED_CHANGE:	{
					Packet<Vector3D> p = Packet<Vector3D>::toType (cp.packet);
					_gameData->ship2->setSpe (p.value);
					break;
				}
				case Game::ROT_CHANGE:	{
					Packet<double> p = Packet<double>::toType (cp.packet);
					_gameData->ship2->setRSpe (p.value);
					Log::log << "Received a rot change packet." << std::endl;
					break;
				}
				case Game::ROCKET_FIRE:
					_gameData->ship2->fire ();
					break;
				case Game::CHAT_MSG:	{
					Packet<std::string> p = Packet<std::string>::toType (cp.packet);
					Log::log << "Chat : [" << cp.clientID << "] " << p.value << std::endl;
					break;
				}
				case Game::FORWARD_CHANGE:	{
					Packet<bool> p = Packet<bool>::toType (cp.packet);
					_gameData->ship2->setAccelerating (p.value);
				}
					
				default:
					break;
			}
		}
	}
	
	ModelProvider::instance ()->update (_gameData->frameLength);
	
	_debugGui->update (_gameData);
}

void GameEngine::processKeypress (SDL_KeyboardEvent event, bool keyUp)	{
	_gameData->guiEngine->processKeypressAll (event);
	
	switch (event.keysym.scancode)	{
		case SDL_SCANCODE_ESCAPE:
			_keepRunning = false;
			break;
			
		case SDL_SCANCODE_Z:
			_gameData->ship->setSpe (Vector3D ());
			_gameData->ship->setAcc (Vector3D ());
			break;
			
		case SDL_SCANCODE_F3:
			if (event.repeat || keyUp)	break;
			_debugGui->setShown (!_debugGui->isShown ());
			break;
			
		/* Ship 1 */
		case SDL_SCANCODE_UP:
			if (event.repeat)	break;
			_gameData->ship->setAccelerating (!keyUp);
			break;
		case SDL_SCANCODE_DOWN:
			if (!keyUp)
				_gameData->ship->fire ();
			break;
		case SDL_SCANCODE_LEFT:
			_gameData->ship->setRSpe ((keyUp) ? 0.0 : -40.0);
			break;
		case SDL_SCANCODE_RIGHT:
			_gameData->ship->setRSpe ((keyUp) ? 0.0 : 40.0);
			break;
			
			/* Ship 2 */
		case SDL_SCANCODE_W:
			if (event.repeat)	break;
			if (_netClient)	{
				Packet<bool> p (Game::FORWARD_CHANGE, !keyUp);
				_netClient->send (&p);
			}
			// _gameData->ship2->setAccelerating (!keyUp);
			break;
		case SDL_SCANCODE_S:
			if (!keyUp)	{
				Packet<bool> p (Game::ROCKET_FIRE, true);
				if (_netClient)
					_netClient->send (&p);
			}
				// _gameData->ship2->fire ();
			break;
		case SDL_SCANCODE_A:
			if (event.repeat) break;
			if (_netClient)	{
				Packet<double> p (Game::ROT_CHANGE, (keyUp) ? 0.0 : -40.0);
				_netClient->send (&p);
			}
			// _gameData->ship2->setRSpe ((keyUp) ? 0.0 : -40.0);
			break;
		case SDL_SCANCODE_D:
			if (event.repeat) break;
			if (_netClient)	{
				Packet<double> p (Game::ROT_CHANGE, (keyUp) ? 0.0 : 40.0);
				_netClient->send (&p);
			}
			// _gameData->ship2->setRSpe ((keyUp) ? 0.0 : 40.0);
			break;
			
		case SDL_SCANCODE_O:
			if (event.repeat)	break;
			if (!keyUp)	{
				if (_optionsGui->isShown ())
					_optionsGui->hide (true);
				else
					_optionsGui->show (true);
			}
			
		case SDL_SCANCODE_SEMICOLON:
			if (!keyUp)	{
				// Packet<Vector3D> p (42, Vector3D(42.1337, 9.11, 1664.1));
				Packet<std::string> p (Game::CHAT_MSG, "This is another test, again !");
				if (_netClient)	{
					if (_netClient->send (&p))
						Log::log << "Data sent !" << std::endl;
					else
						Log::log << "Data NOT sent :(" << std::endl;
				}
			}
			break;
			
		default:
			break;
	}
}
void GameEngine::processTextInput (SDL_TextInputEvent textEvent)	{
	_gameData->guiEngine->processTextInputAll (textEvent);
}

void GameEngine::processLeftClick (int x, int y, bool buttonUp)	{
	
}
void GameEngine::processRightClick (int x, int y, bool buttonUp)	{
	
}
void GameEngine::processMouseMotion (int x, int y, int xrel, int yrel, bool isClicking)	{
	/* Well ... We won't quit like with clicks so that game will continue to keep track of the mouse */
	_gameData->guiEngine->processMouseMotionAll (RenderEngine::pixelsToGLUnit (x), RenderEngine::pixelsToGLUnit (y), isClicking);
}

void GameEngine::addEntity (Entity *entity)	{
	if (entity)
		_entities.insert (entity);
}
void GameEngine::removeEntity (Entity *entity)	{
	if (entity)
		_toKill.push_back (entity);
}

void GameEngine::respawnShips ()	{
	if (_gameData->ship)
		_entities.erase (_gameData->ship);
	if (_gameData->ship2)
		_entities.erase (_gameData->ship2);
	
	_gameData->ship = new Ship (0);
	_gameData->ship->setPosX (TILES_W - 5);
	_gameData->ship->setPosY (TILES_H - 5);
	_gameData->ship->setRot (180.0);
	addEntity (_gameData->ship);
	
	_gameData->ship2 = new Ship (1);
	_gameData->ship2->setPosX (5);
	_gameData->ship2->setPosY (5);
	addEntity (_gameData->ship2);
}

void GameEngine::on_startClient ()	{
	if (_netClient)
		delete _netClient;
	_netClient = new NetworkEngine (_networkGui->host (), _networkGui->port);
}
void GameEngine::on_startServer ()	{
	if (_netServer)
		delete _netServer;
	_netServer = new NetworkEngine (_networkGui->port);
}


















