//
//  GameEngine.h
//  GalacticWar
//
//  Created by Alexis on 13/07/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _GAME_ENGINE_H
#define _GAME_ENGINE_H

#include "OSCompatibility.h"

#include "GFX/RenderEngine.h"
#include "Game/GameData.h"

#include "GUI/GameGUIs/OptionsGUI.h"
#include "GUI/GameGUIs/DebugGUI.h"
#include "GUI/GameGUIs/ScoresGUI.h"
#include "GUI/GameGUIs/NetworkGUI.h"

#include "Network/NetworkEngine.h"
#include "Network/ClientPacket.h"

#include "System/Queuing.h"

#ifdef __WIN32__
# define FORWARD_KEY	SDLK_w
# define LEFT_KEY		SDLK_a
#else
# define FORWARD_KEY	SDLK_z	/* SDL on UNIX systems are definig these constansts according to current keyboard layout */
# define LEFT_KEY		SDLK_q
#endif


class GameEngine : public Queuing<ClientPacket>	{
public:
	GameEngine ();
	~GameEngine ();
	
	/* Game's entry point */
	void run ();
	
	/* Do I need a description ? */
	static GameEngine* instance ()	{ return _instance; }
	
	/* Adds an entity to the world */
	void addEntity (Entity *entity);
	void removeEntity (Entity *entity);
	inline const std::set<Entity*>& entities () const	{ return _entities; }
	
	inline void wrapAround (bool wrap)	{ _gameData->wrap = wrap; }
	
	GUI_SLOT (on_startClient, GameEngine)
	GUI_SLOT (on_startServer, GameEngine)
	
private:
	/* ====== Game methods ====== */
	/* These are meant to cut the main loop */
	void frame ();
	void processEvents ();
	
	/* Event management. Here we are in pixels ! */
	void processLeftClick (int x, int y, bool buttonUp);
	void processRightClick (int x, int y, bool buttonUp);
	void processMouseMotion (int x, int y, int xrel, int yrel, bool isClicking);
	void processKeypress (SDL_KeyboardEvent keyboardEvent, bool keyUp);
	void processTextInput (SDL_TextInputEvent textEvent);
	
	/* ====== Game Data ====== */
	/* Main game's Render Engine */
	RenderEngine *_renderEngine;
	
	/* Containing all game's data. RenderEngine has the same pointer */
	GameData	*_gameData;
	
	/* GUIs */
	OptionsGUI	*_optionsGui;
	DebugGUI	*_debugGui;
	ScoresGUI	*_scoresGui;
	NetworkGUI	*_networkGui;
	
	/* Generic game-related stuff for which the Render Engine don't need an access */
	bool _keepRunning;
	bool _mouseButtonUp, _isHoldingShift;
	double _timeBeforeRespawn;
	bool _multiplayer, _isServer;
	
	/* Ze instance */
	static GameEngine	*_instance;
	
	/* Basically the entity list */
	std::set<Entity*>	_entities;
	std::vector<Entity*> _toKill;
	
	void respawnShips ();
	
	/* There is the fun : THE NETWORK ! */
	NetworkEngine	*_netServer, *_netClient;
};

#endif /* defined(_GAME_ENGINE_H) */








