#!/bin/bash

function installFor	{
	echo "Installing for OS X 10.$1 ... "
	if [ -e "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.$1.sdk/System/Library/Frameworks/SDL.framework" ]; then
		echo "* SDL framework already found, skipping"
	else
		cp -r SDL.framework /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.$1.sdk/System/Library/Frameworks/
	fi
	if [ -e "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.$1.sdk/System/Library/Frameworks/SDL_image.framework" ]; then
		echo "* SDL_image framework already found, skipping"
	else
		cp -r SDL_image.framework /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.$1.sdk/System/Library/Frameworks/
	fi
	echo "Done."
}

if [ `whoami` = 'root' ]; then	# Well we need to be root here

	if [ -e "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk" ]; then
		installFor 8
	else
		echo "Mac OS X 10.8 SDK not found, skipping ..."
	fi

	if [ -e "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk" ]; then
		installFor 9
	else
		echo "Mac OS X 10.9 SDK not found, skipping ..."
	fi

	if [ -e "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.10.sdk" ]; then
		installFor 10
	else
		echo "Mac OS X 10.10 SDK not found, skipping ..."
	fi

	echo "Installing runtime frameworks ... "
	if [ -e "/Library/Frameworks/SDL.framework" ]; then
		echo '* SDL framework already found, skipping ...'
	else
		cp -r SDL.framework /Library/Frameworks/
	fi
	if [ -e "/Library/Frameworks/SDL_image.framework" ]; then
		echo '* SDL_image framework already found, skipping ...'
	else
		cp -r SDL_image.framework /Library/Frameworks/
	fi
	echo "Done."
else
	echo 'You must exectute me as root >:'
fi

