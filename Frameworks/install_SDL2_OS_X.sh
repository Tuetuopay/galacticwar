#!/bin/bash

function installFor	{
	echo "Installing for OS X 10.$1 ... "
	if [ -e "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.$1.sdk/System/Library/Frameworks/SDL2.framework" -a $overwrite == false ]; then
		echo "* SDL2 framework already found, skipping"
	else
		cp -r SDL2.framework /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.$1.sdk/System/Library/Frameworks/
	fi
	if [ -e "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.$1.sdk/System/Library/Frameworks/SDL2_image.framework" -a $overwrite == false ]; then
		echo "* SDL2_image framework already found, skipping"
	else
		cp -r SDL2_image.framework /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.$1.sdk/System/Library/Frameworks/
	fi
	echo "Done."
}

if [ `whoami` = 'root' ]; then	# Well we need to be root here

	overwrite=false
	r=`echo ${1} | tr '[:upper:]' '[:lower:]'`
	if [ "$r" == '--overwrite' ]; then
		overwrite=true
		echo ' ! Will overwrite any previous installation ! '
	fi

	if [ -e "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk" ]; then
		installFor 8 overwrite
	else
		echo "Mac OS X 10.8 SDK not found, skipping ..."
	fi

	if [ -e "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk" ]; then
		installFor 9 overwrite
	else
		echo "Mac OS X 10.9 SDK not found, skipping ..."
	fi

	if [ -e "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.10.sdk" ]; then
		installFor 10 overwrite
	else
		echo "Mac OS X 10.10 SDK not found, skipping ..."
	fi

	echo "Installing runtime frameworks ... "
	if [ -e "/Library/Frameworks/SDL2.framework" -a $overwrite == false ]; then
		echo '* SDL2 framework already found, skipping ...'
	else
		cp -r SDL2.framework /Library/Frameworks/
	fi
	if [ -e "/Library/Frameworks/SDL2_image.framework" -a $overwrite == false ]; then
		echo '* SDL2_image framework already found, skipping ...'
	else
		cp -r SDL2_image.framework /Library/Frameworks/
	fi
	echo "Done."
else
	echo 'You must exectute me as root >:'
fi

